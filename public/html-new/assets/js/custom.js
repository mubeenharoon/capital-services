$(window).scroll(function () {
    make_sticky_header();
});

$('document').ready(function(){
    $('.custom_select').selectpicker();
    load_random_banner();
    make_sticky_header();
});

function make_sticky_header() {
    var sticky = $('.fix_header'),
        scroll = $(window).scrollTop();

    if (scroll >= 300) {
        sticky.addClass('fixed_on_top');
    } else {
        sticky.removeClass('fixed_on_top');
    }
}

function show_main_menu () {
    $('body').addClass('show_main_menu');
}

function hide_main_menu () {
    $('body').removeClass('show_main_menu');
}

function move_to_section_by_id (sec_id) {
    var sec_top_margin = $('#'+sec_id).offset().top;
    $('html, body').animate({scrollTop: sec_top_margin}, 1000)
}

function load_random_banner () {
    var directory = 'assets/images/banners/';
    var images = ['banner_1.jpg', 'banner_2.jpg', 'banner_3.jpg', 'banner_5.jpg', 'banner_6.jpg'];
    
    var image = images[Math.floor(Math.random()*images.length)];
    $('#home_banner_img_container').css('background-image', "url('" + directory + image + "')");
}

function change_home_map_active_img (anchor) {
    var directory = 'assets/images/maps/';
    var anchor_obj = $(anchor);
    var image = anchor_obj.data('state');
    $('.home_uae_map_cont li a').removeClass('active');
    anchor_obj.addClass('active');
    $('.home_uae_map_cont img').attr('src', directory + image);
}

function show_hide_login_popup () {
    $('.header_add_brand_btn').slideUp();
    $('.header_login_popup').slideToggle();
}

function show_hide_add_brand_btn () {
    $('.header_login_popup').slideUp();
    $('.header_add_brand_btn').slideToggle();
}