jQuery('document').ready(function() {
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    
    //Process icon
    if ($('.box > .overlay').length) {
        $(document).ajaxSend(function(event, request, settings) {
            $('.box > .overlay').fadeIn();
        });

        $(document).ajaxComplete(function(event, request, settings) {
            $('.box > .overlay').fadeOut();
        });
    }
    
    $('input[type="checkbox"].flat-red, input[type="radio"].flat-red').iCheck({
        checkboxClass: 'icheckbox_flat-green',
        radioClass: 'iradio_flat-green'
    });
    
    //Initialize Select2 Elements
    $(".customSelect").selectpicker({
        selectAllText: 'SELECT ALL',
        deselectAllText: 'DESELECT ALL'
    });

    $('#example2').DataTable({
      "paging": false,
      "lengthChange": false,
      "ordering": true,
      "info": false,
    });

    if ($('#textEditor').length > 0) {
        CKEDITOR.replace('textEditor', {
            allowedContent: true
        });
    }
});

function getUniqueSlug (value, type, id, slugFor) {
//            if (!slugFro)
//                slugFro = '';
    $.ajax({
        url: URL+"/admin-panel/ajax/get-slug", // Url to which the request is send
        type: "POST",             // Type of request to be send, called as method
        data: {
            value: value,
            type: type,
            id: id,
            slugFor: slugFor
        }, // Data sent to server, a set of key/value pairs (i.e. form fields and values)
        success: function(data) {
            $('[name="slug"]').val(data);
        }
    });
}

jQuery(document).ready(function() {
    
    
    if ($(".btn-file").length > 0 ) {
        $('document').on('change', '.btn-file :file', function() {
            var input = $(this),
                numFiles = input.get(0).files ? input.get(0).files.length : 1,
                label = input.val().replace(/\\/g, '/').replace(/.*\//, '');
            input.trigger('fileselect', [numFiles, label]);
        });

        $('.btn-file :file').on('fileselect', function(event, numFiles, label) {

            var input = $(this).parents('.input-group').find(':text'),
                log = numFiles > 1 ? numFiles + ' files selected' : label;

            if( input.length ) {
                input.val(log);
            } else {
//                if( log ) /*alert(log);*/
            }

        });
    }
    
});

function get_locations_dropdowns (dropdown, dropdownid, selectedValue, grouped, placeholder) {
    var id = $(dropdown).val();
    if (dropdownid == 'state') {
        var ajax_url = URL+"/ajax/get-states";
    } else if (dropdownid == 'city') {
        var ajax_url = URL+"/ajax/get-cities";
    } else if (dropdownid == 'location') {
        var ajax_url = URL+"/ajax/get-locations";
    } else if (dropdownid == 'sub_location') {
        var ajax_url = URL+"/ajax/get-sub-locations";
    } else {
        return false;
    }
    
    $.ajax({
        url: ajax_url,
        method: "POST",
//        async: false,
        data: {
            id: id
        },
        success: function(response) {
            makeDropdownsHTML(response, dropdownid, selectedValue, grouped, placeholder);
            if (dropdownid == 'state') {
                makeDropdownsHTML([], 'city', selectedValue, grouped, '- Select City -');
                makeDropdownsHTML([], 'location', selectedValue, grouped, '- Select Location -');
                makeDropdownsHTML([], 'sub_location', selectedValue, grouped, '- Select Sub-Location -');
            } else if (dropdownid == 'city') {
                makeDropdownsHTML([], 'location', selectedValue, grouped, '- Select Location -');
                makeDropdownsHTML([], 'sub_location', selectedValue, grouped, '- Select Sub-Location -');
            } else if (dropdownid == 'location') {
                makeDropdownsHTML([], 'sub_location', selectedValue, grouped, '- Select Sub-Location -');
            }
            
            if (selectedValue != '') {
                if (dropdownid == 'state') {
                    if ($('#country').val() == $('#country').data('selectvalue')) {
                        $('#'+dropdownid).trigger('change');
                    }
                }
                if (dropdownid == 'city') {
                    if ($('#state').val() == $('#state').data('selectvalue')) {
                        $('#'+dropdownid).trigger('change');
                    }
                }
                if (dropdownid == 'location') {
                    if ($('#city').val() == $('#city').data('selectvalue')) {
                        $('#'+dropdownid).trigger('change');
                    }
                }
                if (dropdownid == 'citsub_locationy') {
                    if ($('#location').val() == $('#location').data('selectvalue')) {
                        $('#'+dropdownid).trigger('change');
                    }
                }
            }
            geocodeAddress();
        }
    });
}

function makeDropdownsHTML(response, dropdown, selectedValue, grouped, placeholder) {
    var selectedDropdown = jQuery("#"+dropdown);
    if (grouped) {
        var optionsHTML = "",
//            optionsMaskHTML = "",
            groupName = "",
            selectedValuesArray = [];
        if (selectedValue) 
            selectedValuesArray = selectedValue.split(',');
                
        for (var i = 0; i < response.length; i++) {
            if (groupName === "") {
                groupName = response[i].parentName;
                optionsHTML += "<optgroup label='"+groupName+"'>\n";
            }
            
            if (groupName !== response[i].parentName) {
                groupName = response[i].parentName;
                optionsHTML += "</optgroup>\n";
                optionsHTML += "<optgroup label='"+groupName+"'>\n";
            }
            optionsHTML += "<option value='"+response[i].id+"' ";
            optionsHTML += ($.inArray(response[i].id.toString(), selectedValuesArray) > -1 || 
                                $.inArray(parseInt(response[i].id), selectedValuesArray) > -1) ? "selected" : "";
            optionsHTML += ">"+response[i].name+"</option>\n";
        }
    } else {
        if (!placeholder) {
            placeholder = "- Nothing Selected -";
        }
        var optionsHTML = "";
        if (selectedDropdown.prop('multiple') === false) {
            optionsHTML += "<option value=''>"+placeholder+"</option>\n";
        }

        for (var i = 0; i < response.length; i++) {
            optionsHTML += "<option value='"+response[i].id+"' ";
            optionsHTML += (selectedValue == response[i].id) ? "selected" : "";
            optionsHTML += ">"+response[i].name+"</option>\n";
        }
    }
    
    selectedDropdown.html(optionsHTML);
    selectedDropdown.selectpicker('refresh');
}

function get_dropdown_values (dropdown, dropdownid, selectedValue, grouped) {
    var id = $(dropdown).val();
    if (dropdownid == 'products_ids') {
        var ajax_url = URL+"/ajax/get-products";
    } else {
        return false;
    }
    
    $.ajax({
        url: ajax_url,
        method: "POST",
        data: {
            id: id
        },
        success: function(response) {
            makeDropdownsHTML(response, dropdownid, selectedValue, grouped);
            if (dropdownid == 'products_ids') {
                $('#'+dropdownid+' option').attr('selected', 'selected');
                $('#'+dropdownid).selectpicker('refresh');
            }
        }
    });
}

function getSubCategories (dropdown, selectedValue, grouped) {
    var mainCategory = $(dropdown).val();
    
    $.ajax({
        url: URL+"/ajax/get-sub-categories",
        method: "POST",
        data: {
            categoryid: mainCategory
        },
        success: function(response) {
            makeDropdownsHTML(response, "sub_categories", selectedValue, grouped);
        }
    });
}

function get_product_sizes_by_package (dropdown, selectedValue) {
//    var sizes_ids = $(dropdown).children('option:selected').data('childsizes');
    var sizes_ids = $(dropdown).val();
    
    $.ajax({
        url: URL+"/ajax/get-package-sizes",
        method: "POST",
        dataType: 'json',
        data: {
            ids: sizes_ids
        },
        success: function(response) {
            makeDropdownsHTML(response, "product_size", selectedValue);
        }
    });
}

/*Messages*/

$(document).on('change', '.btn-file :file', function() {
  var input = $(this),
      numFiles = input.get(0).files ? input.get(0).files.length : 1,
      label = input.val().replace(/\\/g, '/').replace(/.*\//, '');
  input.trigger('fileselect', [numFiles, label]);
});


// js for uploading and prevewing image

function uploadAndPrivew(files) {
    var validFileTypes = ["image/jpeg","image/png","image/jpg"];
    var myFormData = new FormData();
    var totalFiles = files.files.length;
    for (var i = 0; i < totalFiles; i++) {
        myFormData.append('files[]', files.files[i]);
        if ($.inArray(files.files[i].type, validFileTypes) === -1) {
            alert("Invalid Files");
            return false;
        }
    }
    $.ajax({
        url: URL+"/upload-temp-images", // Url to which the request is send
        type: "POST",             // Type of request to be send, called as method
        data: myFormData, // Data sent to server, a set of key/value pairs (i.e. form fields and values)
        contentType: false,       // The content type used when sending data to the server.
        cache: false,             // To unable request pages to be cached
        processData:false,        // To send DOMDocument or non processed data file it is set to false
        success: function(data) {
            var totalFiles = data.length;
            var totalNewImages = $('#priviewPropertyImages > div').slice(-totalFiles);
            for (var i = 0; i < totalNewImages.length; i++) {
                $(totalNewImages[i]).children('span').data('imagename', data[i]);
            }
            var uploadedFilesDetailsCurrent = $("#uploadedFiles").val();
            $("#uploadedFiles").val(uploadedFilesDetailsCurrent + "######" + data.join('######'));
        }
    });
    prviewImages(files);
}

function prviewImages (files) {
    for (var i = 0; i < files.files.length; i++) {
        var reader = new FileReader();
        reader.onload = imageIsLoaded;
        reader.readAsDataURL(files.files[i]);
    }
}

function imageIsLoaded(e) {
    var currentHTML = $("#priviewPropertyImages").html();
    var newHTML = "\n\<div class='addPropertyImagesCont'>\n\
                        <img src='"+e.target.result+"' alt='' />\n\
                        <span onclick='deleteUploadedImages(this);' data-imagename='no'>x</span>\n\
                    </div>";
    $("#priviewPropertyImages").html(currentHTML + newHTML);
}

function deleteUploadedImages (obj) {
    var imageName = $(obj).data('imagename');
    
    var uploadedFiles = $("#uploadedFiles").val();
    var uploadedFilesNew = uploadedFiles.replace(imageName, "");
    uploadedFilesNew = uploadedFilesNew.replace("############", "######");
    
    var uploadedFilesDetailsStringFirstDigits = uploadedFilesNew.substring(0, 6);
    var uploadedFilesDetailsStringLastDigits = uploadedFilesNew.substring(uploadedFilesNew.length - 6, uploadedFilesNew.length);
    
    if (uploadedFilesDetailsStringFirstDigits === "######") {
        uploadedFilesNew = uploadedFilesNew.substring(6, uploadedFilesNew.length);
    }
    if (uploadedFilesDetailsStringLastDigits === "######") {
        uploadedFilesNew = uploadedFilesNew.substring(0, uploadedFilesNew.length - 6);
    }

    $("#uploadedFiles").val(uploadedFilesNew);
    
    $(obj).parent().remove();
}
    
function check_if_other_selected(obj) {
    var fieldOBJ = $(obj).closest('.input_gourp_with_multiple_addons').find('.dorpdown_text_field_for_others');
    if ($(obj).val() === '-1') {
        fieldOBJ.css('display', 'table-cell');
    } else {
        fieldOBJ.css('display', 'none');
    }  
}

function show_hide_bundle_fields (obj) {
    if ($(obj).val() == 1) {
        $('[name="bundle_quantity"]').closest('.registrationFormFieldCont').fadeIn(function (){
            $('[name="bundle_price"]').closest('.registrationFormFieldCont').fadeIn();
        });
    } else {
        $('[name="bundle_price"]').closest('.registrationFormFieldCont').fadeOut(function (){
            $('[name="bundle_quantity"]').closest('.registrationFormFieldCont').fadeOut();
        });
    }
}

function goto_client_view_for_action (obj, company_id) {
    var action = obj.value;
    if (action) {
        var url = URL + '/' + CLIENT_PREFIX;
        var action_url = ''
        switch (action) {
            case 'Service Location':
                action_url = '/companies/service_locations/add?company_id='+company_id;
                break;
            case 'Branches':
                action_url = '/companies/branches/add?company_id='+company_id;
                break;
            case 'Company Available At':
                action_url = '/companies/grocery_stores/add?company_id='+company_id;
                break;
            case 'Brands':
                action_url = '/brands/add?company_id='+company_id;
                break;
            case 'Product':
                action_url = '/products/add?company_id='+company_id;
                break;
        }
        window.open(url + action_url)
    }
}