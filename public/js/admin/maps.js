/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


var marker;
var old_marker;
var map;
var global_geocoder;

$('document').ready(function () {
    initMap();
});

function initMap() {
    if ($("#google_map").length > 0) {

        var myLatLng = {lat: 24.0566564, lng: 53.9934184};

        map = new google.maps.Map(document.getElementById('google_map'), {
            zoom: 4,
            center: myLatLng,
            draggable: true
        });

        marker = new google.maps.Marker({
            position: myLatLng,
            map: map,
            draggable: true
        });


        dragpin();


        marker.setMap(map);
        global_geocoder = new google.maps.Geocoder();
        var infoWindow = new google.maps.InfoWindow();
        var latlngbounds = new google.maps.LatLngBounds();
        google.maps.event.addListener(map, 'click', function (e) {
            var cen = new google.maps.LatLng(e.latLng.lat(), e.latLng.lng());
            $('#latitude').val(e.latLng.lat());
            $('#longitude').val(e.latLng.lng());
            old_marker = marker;
            marker = new google.maps.Marker({
                map: map,
                position: cen,
                draggable: true
            });

            dragpin();

            if (old_marker)
                old_marker.setMap(null);
            marker.setMap(map);

        });

        $('#sub_location').change(function () {
            geocodeAddress();
        });
//        $('#country, #state, #city, #location, #sub_location').change(function () {
//            geocodeAddress();
//        });
    }
}

function geocodeAddress(geocoder, resultsMap, address) {
    if (!geocoder) {
        geocoder = global_geocoder;
    }
    var map_zoom  = 7;
    if (!address) {
//        var country = $('#country option:selected').html();
        var country = 'UAE';
//        var state = $('#state option:selected').html();
        var city = $('#city option:selected').html();
        var location = $('#location option:selected').html();
        var sub_location = $('#sub_location option:selected').html();

        address = "";
        address += (typeof sub_location == 'undefined' || sub_location == '- Select Sub-Location -') ? "" : (sub_location+", ");
        address += (typeof location == 'undefined' || location == '- Select Location -') ? "" : (location+", ");
        address += (typeof city == 'undefined' || city == '- Select City -') ? "" : (city+", ");
//        address += (typeof state == 'undefined' || state == '- Select State -') ? "" : (state+", ");
        address += (country != '- Select Country -') ? country : "";
//        alert(address);
        var map_zoom = 5;
        if (country == '- Select Country -') {
            return false;
        }
        
        map_zoom = (typeof state == 'undefined' || state == '- Select State -') ? map_zoom : 7;
        map_zoom = (typeof city == 'undefined' || city == '- Select City -') ? map_zoom : 10;
        map_zoom = (typeof location == 'undefined' || location == '- Select Location -') ? map_zoom : 13;
        map_zoom = (typeof sub_location == 'undefined' || sub_location == '- Select Sub-Location -') ? map_zoom : 15;
    }
    geocoder.geocode({'address': address}, function (results, status) {
        if (status === google.maps.GeocoderStatus.OK) {
            map.setCenter(results[0].geometry.location);

            var lat = results[0].geometry.location.lat();
            var long = results[0].geometry.location.lng();
            document.getElementById('latitude').value = lat;
            document.getElementById('longitude').value = long;
            old_marker = marker;
            marker = new google.maps.Marker({
                map: map,
                position: results[0].geometry.location,
                draggable: true
            });
            dragpin();
            if (old_marker)
                old_marker.setMap(null);
            marker.setMap(map);
            
            map.setZoom(map_zoom);

        } else {
            console.log('Geocode was not successful for the following reason: ' + status);
        }
    });
}



function dragpin() {
    google.maps.event.addListener(marker, 'drag', function (event) {
        document.getElementById('latitude').value = this.position.lat();
        document.getElementById('longitude').value = this.position.lng();
    });


    google.maps.event.addListener(marker, 'dragend', function (event) {
        document.getElementById('latitude').value = this.position.lat();
        document.getElementById('longitude').value = this.position.lng();
    });
}