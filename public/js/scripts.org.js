/**
 * Convert a string to HTML entities
 */
String.prototype.toHtmlEntities = function() {
    return this.replace(/./gm, function(s) {
        return "&#" + s.charCodeAt(0) + ";";
    });
};

/**
 * Create string from HTML entities
 */
String.fromHtmlEntities = function(string) {
    return (string+"").replace(/&#\d+;/gm,function(s) {
        return String.fromCharCode(s.match(/\d+/gm)[0]);
    })
};


$(window).scroll(function () {
    
});

$('document').ready(function(){
    $('.custom_select').selectpicker();
    load_random_banner();
    
    $('.autocomplete_search_box').autocomplete({
        serviceUrl: URL+'/ajax/autocomplete',
        groupBy: 'type',
        dataType: 'json',
        type: 'POST',
        onSelect: function (option) {
            $("input[type='hidden'][name='option_id']").val(option.id);
            $("input[type='hidden'][name='option_type']").val(option.data.type);
            $("input[type='hidden'][name='option_value']").val(option.value);
            $(this).closest('form').submit();
        },
        onSearchStart: function (query) {
            $(this).addClass('searching');
        },
        onSearchComplete: function (query, suggestions) {
            $(this).removeClass('searching');
            if ($(this).hasClass('fix_header_search')) {
                $('.autocomplete-suggestions').css({'position':'fixed','top':'50px'});
            } else {
                $('.autocomplete-suggestions').css({'position':'absolute'});
            }
        }
    });
    if ($('.search_result_autocomplete').length > -1) {
        selected_city = $('[name="filter_city"]').val();
        $('.search_result_autocomplete').autocomplete({
            serviceUrl: URL+'/ajax/autocomplete-locations',
            params: {
                city: function() {return $('#autocomplete_city').val();},
                location: function() {return $("[name='filter_location[]']").map(function() {return $(this).attr('value');}).get();},
                sub_location: function() {return $("[name='filter_sub_location[]']").map(function() {return $(this).attr('value');}).get();}
            },
            groupBy: 'type',
            dataType: 'json',
            type: 'POST',
//            async: false,
            onSelect: function (option) {
               
                if (option.data.type === 'location') {
                    var field_name = 'filter_location';
                } else {
                    var field_name = 'filter_sub_location';
                }
                var html = '<div class="sidebar_selected_location" >\n'
                                +'<inpu type="hidden" name="'+field_name+'[]" value="'+option.id+'" />'+option.value+'\n'
                                +'<a href="javascript:;" onclick="remove_filtere_loccation(this);"><i class="material-icons">&#xE5CD;</i></a>\n'
                            +'</div>\n'
                $('#search_resutl_selected_locations_cont').append(html);
                $('#search_resutl_selected_locations_cont').show();
                $(this).val('');
                $('#search_all_location').removeAttr('checked');
                filter_search_result_posts();
            },
            onSearchStart: function (query) {
                $(this).addClass('searching');
            },
            onSearchComplete: function (query, suggestions) {
                $(this).removeClass('searching');
                $('.autocomplete-suggestions').css({'position':'absolute'});
            }
        });
    }
    if ($('.price_range').length > -1) {
        $(".price_range").slider({
            range: true,
            min: 0,
            max: 100,
            values: [0, 100],
            slide: function(event, ui) {
                var range_elemt_box = $(this).closest('.price_range_cont');
                range_elemt_box.children('.input_range_preview').html(ui.values[0] + " AED - " + ui.values[1] + " AED");
                range_elemt_box.children('.range_min').val(ui.values[0]);
                range_elemt_box.children('.range_max').val(ui.values[1]);
            },
            stop: function(event, ui) {
                filter_search_result_posts();
            }
        });
    }
    if ($('.custom_bottstrap_dropdown').length > -1) {
        $('.custom_bottstrap_dropdown').children('.dropdown-menu').find('a').click(function() {
            update_dropdown_hidden_input_value($(this).data('value'), this);
        });
    }
    $(".login").click(function (e) {
        $('.header_add_brand_btn').slideUp();
        $('.client_login').slideToggle();
        e.stopPropagation();
    });
    $(document).click(function(e){
        if (!$(e.target).is('.client_login, .client_login *')) {
            $('.client_login').slideUp();
        }
    });
    $(".add_brand").click(function (e) {
        $('.client_login').slideUp();
        $('.header_add_brand_btn').slideToggle();
        e.stopPropagation();
    });
    $(document).click(function(e){
        if (!$(e.target).is('.header_add_brand_btn, .header_add_brand_btn *')) {
            $('.header_add_brand_btn').slideUp();
        }
    });
});

function make_sticky_header() {
    var sticky = $('.fix_header'),
        scroll = $(window).scrollTop();

    if (scroll >= 300) {
        sticky.addClass('fixed_on_top');
    } else {
        sticky.removeClass('fixed_on_top');
    }
}

function show_main_menu () {
    $('body').addClass('show_main_menu');
}

function hide_main_menu () {
    $('body').removeClass('show_main_menu');
}

function move_to_section_by_id (sec_id, subtract_margin) {
    subtract_margin = (!subtract_margin) ? 110 : subtract_margin;
    var sec_top_margin = $('#'+sec_id).offset().top;
    $('html, body').animate({scrollTop: sec_top_margin - subtract_margin}, 1000)
}

function load_random_banner () {
    var directory = URL + '/images/banners/';
    var images = ['banner_1.jpg', 'banner_2.jpg', 'banner_3.jpg', 'banner_5.jpg', 'banner_6.jpg'];
    
    var image = images[Math.floor(Math.random()*images.length)];
    $('#home_banner_img_container').css('background-image', "url('" + directory + image + "')");
}

function change_home_map_active_img (anchor) {
    var directory = URL + '/images/maps/';
    var anchor_obj = $(anchor);
    var image = anchor_obj.data('state');
    $('.home_uae_map_cont li a').removeClass('active');
    anchor_obj.addClass('active');
    $('.home_uae_map_cont img').attr('src', directory + image);
}
//
//function show_hide_login_popup () {
//    $('.header_add_brand_btn').slideUp();
//    $('.header_login_popup').slideToggle();
//}
//
//function show_hide_add_brand_btn () {
//    $('.header_login_popup').slideUp();
//    $('.header_add_brand_btn').slideToggle();
//}

function update_dropdown_hidden_input_value (value, obj, name) {
    var parent_obj = $(obj).closest('.dropdown-submenu');
    parent_obj.children('input[type="hidden"]').val(value);
//    console.log($(obj).closest('.dropdown-submenu').children('a').children('span').html());
    parent_obj.children('a').children('span:first-child').html($(obj).children('span:first-child').html());
    parent_obj.children('a').children('span:nth-child(2)').html($(obj).children('span:last-child').html());
}

function get_user_location () {
//    alert('hi');
    if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(save_location_in_session);
    } else {
        alert("Geolocation is not supported by this browser.");
    }
}

function save_location_in_session (position) {
    $('.ajax_loading_overlay').css('opacity', '1').css('pointer-event', 'default');
    var lat = position.coords.latitude;
    var long = position.coords.longitude;
    $.ajax({
        url: URL+"/ajax/save-location-in-session",
        type: "POST",
        data: {
            lat: lat,
            long: long
//            lat: 25.4040622,
//            long: 55.5053464
        },
        success: function(response) {
            if (response === 'false') {
                alert('Sorry we are unable to detect your location please try again or select your address manually.');
            } else {
//                console.log(response[1]);
                var Useraddress = "";
                if(response[1].address != null){
                    Useraddress +=response[1].address;
                }
                if(response[1].sub_location != null){
                    Useraddress += ", "+response[1].sub_location;
                }
                if(response[1].location != null){
                    Useraddress += ", "+response[1].location;
                }
                if(response[1].city != null){
                    Useraddress += ", "+response[1].city;
                }
                if(response[1].state != null){
                    Useraddress += ", "+response[1].state;
                }
                if(response[1].country != null){
                    Useraddress += ", "+response[1].country;
                }
                
//                console.log(response);
                $('#user_location').text(Useraddress);
                var html = '';
                if (response[0].city) {
                    $('[name="filter_city"]').val(response[0].city).change();
                }
                if (response[0].location) {
                    html += '<div class="sidebar_selected_location">\n'
                                +'<inpu type="hidden" name="filter_location[]" value="'+response[0].location+'" />'+response[1].location+'\n'
                                +'<a href="javascript:;" onclick="remove_filtere_loccation(this);"><i class="material-icons">&#xE5CD;</i></a>\n'
                            +'</div>\n'
                }
                if (response[0].sub_location) {
                    html += '<div class="sidebar_selected_location">\n'
                                +'<inpu type="hidden" name="filter_sub_location[]" value="'+response[0].sub_location+'" />'+response[1].sub_location+'\n'
                                +'<a href="javascript:;" onclick="remove_filtere_loccation(this);"><i class="material-icons">&#xE5CD;</i></a>\n'
                            +'</div>\n'
                }
                $('#search_resutl_selected_locations_cont').html(html);
                if (html == '') {
                    $('#search_all_location').removeAttr('checked');
                } else {
                    $('#search_all_location').attr('checked', 'checked');
                }
//                filter_search_result_posts();
                var location_json_string = JSON.stringify(response);
                html = '<a href="javascript:;" onclick=\"restore_location(\''+location_json_string.toHtmlEntities()+'\', true)\">\n'
                            +'<p id="selected_location">'+Useraddress+'</p>\n'
                        +'</a>\n'
                
                $('.sidebar_user_location').html(html);
                $('.ajax_loading_overlay').css('opacity', '0').css('pointer-event', 'none');
            }
        }
    });
}

function select_other_water_type_checkboxes (obj) {
    var val = $(obj).val();
    var name = $(obj).attr('name');
    if ($(obj).is(':checked')) {
        $('[name="'+name+'"][value='+val+']').prop('checked', $(obj).is(':checked'));
    } else {
        $('[name="'+name+'"][value='+val+']').prop('checked', false);
         filter_search_result_posts();
    }
}

function get_product_sizes_by_package (obj, selectedValue, checkboxes) {
//   console.log('hi');
//   console.log(checkboxes);
    if (checkboxes) {
        var sizes_ids = $("[name='"+$(obj).attr('name')+"']:checked").map(function() {
            return $(this).val();
        }).get();
        if (sizes_ids == '') {
            sizes_ids = 'all';
        }
    } else {
        var sizes_ids = $(obj).children('option:selected').data('childsizes');
    }
    
    $.ajax({
        url: URL+"/ajax/get-package-sizes",
        method: "POST",
        data: {
            ids: sizes_ids
        },
        success: function(response) {
//            console.log(response);
            var sidebar_sizes_box = $('#Sidebar-Product-Size-btn');
            var html = '';
            var loopcounter = 1000;
            for (var i = 0; i < response.length; i++) {
                html += '<div class="checkbox_cont">'
                            +'<input type="checkbox" name="product_sizes[]" value="'+response[i].id+'" id="checkbox'+loopcounter+'" \n'
                                +'onchange="filter_search_result_posts();" />'
                            +'<label for="checkbox'+loopcounter+'">'+response[i].name+'</label>'
                        +'</div>';
                loopcounter++;
            }
            if (response.length > 0) {
                sidebar_sizes_box.find('.side_bar_content_with_more_option').html(html)
            } else {
                sidebar_sizes_box.find('.side_bar_content_with_more_option').html('<div class="empty_message_box">No sizes available</div>')
            }
            
            var show_mor_btn_obj = sidebar_sizes_box.find('.sidebar_more_link_cont');
            if (response.length > 5) {
                if (show_mor_btn_obj.length) {
                    show_mor_btn_obj.css('display', 'block');
                } else {
                    var more_btn_html = '<div class="sidebar_more_link_cont">\n'
                                            +'<a href="javascript:;" onclick="expand_shring_sidebar_boxes(this)">+ More</a>\n'
                                        +'</div>\n';
                    $('#Sidebar-Product-Size-btn').append(more_btn_html);
                }
            } else {
                show_mor_btn_obj.css('display', 'none');
            }
        }
    });
}

function expand_shring_sidebar_boxes (obj) {
    var parent_box = $(obj).closest('.sidebar_content_box').children('.side_bar_content_with_more_option');
    parent_box.toggleClass('active');
    if (parent_box.hasClass('active')) {
        $(obj).html('- Less');
    } else {
        $(obj).html('+ More');
    }
}

function remove_all_selected_locations (obj) {
    if ($(obj).is(':checked')) {
        $('#search_resutl_selected_locations_cont').slideUp(function () {
            $(this).html('');
            filter_search_result_posts();
        });
    }else{
        filter_search_result_posts();
    }
}

function remove_filtere_loccation (obj) {
    $(obj).closest('.sidebar_selected_location').slideUp(function () {
        $(this).remove();
        filter_search_result_posts();
    });
}

function search_resutl_pagination_ajax_loading (obj) {
    var page_url = $(obj).attr('href').split('?page=');
     $('[name="current_page_number"]').val(page_url[1]);
     filter_search_result_posts_ajax();
     scroll_to_top();
    return false;
}

function scroll_to_top(){
    $('html, body').animate({
        scrollTop: 0
    }, 1000);
}

function filter_search_result_posts () {
    $('[name="current_page_number"]').val(1);
     filter_search_result_posts_ajax();
}

function restore_location(location_obj, html_entity) {
    if (html_entity)
        location_obj = String.fromHtmlEntities(location_obj);
    
    var location = JSON.parse(location_obj);
    var html = '';
    if (location[0].location) {
        html += '<div class="sidebar_selected_location">\n'
                    +'<inpu type="hidden" name="filter_location[]" value="'+location[0].location+'" />'+location[1].location+'\n'
                    +'<a href="javascript:;" onclick="remove_filtere_loccation(this);"><i class="material-icons">&#xE5CD;</i></a>\n'
                +'</div>\n'
    }
    if (location[0].sub_location) {
        html += '<div class="sidebar_selected_location">\n'
                    +'<inpu type="hidden" name="filter_sub_location[]" value="'+location[0].sub_location+'" />'+location[1].sub_location+'\n'
                    +'<a href="javascript:;" onclick="remove_filtere_loccation(this);"><i class="material-icons">&#xE5CD;</i></a>\n'
                +'</div>\n'
    }
    $('#search_resutl_selected_locations_cont').html(html);
    $("#autocomplete_city").val(location[0].city);
    $('.custom_select').selectpicker('refresh');
    filter_search_result_posts ();
}

function special_offer(obj){
    if($(obj).is(':checked')){
        filter_search_result_posts();
         $(this).html('');
    }else{
        filter_search_result_posts();
    }
}

function filter_search_on_product_click (val) 
{
    $('[name="filter_water_types[]"]').prop('checked', false);
    $('[name="filter_water_types[]"][value='+val+']').prop('checked', true);
    filter_search_result_posts();
    
//    var boxes = $('[name="filter_water_types[]"][value='+val+']:checked');
//    console.log($('[name="filter_water_types[]"][value='+val+']:checked').length);
//    if($('[name="filter_water_types[]"][value='+val+']:checked').length == 1){
//        $('[name="filter_water_types[]"][value='+val+']').prop('checked', false);
//        filter_search_result_posts();
////        console.log('ok');
//    }else{
//        $('[name="filter_water_types[]"]').prop('checked', false);
//        $('[name="filter_water_types[]"][value='+val+']').prop('checked', true);
//        filter_search_result_posts();
//    }

}

function filter_search_result_posts_ajax (type) {
    $('.ajax_loading_overlay').css('opacity', '1').css('pointer-event', 'default');
    var city = $('#autocomplete_city').val();
    var location_ids = $("[name='filter_location[]']").map(function() {return $(this).attr('value');}).get();
    var sub_location_ids = $("[name='filter_sub_location[]']").map(function() {return $(this).attr('value');}).get();
    var sizes = $("[name='product_sizes[]']:checked").map(function() {return $(this).attr('value');}).get();
    var companies = $("[name='filter_companies[]']:checked").map(function() {return $(this).attr('value');}).get();
    if (type == null) {
        
        var water_types = $("[name='filter_water_types[]']:checked").map(function() {return $(this).attr('value');}).get();
    }else {
        var water_types = [type];
//        console.log(water_types);
    }
    if ($('[name="special_offer"]').is(':checked')) {
        var special_offer = $('[name="special_offer"]').val();
    }else {
        var special_offer ="";
    }
    
    var min_price = $('[name="filter_min_price"]').val();
    var max_price = $('[name="filter_max_price"]').val();
    var post_per_page = $('[name="filter_post_per_page"]').val();
    var order_by = $('[name="filter_order_by"]').val();
    var filter_for = $('[name="filter_for"]').val();
    var current_page_number = $('[name="current_page_number"]').val();
    var packaging_type = $("[name='packaging_type[]']:checked").map(function() {return $(this).attr('value');}).get();
    
    $.ajax({
        url: URL+"/ajax/get-search-result-posts",
        type: "POST",
        data: {
            special_offer : special_offer,
            city: city,
            location_ids: location_ids,
            sub_location_ids: sub_location_ids,
            sizes: sizes,
            companies: companies,
            water_types: water_types,
            min_price: min_price,
            max_price: max_price,
            post_per_page: post_per_page,
            order_by: order_by,
            filter_for: filter_for,
            page: current_page_number,
            packaging_type: packaging_type,
            
            
        },
        success: function(response) {
            
           
            // console.log(filter_for);
//            console.log(response)
//                        console.log(response.data);
            if (filter_for === 'Products') {
                make_products_html(response.data.data);
                $('.left_sidebar_suppliers_box').slideDown();
            } else {
                $('.left_sidebar_suppliers_box').slideUp();
                make_companies_html(response.data.data);
            }
            
            make_pagination_list (response, URL+'/search');
            $('.pagination').html(response.pagination);
            $('.pagination a').click(function(){
                return search_resutl_pagination_ajax_loading(this);
            });
            $('.ajax_loading_overlay').css('opacity', '0').css('pointer-event', 'none');
        }
    });
}

function make_orderBy_html(obj){
    var orderBy_html = "";
    var value = $(obj).val();
//    console.log(value);
    if (value == "Products"){
        orderBy_html = 
                            '<option value="">Select Order </option>'
                            +'<option value="ASC">Price (Low-High)</option>'
                            +'<option value="DESC">Price (High-Low)</option>';
    }else {
        orderBy_html = 
                            '<option value="">Select Order </option>'
                            +'<option value="ASC">Name (A-Z)</option>'
                            +'<option value="DESC">Name (Z-A)</option>';
                       
    }
    
    var dropdown = $('[name = "filter_order_by"]');
//    console.log(orderBy_html);
    dropdown.html(orderBy_html);
    dropdown.selectpicker('refresh');
}

function make_products_html (response) {
   
    var products_html = '', product_img = '', company_logo = '', price_html = '', phone_html = '', type_html = 'N/A', size_html = '';
    for (var i = 0; i < response.length; i++) {
        if (response[i].image === '' || response[i].image === null) {
            product_img = '<img src="'+URL+'/images/placeholders/product.png" alt="" />';
        } else {
            product_img = '<img src="'+URL+'/img/products/'+get_folder_structure_by_date(response[i].created_at)+'/orignal/'+response[i].image
                                    +'" alt=""/>';
        }
        if(response[i].type){
           
            type_html = '<a href="#" onclick="filter_search_on_product_click('+response[i].type_id+');return false ">\n'+ response[i].type+'</a>\n';
        }
        else
        {
            type_html = "N/A";
        }
        if (response[i].logo === '' || response[i].logo === null) {
            company_logo = '<img src="'+URL+'/images/placeholders/company_small.png" alt="'+response[i].company+'" />';
        } else {
            
             company_logo = '<img src="'+URL+'/img/brands/logos/'+get_folder_structure_by_date(response[i].company_created)+'/orignal/'+response[i].logo
                                    +'" alt="'+response[i].company+'"/>';
//            company_logo = '<img src="'+URL+'/img/brand/'+get_folder_structure_by_date(response[i].company_created)+'/orignal/'+response[i].logo
//                                    +'" alt=""/>';
        }
        
        if(response[i].size){
            size_html = response[i].size;
        }
        else
        {
            size_html = 'N/A';
        }
        var product_price = "";
        var product_quantity = (response[i].bundle_quantity > 0) ? response[i].bundle_quantity : 1;
//        var product_price = (response[i].bundle_price > 0) ? response[i].bundle_price : response[i].price;
        product_price = response[i].price;
                
        var discout_price = (response[i].discount_type == 1) ? (product_price - response[i].discount) : (product_price - ((response[i].discount / 100) * product_price));
        
       
        if (response[i].discount > 0)
            price_html = "<del>"+product_price+" AED</del> <span>"+discout_price+" AED</span>";
        else
            price_html = product_price + " AED";
        
        if (response[i].phone_number)
            phone_html = '<i class="material-icons">&#xE0B0;</i> '+response[i].phone_number;
        else if(response[i].phone_number_company)
            phone_html = '<i class="material-icons">&#xE0B0;</i> '+response[i].phone_number_company;
        products_html += '<div class="product_box">\n'
                            
                                +'<!--<div class="offer_product">50 % Discount</div>-->\n'
                                +'<div class="product_img">\n'
                                    +'<a href="'+URL+'/product/'+response[i].slug+'/'+response[i].id+'">'+product_img+'</a>\n'
                                +'</div>\n'
                                +'<div class="product_content">\n'
                                    +'<div class="product_content_company_logo_and_rating">\n'
                                       +'<a href="'+URL+'/brand/'+response[i].brandSlug+'/'+response[i].brandId+'">\n'
                                            +'<div class="product_content_company_logo">\n'
                                                +company_logo+'\n'
                                            +'</div>\n'
                                        +'</a>\n'
                                        +'<div class="product_content_company_rating rating_stars_box">\n'
                                            +'<i class="material-icons active">&#xE838;</i>\n'
                                            +'<i class="material-icons active">&#xE838;</i>\n'
                                            +'<i class="material-icons active">&#xE838;</i>\n'
                                            +'<i class="material-icons active">&#xE838;</i>\n'
                                            +'<i class="material-icons active">&#xE838;</i>\n'
                                        +'</div>\n'
                                    +'</div>\n'
                                    +'<a href="'+URL+'/product/'+response[i].slug+'/'+response[i].id+'">\n'
                                        +'<h2>'+response[i].title+'</h2>\n'
                                    +'</a>\n'
                                    +'<a href="'+URL+'/brand/'+response[i].brandSlug+'/'+response[i].brandId+'">\n'
                                        +'<p>'+response[i].company+'</p>\n'
                                    +'</a>\n'
                                    +'<p class="product_phone_number">'+phone_html+'</p>\n'
                                    +'<table>\n'
                                        +'<tr>\n'
                                            +'<td>Size</td>\n'
                                            +'<td>'+size_html+' </td>\n'
                                        +'</tr>\n'
                                        +'<tr>\n'
                                            +'<td>Quantity</td>\n'
                                            +'<td>'+product_quantity+' PCS</td>\n'
                                        +'</tr>\n'
                                        +'<tr>\n'
                                            +'<td>Price</td>\n'
                                            +'<td>'+price_html+'</td>\n'
                                        +'</tr>\n'
                                        +'<tr>\n'
                                            +'<td>Water Type</td>\n'
                                            +'<td>' +type_html+'</td>\n'
                                        +'</tr>\n'
                                        +'<tr class= "hid_add_to_cart">\n'
                                            +'<td colspan="2">\n'
                                                +'<a href="javascript:;" class="btn btn_blue">Order Now</a>\n'
                                                +'<a href="javascript:;" class="btn btn_blue btn_light_blue"><i class="material-icons">&#xE854;</i></a>\n'
                                            +'</td>\n'
                                        +'</tr>\n'
                                    +'</table>\n'
                                +'</div>\n'
                            +'</div>\n'
                        +'</div>\n';
    }
    
    if (response.length === 0) 
        products_html = '<div class="">No match found!</div>';
    
    $('.product_list_cont').html(products_html);
}

function make_companies_html (response) {
   
    var products_html = '', company_logo = '', availabilty_html = "N/A";
    for (var i = 0; i < response.length; i++) {
        if (response[i].logo === '' || response[i].logo === null) {
            company_logo = '<img src="'+URL+'/images/placeholders/company.png" alt="" />';
        } else {
            
            company_logo = '<img src="'+URL+'/img/brands/logos/'+get_folder_structure_by_date(response[i].created_at)+'/orignal/'+response[i].logo
                                    +'" alt=""/>';
        }
        if (response[i].availablity){
            availabilty_html = response[i].availablity;
        }
        else
        {
            availabilty_html = "N/A";
        }
        var packages_html = '';
        if (response[i].packages > 0) {
            var company_packages = response[i].packages.split(',');
            for (var j = 0; j < company_packages.length; j++) {
//                 console.log(company_packages[j]);
                
                if (product_packages[company_packages[j]]) {
                    
                    packages_html += '<img src="'+product_packages[company_packages[j]]+'" alt="" />'
                }
            }
        }
        
        if (packages_html == '') {
            packages_html = 'N/A';
        }
       
//        console.log(response[i].packages);
        var urlAdress =URL+"/brand/";
               urlAdress +=response[i].brandSlug+"/";
                       
               urlAdress += response[i].brandsID; 
//               console.log(response[i].brandsID);
        var address = "";
        
       if(response[i].city != null){
           address =response[i].cities;
       }
       else
       {
           address = "N/A";
       }
        
//        console.log(response[i].stateName);
//        console.log(address);
        
        products_html += '<div class="product_box company_box">\n'
                                +'<!--<div class="offer_product">50 % Discount</div>-->\n'
                                
                                +'<div class="product_img">\n'
                                    +'<a href="'+URL+'/brand/'+response[i].brandSlug+'/'+response[i].brandsID+'">\n'
                                        +company_logo+'\n'
                                    +'</a>\n'
                                    +'<div class="product_content_company_rating rating_stars_box company_stars">\n'
                                        +'<i class="material-icons active">&#xE838;</i>\n'
                                        +'<i class="material-icons active">&#xE838;</i>\n'
                                        +'<i class="material-icons active">&#xE838;</i>\n'
                                        +'<i class="material-icons active">&#xE838;</i>\n'
                                        +'<i class="material-icons active">&#xE838;</i>'
                                    +'</div>\n'
                                +'</div>\n'
                                +'<div class="product_content">\n'
                                    +'<a href="'+URL+'/brand/'+response[i].brandSlug+'/'+response[i].brandsID+'">\n'
                                        +'<h2>'+response[i].name+'</h2>\n'
                                    +'</a>\n'
//                                    +'<p>'+response[i].company_name+'</p>\n'
                                    +'<table class="company_content_table">\n'
                                        +'<tr>\n'
                                            +'<td>Products</td>\n'
                                            +'<td class="company_products_row">\n'
                                                +packages_html+'\n'
                                            +'</td>\n'
                                        +'</tr>\n'
                                        +'<tr>\n'
                                            +'<td>Delivery Days</td>\n'
//                                            +'<td>'+availabilty_html+'</td>\n'
                                            +'<td>'+availabilty_html+'</td>\n'
                                        +'</tr>\n'
                                        +'<tr>\n'
                                            +'<td>Available In</td>\n'
                                            +'<td>\n'
                                                +address+'\n'
                                            +'</td>\n'
                                        +'</tr>\n'
                                        +'<tr>\n'
                                            +'<td colspan="2">\n'
                                                +'<a href="'+urlAdress+'" class="btn btn_blue" >Learn More</a>\n'
                                                +'<a href="'+urlAdress+'?product" class="btn brandp btn_blue btn_light_blue" >All Products</a>\n'
                                            +'</td>\n'
                                        +'</tr>\n'
                                    +'</table>\n'
                                +'</div>\n'
                            +'</div>\n';
    }
    
    if (response.length === 0) 
        products_html = '<div class="">No match found!</div>';
    
    $('.product_list_cont').html(products_html);
}

function make_pagination_list (data, page_url) {
//    console.log(data);
    var html = '';
    if (data.last_page !== 0) {
        if (data.prev_page_url === null) {
            html += '<li class="disabled"><span>«</span></li>\n';
        } else {
            html += '<li><a href="'+page_url+'?page='+(data.current_page-1)+'" onclick="return search_resutl_pagination_ajax_loading(this);" rel="prev">«</a></li>\n';
        }

        var first_loop_length = (data.last_page > 8 && data.last_page != 10) ? 8 : data.last_page;
        for (var i = 1; i <= first_loop_length; i++) {
            if (i == data.current_page)
                html += '<li class="active"><span>'+i+'</span></li>\n';
            else 
                html += '<li><a href="'+page_url+'?page='+i+'" onclick="return search_resutl_pagination_ajax_loading(this);" rel="prev">'+i+'</a></li>\n';
        }
        
        if (data.last_page > 8 && data.last_page != 10) {
            html += '<li class="disabled"><span>...</span></li>\n';
            for (var i = data.last_page; i >= (data.last_page - 1); i--) {
                if (i == data.current_page)
                    html += '<li class="active"><span>'+i+'</span></li>\n';
                else 
                    html += '<li><a href="'+page_url+'?page='+i+'" onclick="return search_resutl_pagination_ajax_loading(this);" rel="prev">'+i+'</a></li>\n';
            }
        }

        if (data.next_page_url === null) {
            html += '<li class="disabled"><span>»</span></li>\n';
        } else {
            html += '<li><a href="'+page_url+'?page='+(data.current_page+1)+'" onclick="return search_resutl_pagination_ajax_loading(this);" rel="next">»</a></li>\n';
        }
    }
    
//    console.log(html);
    if($('.pagination').length > 0){
        $('.pagination').html(html);
    }else{
//        console.log('hi');
        $('#paginate_id').html(html);
        $('#paginate_id').addClass('pagination');
    }
    
}

function get_folder_structure_by_date (date) {
    var date = new Date(date);
    var dd   = date.getDate();
    var mm   = date.getMonth()+1;
    var yyyy = date.getFullYear();
    var path= yyyy+"/"+mm;
    return path;
}

function subscribe_newsletter_for_onkey(event, email_obj) {
    var x = event.which || event.keyCode;
    
//    console.log (x);
    if ( x == 13 && !$('.email_newsletter').hasClass('searching')){
        subscribe_newsletter(email_obj);
    }
}

function validateEmail(email) {
    var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
}

function subscribe_newsletter(email_obj) {
//    alert($('.email_newsletter').attr('class'))
    email_obj.addClass('searching');
    $('.newsletter_submit_btn').prop("disabled", true);
    var email = email_obj.val(); 
//        console.log(val);
//        alert (val);

    if (!validateEmail(email)) {
        $('.newsletter_submit_btn').prop("disabled", false);
        email_obj.addClass("error1");
        email_obj.removeClass('searching');
    } else {
        email_obj.removeClass("error1");
        
        $.ajax({
            url: URL + "/ajax/subscribe-newsletter",
            type: "POST",
            data: {
                email: email
            },
            success: function (response) {
                $('.newsletter_submit_btn').prop("disabled", false);
                email_obj.removeClass('searching');
                $(".newsletter_msg").html(response);
                email_obj.val('');
            },
            error: function (response) {
                
                $('.newsletter_submit_btn').prop("disabled",false);
                email_obj.removeClass('searching');
                $(".newsletter_msg").html('Whoops! There were some problems....');
                $(".newsletter_msg").addClass("error2");
            }
        });
    }
}