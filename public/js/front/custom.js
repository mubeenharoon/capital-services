/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

// js for uploading and prevewing image

function uploadAndPrivew(files) {
    var validFileTypes = ["image/jpeg","image/png","image/jpg"];
    var myFormData = new FormData();
    var totalFiles = files.files.length;
    for (var i = 0; i < totalFiles; i++) {
        myFormData.append('pictureFiles[]', files.files[i]);
        if ($.inArray(files.files[i].type, validFileTypes) === -1) {
            alert("Invalid Files");
            return false;
        }
    }
    $.ajax({
        url: URL+"/admin-panel/upload-images", // Url to which the request is send
        type: "POST",             // Type of request to be send, called as method
        data: myFormData, // Data sent to server, a set of key/value pairs (i.e. form fields and values)
        contentType: false,       // The content type used when sending data to the server.
        cache: false,             // To unable request pages to be cached
        processData:false,        // To send DOMDocument or non processed data file it is set to false
        success: function(data) {
            var totalFiles = data.length;
            var totalNewImages = $('#priviewPropertyImages > div').slice(-totalFiles);
            for (var i = 0; i < totalNewImages.length; i++) {
                $(totalNewImages[i]).children('span').data('imagename', data[i]);
            }
            var uploadedFilesDetailsCurrent = $("#uploadedFilesDetails").val();
            $("#uploadedFilesDetails").val(uploadedFilesDetailsCurrent + "######" + data.join('######'));
        }
    });
    prviewImages(files);
}

function prviewImages (files) {
    for (var i = 0; i < files.files.length; i++) {
        var reader = new FileReader();
        reader.onload = imageIsLoaded;
        reader.readAsDataURL(files.files[i]);
    }
}

function imageIsLoaded(e) {
    var currentHTML = $("#priviewPropertyImages").html();
    var newHTML = "\n\<div class='addPropertyImagesCont'>\n\
                        <img src='"+e.target.result+"' alt='' />\n\
                        <span data-imagename='no'>x</span>\n\
                    </div>";
    $("#priviewPropertyImages").html(currentHTML + newHTML);
    
    $('.addPropertyImagesCont span').click(function () {
        deleteUploadedImages(this);
    });
}