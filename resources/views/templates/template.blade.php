<!DOCTYPE html>
<html>
    <head>
        @yield('title_and_meta')
        @include('partials.head')
    </head>
    <body>
        @include('partials.header')
        <!--if(Request::is('/'))-->
            @include('partials.banner')
        <!--endif-->
        <section class="bodyCon">
            <div class="container">
                @yield('page_content')
                @include('partials.footer_links')
            </div>
        </section>
        @include('partials.footer')
        @include('partials.scripts')
        @yield('on_page_scripts')
    </body>
</html>
