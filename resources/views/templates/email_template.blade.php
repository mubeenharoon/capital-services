<!DOCTYPE html>
<html>
    <head>
    </head>
    <body>
        <div style="max-width:800px; margin: 0 auto;">
            <div class="header" style="background: url('http://www.technologywisdom.com/devp/waterapp/public/images/newsLetter_header.png') right top;height: 120px;">
                <a href="http://wateronline.ae"><img style="width: 100px; margin: 10px 0px 0px 45px;" src="http://www.technologywisdom.com/devp/waterapp/public/images/water_online.jpg"></a>
            </div>

            <div style="padding: 0 15px;">
                @yield('email_content')

                <p style="color: black;"><b>Warm Regards,<br>WO! (Team of Excellence)</b></p>

                @yield('email_unsubscribe')
            </div>

            <div style="width: 800px; height: 200px; margin-top: 15px; background:url('http://www.technologywisdom.com/devp/waterapp/public/images/footer_bg.jpg') no-repeat center top / auto 250px; ">
            </div>
            <div  style="background: #1e88e5; padding: 1px 0 0; bottom: 0; width: 800px;  height: 32px;">
                <div style="padding: 0 15px;">
                    <div  style="float: left; "  >
                        <p style="color: #fff; margin: 2px 0 0; font-size: 12px; line-height: 25px;">© {{date('Y')}} <a style="color: #fff; text-decoration: none; font-size: 12px;">wateronline.ae</a>, All rights are reserved.</p>
                    </div>
                    <div style="float: right;margin: 3px 0 0 0">
                        <ul style="height: 20px; padding: 0; margin: 0; display: inline-block;">
                            <li style="margin-left: 5px; text-align: center;display: inline-block;padding: 0;">
                                <a href="https://www.facebook.com/Water-Online-UAE-1159860754104677/" target="_blank"><img class="social_pic img-responsive" style="width:26px;height: 24px;" src="http://www.technologywisdom.com/devp/waterapp/public/images/fb.png"/></a>
                            </li>
                            <li style=" margin-left: 5px; text-align: center;display: inline-block;padding: 0;">
                                <a href="https://twitter.com/WaterOnlineUAE" target="_blank"><img class="social_pic img-responsive" style="width:26px;height: 24px;" src="http://www.technologywisdom.com/devp/waterapp/public/images/tw.png"/></a>
                            </li>
                            <li style="margin-left: 5px; text-align: center;display: inline-block;padding: 0;">
                                <a href="https://www.linkedin.com/company/wateronlineuae" target="_blank"><img class="social_pic img-responsive" style="width:26px;height: 24px;" src="http://www.technologywisdom.com/devp/waterapp/public/images/in.png"/></a>
                            </li>
                            <!--                            <li style="text-align: center;display: inline-block;padding: 0;">
                                                            <a ><img class="social_pic img-responsive" style="width:26px;height: 24px;" src="http://www.technologywisdom.com/devp/waterapp/public/images/rss.png"/></a>
                                                        </li>-->
                            <li style="margin-left: 5px; text-align: center;display: inline-block;padding: 0;">
                                <a href="https://plus.google.com/u/0/117924966328178986237" target="_blank"><img class="social_pic img-responsive" style="width:26px;height: 24px;" src="http://www.technologywisdom.com/devp/waterapp/public/images/g.png"/></a>
                            </li>
                        </ul> 
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>
