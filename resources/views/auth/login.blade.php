@extends('user.templates.template')

@section('page_title') Login @endsection

@section('page_content')

            <div class="row">
                <div class="col-md-12">
                    <h4 class="page-head-line">Please Login To Enter </h4>

                </div>

            </div>
            <div class="row">
                <div class="col-md-6">
                    <h4> Login with <strong>Capital Sign Services Account  :</strong></h4>
                    <div class="col-sm-12">@include("partials.form_errors")</div>
                    <br />
                    <form role="form" method="POST" action="{{url('auth/login')}}">
                        <input type="hidden" name="_token" value="{{csrf_token()}}">
                        <label>Enter Email ID : </label>
                        <input type="text" name="email" class="form-control" placeholder="E-Mail Address" value="{{old('email')}}">
                        <label>Enter Password :  </label>
                        <input type="password" name="password" placeholder="Password" value="{{old('password')}}" class="form-control">
                        <hr />
                        <button type="submit" class="btn btn-info"><span class="glyphicon glyphicon-user"></span> &nbsp;Log Me In </button>&nbsp;
                        <hr/>
                        <a href="{{ url('password/email') }}">Forgot Password? Click here</a>
                    </form>
                </div>

            </div>
            
@endsection