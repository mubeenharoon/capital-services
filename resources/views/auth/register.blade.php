@extends('user.templates.template')

@section('page_title') Login @endsection

@section('page_content')

            <div class="row">
                <div class="col-md-12">
                    <h4 class="page-head-line">Please Enter Your Account Details </h4>

                </div>

            </div>
            <form role="form" method="POST" action="{{url('auth/register')}}">
                <input type="hidden" name="_token" value="{{csrf_token()}}">
                <div class="row">
                    <div class="col-md-12">
                        <h4> Create <strong>Capital Sign Services Account  :</strong></h4>
                        <div class="col-sm-12">@include("partials.form_errors")</div>
                        <br />
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <div class="row">
                            <div class="col-md-6">
                                <label>First Name : </label>
                                <input type="text" name="first_name" class="form-control" placeholder="First Name" value="{{old('first_name')}}" />
                            </div>
                            <div class="col-md-6">
                                <label>Last Name :  </label>
                                <input type="text" name="last_name" class="form-control" placeholder="Last Name" value="{{old('last_name')}}" />
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <label>Phone Number : </label>
                                <input type="text" name="phone_number" class="form-control" placeholder="Phone Number" value="{{old('phone_number')}}" />
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <label>Address : </label>
                                <input type="text" name="address" class="form-control" placeholder="Address" value="{{old('address')}}" />
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-9">
                                <div class="row">
                                    <div class="col-md-6">
                                        <label>City : </label>
                                        <input type="text" name="city" class="form-control" placeholder="City" value="{{old('city')}}" />
                                    </div>
                                    <div class="col-md-6">
                                        <label>State :  </label>
                                        <input type="text" name="state" class="form-control" placeholder="State" value="{{old('state')}}" />
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <label>ZipCode :  </label>
                                <input type="text" name="zipcode" class="form-control" placeholder="ZipCode" value="{{old('state')}}" />
                            </div>
                        </div>
                    </div>
                    <div class="col-md-5 col-md-offset-1">
                        <label>Company Name : </label>
                        <input type="text" name="company_name" class="form-control" placeholder="Company Name" value="{{old('company_name')}}" />
                        <label>Email Address : </label>
                        <input type="text" name="email" class="form-control" placeholder="E-Mail Address" value="{{old('email')}}" />
                        <label>Password :  </label>
                        <input type="password" name="password" class="form-control" placeholder="Password" value="{{old('password')}}" />
                        <label>Confirm Password :  </label>
                        <input type="password" name="password_confirmation" class="form-control" placeholder="Password" 
                               value="{{old('password_confirmation')}}" />
                    </div>
                </div>
<!--                <div class="row">
                    <div class="col-md-12">
                        <h4> Create <strong>Capital Sign Services Account  :</strong></h4>
                        <div class="col-sm-12">@include("partials.form_errors")</div>
                        <br />
                    </div>
                    <div class="col-md-4">
                        <label>Company Name : </label>
                        <input type="text" name="company_name" class="form-control" placeholder="Company Name" value="{{old('email')}}" />
                    </div>
                    <div class="col-md-4">
                        <label>First Name : </label>
                        <input type="text" name="first_name" class="form-control" placeholder="First Name" value="{{old('email')}}" />
                    </div>
                    <div class="col-md-4">
                        <label>Last Name :  </label>
                        <input type="text" name="last_name" class="form-control" placeholder="Last Name" value="{{old('password')}}" />
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <label>Address : </label>
                        <input type="text" name="address" class="form-control" placeholder="Address" value="{{old('address')}}" />
                    </div>
                    <div class="col-md-6">
                        <div class="row">
                            <div class="col-md-9">
                                <div class="row">
                                    <div class="col-md-6">
                                        <label>City : </label>
                                        <input type="text" name="city" class="form-control" placeholder="City" value="{{old('city')}}" />
                                    </div>
                                    <div class="col-md-6">
                                        <label>State :  </label>
                                        <input type="text" name="state" class="form-control" placeholder="State" value="{{old('state')}}" />
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <label>ZipCode :  </label>
                                <input type="text" name="state" class="form-control" placeholder="ZipCode" value="{{old('state')}}" />
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4">
                        <label>Email Address : </label>
                        <input type="text" name="email" class="form-control" placeholder="E-Mail Address" value="{{old('email')}}" />
                    </div>
                    <div class="col-md-4">
                        <label>Password :  </label>
                        <input type="password" name="password" class="form-control" placeholder="Password" value="{{old('password')}}" />
                    </div>
                </div>-->
                <hr />
                <button type="submit" class="btn btn-info"><span class="glyphicon glyphicon-user"></span> &nbsp;Log Me In </button>&nbsp;
            </form>
            
@endsection