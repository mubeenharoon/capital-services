@extends('templates.template')
@section('title_and_meta')
<title>404 | Capital Sign Services</title>
@endsection
@section('page_content')
<section class="inner_page_content_sec">
    <div class="container">
        
        <div class="breadcrumps brand">
            <ul>
                <li><a href="{{url('/')}}">Home</a></li>
                <li>404</li>
            </ul>
        </div>
        <div class="page_body content_body_404 text-center">
<!--            <div class="image_404">
                <img src="{{asset('images/logos/404_error.png')}}">
            </div>-->
            <div class="content_404 text-left">
                <h3>
                    Oops!
                </h3>
                <h2>
                    Sorry ....
                </h2>
                <p>
                    It's not You.<br>it' Us
                </p>
                <a class="goback" href="javascript:" onclick="goback();">Go Back</a>
            </div>
            
            
        </div>
        
        
    </div>
</section>
    <script>
        function goback(){
            window.history.back();
        }

    </script>
@endsection