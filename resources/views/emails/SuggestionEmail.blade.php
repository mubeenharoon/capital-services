@extends('templates.email_template')
@section('email_content')
<p style="color: black;">
    Dear WO! Admin,<br>
    Please note there is a new suggestion request received; 
</p>
<p style="color: black;">Request Details are as follows;</p>
<table style="width: 100%; border-collapse: collapse;">
    <tr style="background: #2866b3; color: #fff;">
        <th style="border: 1px solid #dddddd; text-align: left; padding: 8px; border-right: none;width: 50%;">
            <b>Suggestion Information</b>
        </th>
        <th style="border: 1px solid #dddddd; text-align: left; padding: 8px; border-left: none;width: 50%;">
        </th>
    </tr>
    <tr>
        <td style="border: 1px solid #dddddd; text-align: left; padding: 8px;">
            <b>Subject</b>
        </td>
        <td style="border: 1px solid #dddddd; text-align: left; padding: 8px;">
            {{ $subject }} 
        </td>
    </tr>
    <tr>
        <td style="border: 1px solid #dddddd; text-align: left; padding: 8px;">
            <b>Name</b>
        </td>
        <td style="border: 1px solid #dddddd; text-align: left; padding: 8px;">
            {{ $full_name }}
        </td>
    </tr>
    <tr>
        <td style="border: 1px solid #dddddd; text-align: left; padding: 8px;">
            <b>Email</b>
        </td>
        <td style="border: 1px solid #dddddd; text-align: left; padding: 8px;">
            {{ $email }}
        </td>
    </tr>
    <tr>
        <td style="border: 1px solid #dddddd; text-align: left; padding: 8px;">
            <b>Message</b>
        </td>
        <td style="border: 1px solid #dddddd; text-align: left; padding: 8px;">
            {{ $suggesstion }}
        </td>
    </tr>
</table>
@endsection