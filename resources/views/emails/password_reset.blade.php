<!DOCTYPE html>
<html lang="en-US">
    <head>
        <meta charset="utf-8">
    </head>
    <body>
        <p>Dear User,</p>
        <p>Click here to reset your password: {{ url('password/reset/'.$token) }}</p>
        <p>
            Thanks,<br/>
            Capital Sign Services Team.
        </p>
    </body>
</html>
