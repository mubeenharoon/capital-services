    <!-- REQUIRED JS SCRIPTS -->

    <!-- jQuery 2.1.4 -->
    <script src="{{ asset('/plugins/jQuery/jQuery-2.1.4.min.js') }}"></script>
    <!-- Bootstrap 3.3.2 JS -->
    <script src="{{ asset('/plugins/bootstrap/js/bootstrap.min.js') }}" type="text/javascript"></script>
    <!-- AdminLTE App -->
    <script src="{{ asset('/js/app.min.js') }}" type="text/javascript"></script>

    <!-- Optionally, you can add Slimscroll and FastClick plugins.
          Both of these plugins are recommended to enhance the
          user experience. Slimscroll is required when using the
          fixed layout. -->

    <!-- DATA TABES SCRIPT -->
    <script src="{{ URL::to('/') }}/plugins/datatables/jquery.dataTables.min.js" type="text/javascript"></script>
    <script src="{{ URL::to('/') }}/plugins/datatables/dataTables.bootstrap.min.js" type="text/javascript"></script>
    <!-- SlimScroll -->
    <script src="{{ URL::to('/') }}/plugins/slimScroll/jquery.slimscroll.min.js" type="text/javascript"></script>
    <!-- FastClick -->
    <script src="{{ URL::to('/') }}/plugins/fastclick/fastclick.min.js" type="text/javascript"></script>
    <!-- iCheck 1.0.1 -->
    <script src="{{ URL::to('/') }}/plugins/iCheck/icheck.min.js" type="text/javascript"></script>
    <!-- Custom Select -->
    <script src="{{ URL::to('/') }}/plugins/bootstrap-select/bootstrap-select.js" type="text/javascript"></script>
    <!-- CK Editor -->
    <script src="{{ URL::to('/') }}/plugins/ckeditor/ckeditor.js" type="text/javascript"></script>
    <!-- Datepicker -->
    <script src="{{ URL::to('/') }}/plugins/datepicker/bootstrap-datepicker.js" type="text/javascript"></script>
    <!-- Google Maps API -->
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBbRQFYlnD-RealUlTQ4B5jiuU6hPTXx80&signed_in=true"></script>
    
    <!-- AdminLTE for demo purposes -->
    <script src="{{ URL::to('/') }}/plugins/adminlte/demo.js" type="text/javascript"></script>
    <!-- Custom JS -->
    <script type="text/javascript">var URL = "{{ url('') }}";var ADMIN_PREFIX = "{{ ADMIN_PREFIX }}";</script>
    <script src="{{ URL::to('/') }}/js/admin/custom.js" type="text/javascript"></script>
    <script src="{{ URL::to('/') }}/js/admin/maps.js" type="text/javascript"></script>
        