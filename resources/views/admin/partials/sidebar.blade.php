<!-- Left side column. contains the logo and sidebar -->
<aside class="main-sidebar">

    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">

        <!-- Sidebar user panel (optional) -->
        <div class="user-panel">
            <div class="pull-left image">
                @if (empty(Session::get('admin_user')->image)) 
                    <img src="{{asset('/img/user2-160x160.jpg')}}" class="img-circle" alt=""/>
                @else
                    <img src="{{url("img/admin_users/".getFolderStructureByDate(Session::get('admin_user')->created_at)."/orignal/".
                                    Session::get('admin_user')->image)}}" class="img-circle" alt="" />
                @endif
            </div>
            <div class="pull-left info">
                <p>{{Session::get('admin_user')->first_name}} {{Session::get('admin_user')->last_name}}</p>
                <!-- Status -->
                <!--<a href="#"><i class="fa fa-circle text-success"></i> Online</a>-->
            </div>
        </div>

        <!-- search form (Optional) -->
<!--        <form action="#" method="get" class="sidebar-form">
            <div class="input-group">
                <input type="text" name="q" class="form-control" placeholder="Search..."/>
              <span class="input-group-btn">
                <button type='submit' name='search' id='search-btn' class="btn btn-flat"><i class="fa fa-search"></i></button>
              </span>
            </div>
        </form>-->
        <!-- /.search form -->

        <!-- Sidebar Menu -->
        <ul class="sidebar-menu">
            <li class="header">HEADER</li>
            <!-- Optionally, you can add icons to the links -->
            <li class="{{ activeMenu(ADMIN_PREFIX) }}"><a href="{{ url(ADMIN_PREFIX) }}"><i class='fa fa-dashboard'></i> <span>Dashboard</span></a></li>
            
            <!-- Users Links -->
            <li class="treeview {{ activeMenu('users') }}">
                <a href="{{ url(ADMIN_PREFIX.'/users') }}"><i class='fa fa-users'></i> <span>Users</span> <i class="fa fa-angle-left pull-right"></i></a>
            </li>
            
            <!-- Sign Boards -->
            <li class="treeview {{ activeMenu('sign_boards') }}">
                <a href="{{ activeMenu(ADMIN_PREFIX.'/sign_boards') }}"><i class='fa fa-life-bouy'></i> <span>Sign Boards</span> <i class="fa fa-angle-left pull-right"></i></a>
                <ul class="treeview-menu">
                    <li class="{{ activeMenu(ADMIN_PREFIX.'/sign_boards') }}">
                        <a href="{{ url(ADMIN_PREFIX.'/sign_boards') }}"><i class="fa fa-circle-o"></i> All</a></li>
                    <li class="{{ activeMenu(ADMIN_PREFIX.'/sign_boards/add') }}">
                        <a href="{{ url(ADMIN_PREFIX.'/sign_boards/add') }}"><i class="fa fa-circle-o"></i> Add New</a></li>
                </ul>
            </li>
            
            <!-- Gallery -->
            <li class="treeview {{ activeMenu('gallery') }}">
                <a href="{{ activeMenu(ADMIN_PREFIX.'/gallery') }}"><i class='fa fa-image'></i> <span>Gallery</span> <i class="fa fa-angle-left pull-right"></i></a>
                <ul class="treeview-menu">
                    <li class="{{ activeMenu(ADMIN_PREFIX.'/gallery') }}">
                        <a href="{{ url(ADMIN_PREFIX.'/gallery') }}"><i class="fa fa-circle-o"></i> All</a></li>
                    <li class="{{ activeMenu(ADMIN_PREFIX.'/gallery/add') }}">
                        <a href="{{ url(ADMIN_PREFIX.'/gallery/add') }}"><i class="fa fa-circle-o"></i> Add New</a></li>
                </ul>
            </li>
            
            <!-- Transactions -->
            <li class="{{ activeMenu('transactions') }}">
                <a href="{{ url(ADMIN_PREFIX.'/transactions') }}"><i class='fa fa-globe'></i> <span>Transactions</span></a>
            </li>
            
            <!-- Installations -->
            <li class="{{ activeMenu('installations') }}">
                <a href="{{ url(ADMIN_PREFIX.'/installations') }}"><i class='fa fa-globe'></i> <span>Installations</span></a>
            </li>
            
        </ul><!-- /.sidebar-menu -->
    </section>
    <!-- /.sidebar -->
</aside>
