@extends('admin.app')

@section('htmlheader_title') Transactions @endsection
@section('contentheader_title') Transactions @endsection

@section('contentheader_description')
    <!--<li class="active">Categories</li>-->
@endsection


@section('main-content')
<div class="row">
    <div class="col-xs-12">
        <div class="box">
            @include("partials.form_errors")
    
            <!-- /.box-header -->
            <div class="box-body">
              <table class="table table-bordered">
                <thead>
                    <tr>
                        <th width="40">No</th>
                        <th width="600">Name</th>
                        <th width="100">Amount</th>
                        <th width="200">Payment ID</th>
                        <th width="200">Action</th>
                    </tr>
                </thead>
                <tbody>
                    @if (count($posts) > 0)
                        {{--*/ $loopCounter = ($posts->currentPage() * $posts->perPage()) - $posts->perPage() /*--}}
                        @foreach ($posts as $post)
                            {{--*/ $loopCounter++ /*--}}
                            <tr>
                                <td>{{$loopCounter}}</td>
                                <td>{{$post->first_name}} {{$post->last_name}}</td>
                                <td>{{$post->amount}}</td>
                                <td>{{$post->payment_id}}</td>
                                <td class="text-right">
                                    <a class="btn btn-default" href="{{ url(ADMIN_PREFIX."/installations/".$post->id) }}">Order Link</a>
                                </td>
                            </tr>
                        @endforeach
                    @endif
                </tbody>
                <tfoot>
                    <tr>
                        <th>No</th>
                        <th>Name</th>
                        <th>Amount</th>
                        <th>Payment ID</th>
                        <th>Action</th>
                    </tr>
                </tfoot>
              </table>
            </div>
            <!-- /.box-body -->
            <div class="box-footer clearfix">
                <div id="example2_paginate" class="dataTables_paginate paging_simple_numbers">{!! $posts->render() !!}</div>
            </div>
          </div><!-- /.box -->


    </div>
</div>

@endsection
