@extends('admin.app')

@section('htmlheader_title') Sign Boards @endsection
@section('contentheader_title')
    Sign Boards
    <a class="btn btn-default" href="{{ url(ADMIN_PREFIX.'/grocery_stores/add') }}"><i class="fa fa-plus"></i></a>
@endsection

@section('contentheader_description')
    <!--<li class="active">Categories</li>-->
@endsection


@section('main-content')
<div class="row">
    <div class="col-xs-12">
        <div class="box">
            @include("partials.form_errors")
    
    
            <div class="box-header with-border">
                <div class="box-tools">
<!--                    <div class="input-group input-group-sm" style="width: 150px;">
                        <input type="text" name="table_search" class="form-control pull-right" placeholder="Search">
                        <div class="input-group-btn">
                            <button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
                        </div>
                    </div>-->
                </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table class="table table-bordered">
                <thead>
                    <tr>
                        <th width="40">No</th>
                        <th width="600">Name</th>
                        <th width="200">Price</th>
                        <th width="200">Action</th>
                    </tr>
                </thead>
                <tbody>
                    @if (count($posts) > 0)
                        {{--*/ $loopCounter = ($posts->currentPage() * $posts->perPage()) - $posts->perPage() /*--}}
                        @foreach ($posts as $post)
                            {{--*/ $loopCounter++ /*--}}
                            <tr>
                                <td>{{$loopCounter}}</td>
                                <td>{{$post->name}}</td>
                                <td>{{$post->price}}</td>
                                <td class="text-right">
                                    <form action="{{url(ADMIN_PREFIX."/sign_boards")}}" method="POST">
                                        <input type="hidden" name="_token" value="{{ csrf_token() }}"/>
                                        <input type="hidden" name="_method" value="DELETE" />
                                        <input type="hidden" name="id" value="{{$post->id}}"/>
                                        <a class="btn btn-default" href="{{ url(ADMIN_PREFIX."/sign_boards/".$post->id) }}">
                                            <i class="fa fa-edit"></i></a>
                                        <button type="submit" class="btn btn-danger" onclick="return confirm('Are you sure you want to delete?')">
                                            <i class="fa fa-times"></i></button>
                                    </form>
                                </td>
                            </tr>
                        @endforeach
                    @endif
                </tbody>
                <tfoot>
                    <tr>
                        <th>No</th>
                        <th>Name</th>
                        <th>Price</th>
                        <th>Action</th>
                    </tr>
                </tfoot>
              </table>
            </div>
            <!-- /.box-body -->
            <div class="box-footer clearfix">
                <div id="example2_paginate" class="dataTables_paginate paging_simple_numbers">{!! $posts->render() !!}</div>
            </div>
          </div><!-- /.box -->


    </div>
</div>

@endsection
