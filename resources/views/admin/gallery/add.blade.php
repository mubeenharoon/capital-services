@extends('admin.app')

@section('htmlheader_title') Add Product @endsection
@section('contentheader_title')
    Add Product
    <a class="btn btn-default" href="{{ url(ADMIN_PREFIX.'/gallery/add') }}"><i class="fa fa-plus"></i></a>
@endsection

@section('main-content')
<!-- general form elements disabled -->
<div class="box box-warning">
    <div class="box-header with-border">
        <h3 class="box-title">Add Gallery</h3>
    </div><!-- /.box-header -->
    @include("admin.partials.form_errors")
    <form action="{{ url(ADMIN_PREFIX.'/gallery') }}" class="form-horizontal" method="post" enctype="multipart/form-data">
        <input type="hidden" name="_token" value="{{ csrf_token() }}">
        <input type="hidden" name="id" value="{{ $post->id }}">
        <div class="box-body">

            <div class="registrationFormCont">
                <div class="row">
                    <div class="col-sm-12 registrationFormFieldCont">
                        <label>Gallery Name *</label>
                        <div class="input-group">
                            <span class="input-group-addon input-lg" id="basic-addon1"><img src="{{ asset('img/front/form_icon_bus_name.png') }}" alt="" /></span>
                            <input type="text" class="form-control input-lg" name="name" aria-describedby="basic-addon1" 
                                   value="{{profileGetFieldsValues(old('name'), $post->name)}}" />
                        </div>
                    </div>
                    <div class="col-sm-12 registrationFormFieldCont">
                        <label>&nbsp;<br/><br/></label>
                        <span class="btn btn-primary btn-file">
                            <?php
                            $folderDateStructure = getFolderStructureByDate($post->created_at);
                            $path = "img/gallery/$folderDateStructure";
                            $uploadedFiles = implode('######', $gallery_images)
                            ?>
                            Add Pictures
                            <input type="file" class="btn btn-success" type="button" multiple value="Add Images" onchange="uploadAndPrivew(this)">
                            <input type="hidden" name="uploadedFiles" id="uploadedFiles" value="{{profileGetFieldsValues(old('uploadedFiles'), $uploadedFiles)}}" />
                        </span><br/><br/>
                        <div id="priviewPropertyImages">
                            @if (count($gallery_images) > 0)
                            @foreach ($gallery_images as $gallery_image)
                            <div class="addPropertyImagesCont">
                                <img src="{{url("$path/small/".$gallery_image)}}" alt="" />
                                <span onclick="deleteUploadedImages(this);" data-imagename="{{$gallery_image}}">x</span>
                            </div>
                            @endforeach
                            @endif
                        </div>
                    </div>
                </div>
            </div>




        </div><!-- /.box-body -->
        <div class="box-footer">
            <input class="btn btn-primary pull-right" type="submit" value="Save"/>
        </div><!-- /.box-footer -->
    </form>
    <div class="overlay" style="display: none;">
        <i class="fa fa-refresh fa-spin"></i>
    </div><!-- Box Overlay -->
</div><!-- /.box -->

<script type="text/javascript">
    $('document').ready(function () {
        $('[name="product_type"]').trigger('change');
        $('[name="package_type"]').trigger('change');
    });
</script>
@endsection
