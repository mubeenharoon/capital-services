@extends('admin.app')

@section('htmlheader_title') Products @endsection
@section('contentheader_title')
    Add Gallery
    <a class="btn btn-default" href="{{ url(ADMIN_PREFIX.'/gallery/add') }}"><i class="fa fa-plus"></i></a>
@endsection
@section('main-content')
<div class="row">
    <div class="col-md-12">
    	@include("admin.partials.form_errors")
        <div class="box">
            <div class="box-header with-border">
                <div class="box-tools">
<!--                    <div class="input-group input-group-sm" style="width: 150px;">
                        <input type="text" name="table_search" class="form-control pull-right" placeholder="Search">
                        <div class="input-group-btn">
                            <button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
                        </div>
                    </div>-->
                </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body table-responsive">
              <table class="table table-bordered table-responsive">
                <thead>
                    <tr>
                        <th width="10">No</th>
                        <th width="400">Name</th>
                        <th width="70">Actions</th>
                    </tr>
                </thead>
                <tbody>
                    @if (count($posts) > 0)
                        {{--*/ $loopCounter = ($posts->currentPage() * $posts->perPage()) - $posts->perPage() /*--}}
                        @foreach ($posts as $post)
                            {{--*/ $loopCounter++ /*--}}
                            <tr>
                                <td>{{$loopCounter}}</td>
                                <td>{{$post->name}}</td>
                                <td class="text-right">
                                    <form action="{{url(ADMIN_PREFIX."/gallery")}}" method="POST">
                                        <input type="hidden" name="_token" value="{{ csrf_token() }}"/>
                                        <input type="hidden" name="_method" value="DELETE"/>
                                        <input type="hidden" name="id" value="{{$post->id}}"/>
                                        <a class="btn btn-default" href="{{url(ADMIN_PREFIX."/gallery/".$post->id)}}">
                                            <i class="fa fa-edit"></i>
                                        </a>
                                        <button onclick="return confirm('Do you really want to delete this item');" type="submit" class="btn btn-danger">
                                            <i class="fa fa-times"></i>
                                        </button>
                                    </form>
                                </td>
                            </tr>
                        @endforeach
                    @else
                        <tr><td colspan="3">No Products Found</td></tr>
                    @endif
                </tbody>
                <tfoot>
                    <tr>
                        <th>No</th>
                        <th>Name</th>
                        <th>Actions</th>
                    </tr>
                </tfoot>
              </table>
              <div class="box-footer clearfix">
                <div id="example2_paginate" class="dataTables_paginate paging_simple_numbers">{!! $posts->render() !!}</div>
              </div>
            </div><!-- /.box-body -->
        </div><!-- /.box -->
    </div>
</div>
@endsection
