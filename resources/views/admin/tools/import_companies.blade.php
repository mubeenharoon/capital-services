@extends('admin.app')

@section('htmlheader_title') Import Companies @endsection
@section('contentheader_title')
    Import Companies
@endsection

@section('main-content')
<!-- general form elements disabled -->
<div class="box box-warning">
    <div class="box-header with-border">
        <h3 class="box-title">Import Companies</h3>
    </div><!-- /.box-header -->
    @include("partials.form_errors")
    <form action="{{ url(ADMIN_PREFIX.'/tools/import_companies') }}" class="form-horizontal" method="post" enctype="multipart/form-data">
        <input type="hidden" name="_token" value="{{ csrf_token() }}">
        <div class="box-body">
            <div class="form-group">
                <label class="col-sm-2 control-label">States</label>
                <div class="col-sm-10">
                    <select name="state" class="customSelect form-control input-lg" aria-describedby="basic-addon1" data-live-search="true">
                        {{--*/ $selectedState = old('state'); /*--}}
                        <option value="">- Nothing Selected -</option>
                        {{--*/ $dropdownGroup = ""; /*--}}
                        @foreach ($states as $state) 
                            @if ($dropdownGroup == "") 
                                {{--*/ $dropdownGroup = $state->parent; /*--}}
                                <optgroup label='{{$dropdownGroup}}'>
                            @endif

                            @if ($dropdownGroup != $state->parent) 
                                {{--*/ $dropdownGroup = $state->parent; /*--}}
                                </optgroup>
                                <optgroup label='{{$dropdownGroup}}'>
                            @endif
                            
                            <option value="{{$state->id}}" @if ($state->id == $selectedState)) selected @endif >{{$state->name}}</option>
                        @endforeach
                    </select>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label">Select CSV File</label>
                <div class="col-sm-10">
                    <div class="input-group input-group-lg">
                        <span class="input-group-btn input-group-lg">
                            <span class="btn btn-primary btn-file">
                                <img src="{{ asset('img/front/browse.jpg') }}">&nbsp Browse 
                                <input type="file" name="csv_file" value="{{old('csv_file')}}" accept=".csv">
                            </span>
                        </span>
                        <input type="text" class="form-control input-lg" readonly>
                    </div>
                </div>
            </div>
        </div><!-- /.box-body -->
        <div class="box-footer">
            <input class="btn btn-primary pull-right" type="submit" value="Import"/>
        </div><!-- /.box-footer -->
    </form>
</div><!-- /.box -->
@endsection
