@extends('admin.app')

@section('htmlheader_title') Installations @endsection
@section('contentheader_title') Installations @endsection

@section('contentheader_description')
    <!--<li class="active">Categories</li>-->
@endsection


@section('main-content')
<div class="row">
    <div class="col-xs-12">
        <div class="box">
            @include("partials.form_errors")
    
    
            <div class="box-header with-border">
                <div class="box-tools">
<!--                    <div class="input-group input-group-sm" style="width: 150px;">
                        <input type="text" name="table_search" class="form-control pull-right" placeholder="Search">
                        <div class="input-group-btn">
                            <button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
                        </div>
                    </div>-->
                </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table class="table table-bordered">
                <thead>
                    <tr>
                        <th width="40">No</th>
                        <th width="100">Job Date</th>
                        <th width="100">Expiery Date</th>
                        <th width="490">Address</th>
                        <th width="150">State</th>
                        <th width="165">Status</th>
                        <th width="80">Amount</th>
                        <th width="110">Action</th>
                    </tr>
                </thead>
                <tbody>
                    @if (count($posts) > 0)
                        {{--*/ $loopCounter = ($posts->currentPage() * $posts->perPage()) - $posts->perPage() /*--}}
                        @foreach ($posts as $post)
                            {{--*/ $loopCounter++ /*--}}
                            <tr>
                                <td>{{$loopCounter}}</td>
                                <td>{{date('Y-m-d', strtotime($post->job_date))}}</td>
                                <td>{{date('Y-m-d', strtotime($post->expiry_date))}}</td>
                                <td>{{$post->address}}</td>
                                <td>{{$post->state}}</td>
                                <td>{{$post->status_name}}</td>
                                <td>${{$post->total_amount}}</td>
                                <td class="text-right">
                                    <form action="{{url(ADMIN_PREFIX."/sign_boards")}}" method="POST">
                                        <input type="hidden" name="_token" value="{{ csrf_token() }}"/>
                                        <input type="hidden" name="_method" value="DELETE" />
                                        <input type="hidden" name="id" value="{{$post->id}}"/>
                                        @if ($post->status == 2)
                                        <a class="btn btn-info" href="{{ url(ADMIN_PREFIX."/installations/approve/".$post->id) }}">
                                            <i class="fa fa-check-square"></i></a>
                                        @endif
                                        <a class="btn btn-default" href="{{ url(ADMIN_PREFIX."/installations/".$post->id) }}">
                                            <i class="fa fa-eye"></i></a>
<!--                                        <button type="submit" class="btn btn-danger" onclick="return confirm('Are you sure you want to delete?')">
                                            <i class="fa fa-times"></i></button>-->
                                    </form>
                                </td>
                            </tr>
                        @endforeach
                    @endif
                </tbody>
                <tfoot>
                    <tr>
                        <th>No</th>
                        <th>Job Date</th>
                        <th>Expiery Date</th>
                        <th>Address>
                        <th>State</th>
                        <th>Status</th>
                        <th>Amount</th>
                        <th>Action</th>
                    </tr>
                </tfoot>
              </table>
            </div>
            <!-- /.box-body -->
            <div class="box-footer clearfix">
                <div id="example2_paginate" class="dataTables_paginate paging_simple_numbers">{!! $posts->render() !!}</div>
            </div>
          </div><!-- /.box -->


    </div>
</div>

@endsection
