@extends('admin.app')

@section('htmlheader_title') Add Sign Boards @endsection
@section('contentheader_title')
    Sign Boards
    <a class="btn btn-default" href="{{ url(ADMIN_PREFIX.'/sign_boards/add') }}"><i class="fa fa-plus"></i></a>
@endsection

@section('main-content')
<!-- general form elements disabled -->
<div class="box box-warning">
    <div class="box-header with-border">
        <h3 class="box-title">Add Sign Boards</h3>
    </div><!-- /.box-header -->
    @include("partials.form_errors")
    <form action="{{ url(ADMIN_PREFIX.'/sign_boards') }}" class="form-horizontal" method="post" enctype="multipart/form-data">
        <input type="hidden" name="_token" value="{{ csrf_token() }}">
        <input type="hidden" name="id" value="{{ $post->id }}">
        <div class="box-body">

            <div class="form-group">
                <label class="col-sm-2 control-label">Name</label>
                <div class="col-sm-10">
                    <input class="form-control" placeholder="Name" type="text" name="name" value="{{$post->name}}" />
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label">Price</label>
                <div class="col-sm-10">
                    <input class="form-control" placeholder="Price" type="number" name="price" value="{{$post->price}}" />
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label">Description</label>
                <div class="col-sm-10">
                    <!--<textarea class="form-control" rows="3" placeholder="Description" id="textEditor" name="">{{$post->description}}</textarea>-->
                    <textarea class="form-control" rows="3" placeholder="Description" name="description">{{$post->description}}</textarea>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label">Image</label>
                <div class="col-sm-10">
                    <div class="row">
                        <div class="col-sm-4">
                            <div class="btn btn-default btn-file">
                                <i class="fa fa-paperclip"></i> Attachment
                                <input type="file" name="image" />
                            </div>
                        </div>
                        @if ($post->image != "")
                         <div class="col-md-8" id="categoryImageCont">
                             <img src="{{ asset("img/sign_boards/".getFolderStructureByDate($post->created_at)."/orignal/".$post->image) }}" 
                                  height="150" style="vertical-align: top;"/>
                         </div>
                        @endif
                    </div>
                </div>
            </div>

        </div><!-- /.box-body -->
        <div class="box-footer">
            <input class="btn btn-primary pull-right" type="submit" value="Save"/>
        </div><!-- /.box-footer -->
    </form>
    <div class="overlay" style="display: none;">

      <i class="fa fa-refresh fa-spin"></i>

    </div><!-- Box Overlay -->
</div><!-- /.box -->

<style>
.registrationFormCont {

}
.registrationFormCont h1 {
    font-size: 24px;
}
.registrationFormCont label {
    font-size: 16px;
    font-weight: normal;
    margin-top: 8px;
}
.registrationFormCont .registrationFormFieldCont .radioBtnCont + .radioBtnCont {
	margin-left: 20px;
}
.registrationFormCont .registrationFormFieldCont .input-group .input-group-addon {
    background: #8aa4af;
}
.registrationFormCont .registrationFormFieldCont .input-group .customSelect {
    z-index: auto;
}
.registrationFormCont .registrationFormFieldCont .input-group .customSelect .dropdown-toggle {
    background: #fff;
    height: 46px;
}
.registrationFormCont .registrationFormFieldCont .input-group .customSelect .dropdown-menu {
    z-index: 3;
}
</style>
@endsection
