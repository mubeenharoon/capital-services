@extends('templates.template')
@section('title_and_meta')
<title>Capital Sign Services</title>
@endsection

@section('page_content')

                <div class="we-are-con">
                    <div class="row">
                        <div class="col-md-12 text-center">
                            <h2 class="who-are-head">WHO <span class="grn-it">WE ARE</span></h2>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="row">
                                <div class="col-md-6 we-are-text-con">
                                    <h3 class="head-we-are">Established in 1992</h3>
                                    <div class="we-are-para">
                                        <p>Lorem <span class="grn-it italicize-it">ipsum dolor</span> sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>

                                        <p>Cras a sem vestibulum diam <span class="grn-it underline-it"><strong>faucibus vestibulum.</strong></span> Quisque et mi dictum, tristique libero finibus, facilisis turpis. Nulla sem turpis, iaculis vel felis vel, gravida fringilla justo. Vestibulum eu lectus non risus convallis interdum sed vel sem.</p>
                                    </div>
                                    <button class="join-me-btn">JOIN MY TEAM</button>
                                </div>
                                <div class="col-md-6 we-are-img-con">
                                    <img src="{{asset('img/we-are.png')}}">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="we-are-con services-con">
                    <div class="row">
                        <div class="col-md-12 text-center">
                            <h2 class="who-are-head services-con-head">Our <span class="grn-it">Services</span></h2>
                            <div class="service-sub-head">Pastry liquorice sesame snaps</div>
                            <div class="service-gallery-tab">
                                <ul class="nav nav-tabs">
                                    {{--*/ $GalleryLoopCounter = 0; /*--}}
                                    @foreach($GalleryCategories as $GalleryCategory)
                                        {{--*/ $GalleryLoopCounter++; /*--}}
                                        <li class="{{$GalleryLoopCounter == 1 ? 'active' : ''}}">
                                            <a data-toggle="tab" href="#gallery{{$GalleryCategory->id}}">{{$GalleryCategory->name}}</a>
                                        </li>
                                    @endforeach
<!--                                    <li><a data-toggle="tab" href="#menu2">photography</a></li>
                                    <li><a data-toggle="tab" href="#menu3">modeling</a></li>
                                    <li><a data-toggle="tab" href="#menu4">graphic design</a></li>-->
                                </ul>
                                <div class="tab-content">
                                    {{--*/ $GalleryLoopCounter = 0; /*--}}
                                    @foreach($GalleryCategories as $GalleryCategory)
                                        {{--*/ $GalleryLoopCounter++; /*--}}
                                        {{--*/ $GalleryPath = url("img/gallery/".getFolderStructureByDate($GalleryCategory->created_at)."/small/"); /*--}}
                                        <div id="gallery{{$GalleryCategory->id}}" class="tab-pane fade {{$GalleryLoopCounter == 1 ? 'active in' : ''}}">
                                            <!-- Start Tab Content -->
                                            <div class="row">
                                                @foreach($GalleryImages[$GalleryCategory->id] as $GalleryImage)
                                                <div class="col-md-3">
                                                    <div class="ser-img-con">
                                                        <img src="{{$GalleryPath."/".$GalleryImage}}" alt="" height="400" width="400" />
                                                    </div>
                                                </div>
                                                @endforeach
                                            </div>
                                            <!-- End Tab Content -->
                                        </div>
                                    @endforeach
                                </div>
                            </div>
                            <div class="service-btn">
                                <a href="#">SHOW ME MORE</a>
                            </div>
                        </div>
                    </div>
                </div>

@endsection