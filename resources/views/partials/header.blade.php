
        <div class="container" id="headerTopCon">
            <div class="row">
                <div class="col-md-4">
                    <div class="logo-image-con">
                        <div class="logo-image">
                            <img src="{{url('img/logo.png')}}">
                        </div>
                    </div>
                </div>
                <div class="col-md-8" style="margin-top: 15px;">
                    <div class="head-menu-con">

                        <nav class="navbar navbar-inverse">
                            <div class="container-fluid">
                                <div class="navbar-header">
                                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
                                        <span class="icon-bar"></span>
                                        <span class="icon-bar"></span>
                                        <span class="icon-bar"></span>                        
                                    </button>
                                </div>
                                <div class="collapse navbar-collapse" id="myNavbar">
                                    <ul class="nav navbar-nav navbar-right">
                                        <li class="{{activeMenu('/')}}"><a href="{{url('/')}}">Home</a></li>
                                        <!-- <li class="dropdown">
                                          <a class="dropdown-toggle" data-toggle="dropdown" href="#">Page 1 <span class="caret"></span></a>
                                          <ul class="dropdown-menu">
                                            <li><a href="#">Page 1-1</a></li>
                                            <li><a href="#">Page 1-2</a></li>
                                            <li><a href="#">Page 1-3</a></li>
                                          </ul>
                                        </li> -->
                                        <li><a href="#">Services</a></li>
                                        <li><a href="#">Gallery</a></li>
                                        <li><a href="#">Contact Us</a></li>
                                    </ul>
                                    <!-- <ul class="nav navbar-nav navbar-right">
                                      <li><a href="#"><span class="glyphicon glyphicon-user"></span> Sign Up</a></li>
                                      <li><a href="#"><span class="glyphicon glyphicon-log-in"></span> Login</a></li>
                                    </ul> -->
                                </div>
                            </div>
                        </nav>

                    </div>

                </div>
            </div>
        </div>
