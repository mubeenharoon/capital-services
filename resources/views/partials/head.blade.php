        
        
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <link href="https://fonts.googleapis.com/css?family=Raleway:700,800" rel="stylesheet">

        <!-- BOOTSTRAP CORE STYLE  -->
        <link href="{{asset('/plugins/bootstrap/css/bootstrap.min.css')}}" rel="stylesheet" />
        <!-- FONT AWESOME ICONS  -->
        <link href="{{asset('/plugins/font-awesome/css/font-awesome.css')}}" rel="stylesheet" />
        <!-- FLEX SLIDER -->
        <link rel="stylesheet" href="{{asset('plugins/flexslider/flexslider.css')}}" type="text/css" media="screen" />
        <!-- CUSTOM STYLESHEET -->
        <link rel="stylesheet" href="{{asset('/css/custom.css')}}" type="text/css">


        
        