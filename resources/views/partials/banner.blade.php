

        <section class="sliderCon loading">
            <div class="flexslider">
                <ul class="slides">
                    <li>
                        <div class="slideCon">
                            <div class="container">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="slideHeadingOne">
                                            <h2>real estate sign<br>installation &amp;<br>management</h2>
                                        </div>
                                        <div class="slideButtons">
                                            <a href="{{url('auth/register')}}" class="regBtn">REGISTER</a>
                                            <a href="{{url('auth/login')}}" class="loginBtn">LOGIN</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <img class="sign-board" src="{{url('img/sign-board.png')}}" />
                        <img src="{{url('img/slide-1.jpg')}}" />
                    </li>
                    <!-- <li>
                      <img src="img/slide-2.jpg" />
                    </li>
                    <li>
                      <img src="img/slide-3.jpg" />
                    </li> -->
                </ul>
            </div>
        </section>
