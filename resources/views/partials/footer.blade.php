

        <footer>
            <div class="container">
                <div class="row footer-row-1">
                    <div class="col-md-4 fotter-col-1">
                        <h3 class="footer-heads">Gatsby PSD Template</h3>
                        <p>Gatsby PSD Template</p>
                        <h3 class="footer-heads">Subscribe</h3>
                        <form method="POST" class="subscription">
                            <input type="email" name="email" placeholder="info@samplemail.com">
                            <button type="submit"><img src="{{url('img/contact.png')}}"></button>
                        </form>
                    </div>
                    <div class="col-md-4 fotter-col-2">
                        <h3 class="footer-heads">Latest tweets</h3>
                        <p>@gtsby Win a signed poster from the Terminator Genisys: Guardian mobile game! Click for details - <a href="http://bit.ly/1X4">http://bit.ly/1X4</a></p>

                        <p>@gtsby A long time ago... Guess where? <a href="https://youtu.be/klEU2vL_HpA">https://youtu.be/klEU2vL_HpA</a></p>
                    </div>
                    <div class="col-md-4 fotter-col-3">
                        <h3 class="footer-heads">Instagram</h3> 
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <hr style="border-color: #444444;">
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12 footer-menu">
                        <div class="row">
                            <ul class="col-md-8">
                                <li><a href="#">Product</a></li>
                                <li><a href="#">Features</a></li>
                                <li><a href="#">Pricing</a></li>
                                <li><a href="#">Contacts</a></li>
                                <li><a href="#">About</a></li>
                            </ul>
                            <div class="col-md-4 footer-social">
                                <span>
                                    <a class="li-ftr" href="#" target="_blank"></a>
                                    <a class="twi-ftr" href="#" target="_blank"></a>
                                    <a class="vee-ftr" href="#" target="_blank"></a>
                                    <a class="fb-ftr" href="#" target="_blank"></a>
                                    <a class="dots-ftr" href="#" target="_blank"></a>
                                </span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </footer>
