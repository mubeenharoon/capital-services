<div class="col-md-12">
    <div class="right_side_bar text-left">
        <span>Special Offer </span>
    </div>
</div>
<div class="col-md-12">

    @if(count($special_offers)==0)
    <div class="triangle">
        <div class="empty"></div>
    </div>
    <div class="similar text-center">
        <div class="row similar_main product_item" >
            Not Found
        </div>
    </div>    
    @else
    <div class="triangle">
        <div class="empty"></div>
    </div>
    <div class="similar text-left">
        @foreach($special_offers as $special_offer)
        <div class="row similar_main product_item">
            <div class="col-md-4 col-xs-4 col-sm-4 similarproduct_img">
                <a href="{{url('product/'.$special_offer->slug.'/'.$special_offer->id)}}">
                    @if($special_offer->image != null)
                    <img src="{{ URL::to('/') }}/img/products/{{getFolderStructureByDate($special_offer->created_at)}}/orignal/{{$special_offer->image}}" alt="">
                    @else
                    <img src="{{asset('images/placeholders/company.png')}}" alt="">
                    @endif

                </a>
            </div>
            <div class="col-md-8 col-xs-8 col-sm-8">
                <a href="{{url('product/'.$special_offer->slug.'/'.$special_offer->id)}}">
                    <h1 title="{{$special_offer->title}}">
                        {{ str_limit($special_offer->title, 40) }}
                    </h1>
                </a>
                <p>

                    {{$special_offer->size}}  
                </p>
                <p>
                    @if($special_offer->bundle_qnty)
                    {{$special_offer->bundle_qnty}} PCS
                    @else
                    1 PCS    
                    @endif
                </p>
                @if($special_offer->discount_type == 1)
                {{--*/$discount_price =$special_offer->product_price - $special_offer->discount /*--}}
                <p><span class="oldprice">{{$special_offer->product_price}} AED</span> <span class="new-price">{{$discount_price}} AED</span></p>
                @elseif($special_offer->discount_type == 2)
                {{--*/$discount =$special_offer->product_price*($special_offer->discount/100) /*--}}
                {{--*/$discount_price =$special_offer->product_price - $discount /*--}}
                <p><span class="oldprice">{{$special_offer->product_price}} AED</span> <span class="new-price">{{$discount_price}} AED</span></p>
                @endif   


            </div>
        </div>
        @endforeach
        <div class="sidbarbtn similar_main product_item">
            <a href="{{url('search?special_offer')}}" class="right_side_bar text-center">View All Offers</a>
        </div>
    </div>
    @endif
</div>
<div class="col-md-12 facebook-panel">
    <div class="right_side_bar facbook-title text-left">
        <span class="f">f</span><span>Facebook </span>
    </div>
</div>
<div class="col-md-12 ">
    <div class="triangle">
        <div class="empty"></div>
    </div>
    <div class="facebook-like">
        <div class="fb-page" data-href="https://www.facebook.com/Water-Online-UAE-1159860754104677/" data-tabs="timeline" data-small-header="false" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true"><blockquote cite="https://www.facebook.com/PAK-Sport-141523412604616/" class="fb-xfbml-parse-ignore"><a href="https://www.facebook.com/PAK-Sport-141523412604616/">Facebook</a></blockquote></div>
    </div>
</div>