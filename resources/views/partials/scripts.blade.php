

        <!-- jQuery -->
<!--        <script src="http://ajax.googleapis.com/ajax/libs/jquery/1/jquery.min.js"></script>
        <script>window.jQuery || document.write('<script src="js/libs/jquery-1.7.min.js">\x3C/script>')</script>-->

        <!-- JAVASCRIPT AT THE BOTTOM TO REDUCE THE LOADING TIME  -->
        <!-- CORE JQUERY SCRIPTS -->
        <script src="{{asset('/plugins/jQuery/jQuery-2.1.4.min.js')}}"></script>
        <!-- Bootstrap -->
        <script src="{{asset('/plugins/bootstrap/js/bootstrap.min.js')}}"></script>
        <!-- FlexSlider -->
        <script defer src="{{asset('/plugins/flexslider/jquery.flexslider-min.js')}}"></script>
        
        <script type="text/javascript">

            $(window).load(function () {
                $('.flexslider').flexslider({
                    animation: "slide",
                    slideshow: true,
                    mousewheel: false,
                    keyboard: true,
                    touch: true,
                    pauseOnHover: true,
                    controlNav: false,
                    start: function (slider) {
                        $('.sliderCon').removeClass('loading');
                    }
                });
            });
        </script>