
@if (count($errors) > 0 || session('message'))
<div class="panel panel-default">
    <div class="panel-body">
        @if (session('message'))
        <div class="alert alert-success">
            <div class="message_div">
                <span>{{ session('message') }}</span>
            </div>
        </div>
        @else
        <div class="alert alert-danger">            
            <div class="message_div">
                <span><strong>Whoops!</strong> There were some problems.</span><br>
                <ul>
                    @foreach ($errors->all() as $error)
                    <li>{!! $error !!}</li>
                    @endforeach
                </ul>
            </div>
        </div>
        @endif
    </div>
</div>
@endif
