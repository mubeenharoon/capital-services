@extends('user.templates.template')

@section('page_title') Installation @endsection

@section('page_content')
                <div class="row">
                    <div class="col-md-12">
                        <h4 class="page-head-line">Profile</h4>
                    </div>
                </div>
                <form action="{{url(USER_PREFIX.'/profile')}}" method="post">
                    <input type="hidden" name="id" value="{{$post->id}}" />
                    <div class="row">
                        <div class="col-md-12">@include('user.partials.form_errors')</div>
                        <div class="col-md-6">
                            <div class="row">
                                <div class="col-md-6">
                                    <label>First Name : </label>
                                    <input type="text" name="first_name" class="form-control" placeholder="First Name" 
                                           value="{{get_none_empty_string(old('first_name'), $post->first_name)}}" />
                                </div>
                                <div class="col-md-6">
                                    <label>Last Name :  </label>
                                    <input type="text" name="last_name" class="form-control" placeholder="Last Name" 
                                           value="{{get_none_empty_string(old('last_name'), $post->last_name)}}" />
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <label>Phone Number : </label>
                                    <input type="text" name="phone_number" class="form-control" placeholder="Phone Number" 
                                           value="{{get_none_empty_string(old('phone_number'), $post->phone_number)}}" />
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <label>Address : </label>
                                    <input type="text" name="address" class="form-control" placeholder="Address" 
                                           value="{{get_none_empty_string(old('address'), $post->address)}}" />
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-9">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <label>City : </label>
                                            <input type="text" name="city" class="form-control" placeholder="City" 
                                           value="{{get_none_empty_string(old('city'), $post->city)}}" />
                                        </div>
                                        <div class="col-md-6">
                                            <label>State :  </label>
                                            <input type="text" name="state" class="form-control" placeholder="State" 
                                           value="{{get_none_empty_string(old('state'), $post->state)}}" />
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <label>ZipCode :  </label>
                                    <input type="text" name="zipcode" class="form-control" placeholder="ZipCode" 
                                           value="{{get_none_empty_string(old('zipcode'), $post->zipcode)}}" />
                                </div>
                            </div>
                        </div>
                        <div class="col-md-5 col-md-offset-1">
                            <label>Company Name : </label>
                            <input type="text" name="company_name" class="form-control" placeholder="Company Name" 
                                   value="{{get_none_empty_string(old('company_name'), $post->company_name)}}" />
                            <label>Email Address : </label>
                            <input type="email" name="email" class="form-control" placeholder="E-Mail Address" value="{{$post->email}}" disabled/>
                            <label>Password :  </label>
                            <input type="password" name="password" class="form-control" placeholder="Password" value="{{old('password')}}" />
                            <label>Confirm Password :  </label>
                            <input type="password" name="password_confirmation" class="form-control" placeholder="Password" 
                                   value="{{old('password_confirmation')}}" />
                        </div>
                    </div>
                    <hr />
                    <button type="submit" class="btn btn-default">Update</button>

                </form>

@endsection
