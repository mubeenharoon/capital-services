@extends('user.templates.template')

@section('page_title') Installation @endsection

@section('page_content')
                <div class="row">
                    <div class="col-md-12">
                        <h4 class="page-head-line">New Installation</h4>
                    </div>
                </div>
                <form action="{{url('user/installation')}}" method="post">
                    <input type="hidden" name="id" value="{{$post->id}}" />
                    <div class="row">
                        <div class="col-md-12">@include('user.partials.form_errors')</div>
                        <div class="col-md-6">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="panel panel-default">
                                        <div class="panel-heading"><b>Installation Details</b></div>
                                        <div class="panel-body">
                                            <!-- has-success has-warning has-error -->
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group {{false ? 'has-success' : ''}}">
                                                        <label for="job_date">Job Date</label>
                                                        <input type="text" class="form-control datepicker" name="job_date" placeholder="Job Date" 
                                                               value="{{get_none_empty_string(old('job_date'), $post->job_date)}}"/>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label for="expiry_date">Expiry Date</label>
                                                        <input type="text" class="form-control datepicker" name="expiry_date" placeholder="Expiry Date" 
                                                               value="{{get_none_empty_string(old('expiry_date'), $post->expiry_date)}}"/>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label for="phone_number">Phone Number</label>
                                                        <input type="text" class="form-control" name="phone_number" placeholder="Phone Number" 
                                                               value="{{get_none_empty_string(old('phone_number'), $post->phone_number)}}"/>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label for="property_type">Property Type</label>
                                                        {{--*/ $property_type_selected = get_none_empty_string(old('property_type'), $post->property_type); /*--}}
                                                        <select class="form-control" name="property_type">
                                                            <option value=""> - Please Select - </option>
                                                            @foreach($PropertyTypes as $PropertyType)
                                                                <option {{$property_type_selected == $PropertyType->id ? 'selected' : ''}} 
                                                                        value="{{$PropertyType->id}}">{{$PropertyType->name}}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label for="home_owner">Home Owner</label>
                                                        <input type="text" class="form-control" name="home_owner" placeholder="Home Owner" 
                                                               value="{{get_none_empty_string(old('home_owner'), $post->home_owner)}}"/>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label>Sprinklers </label>
                                                        {{--*/ $is_sprinklers_selected = get_none_empty_string(old('is_sprinklers'), $post->is_sprinklers); /*--}}
                                                        <div class="radio">
                                                            <label>
                                                                <input type="radio" name="is_sprinklers" value="1" {{$is_sprinklers_selected == 1 ? 'checked' : ''}}/>
                                                                Yes
                                                            </label> &nbsp;&nbsp;&nbsp; 
                                                            <label>
                                                                <input type="radio" name="is_sprinklers" value="0" {{$is_sprinklers_selected == 0 ? 'checked' : ''}}/>
                                                                No
                                                            </label>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label>Permit Required</label>
                                                        {{--*/ $is_permit_selected = get_none_empty_string(old('is_permit'), $post->is_permit); /*--}}
                                                        <div class="radio">
                                                            <label>
                                                                <input type="radio" name="is_permit" value="1" {{$is_permit_selected == 1 ? 'checked' : ''}}/>
                                                                Yes
                                                            </label> &nbsp;&nbsp;&nbsp; 
                                                            <label>
                                                                <input type="radio" name="is_permit" value="0" {{$is_permit_selected == 0 ? 'checked' : ''}}/>
                                                                No
                                                            </label>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label for="permit_number">Permit Number</label>
                                                        <input type="text" class="form-control" name="permit_number" placeholder="Permit Number" 
                                                               value="{{get_none_empty_string(old('permit_number'), $post->permit_number)}}"/>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <label for="instructions">Specific Instructions</label>
                                                        <textarea class="form-control" name="instructions" rows="3" placeholder="Specific Instructions"
                                                                  >{{get_none_empty_string(old('instructions'), $post->instructions)}}</textarea>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="panel panel-default">
                                        <div class="panel-heading"><b>Rentable Riders</b></div>
                                        <div class="panel-body">
                                            <!-- has-success has-warning has-error -->
                                            <div class="row">
                                                <?php
                                                $old_order_items = old('order_items') ? array_column(old('order_items'), 'id') : [];
                                                $order_items_selected = get_none_empty_string($old_order_items, $order_items_ids);
                                                ?>
                                                @forelse ($signbards as $signbard)
                                                <div class="col-md-4">
                                                    <div class="installation_products_cont">
                                                        <label>
                                                            <span class="product_title">{{$signbard->name}}</span>
                                                            <span class="product_image">
                                                                <input type="hidden" name="order_items[{{$signbard->id}}][price]" 
                                                                       value="{{$signbard->price}}" />
                                                                <input type="hidden" name="order_items[{{$signbard->id}}][name]" 
                                                                       value="{{$signbard->name}}" />
                                                                <input type="checkbox" name="order_items[{{$signbard->id}}][id]" value="{{$signbard->id}}"
                                                                       {{(in_array($signbard->id, $order_items_selected)) ? 'checked' : ''}}/>
                                                                <img src="{{url("img/sign_boards/".getFolderStructureByDate($signbard->created_at)."/orignal/".$signbard->image)}}" 
                                                                     alt=""/>
                                                            </span>
                                                            <span>${{$signbard->price}}</span>
                                                        </label>
                                                    </div>
                                                </div>
                                                @empty
                                                <div class="col-md-12">Sorry no Rentable Riders Available at the moment.</div>
                                                @endforelse
                                            </div>
                                            <br/>
                                            <button type="submit" class="btn btn-default">Save And Pay</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="panel panel-default">
                                <div class="panel-heading"><b>Address and Post Option</b></div>
                                <div class="panel-body">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label for="address">Address</label>
                                                <input type="text" class="form-control" name="address" placeholder="Address" 
                                                       value="{{get_none_empty_string(old('address'), $post->address)}}"/>
                                            </div>
                                        </div>
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label for="cross_street">Cross Street</label>
                                                <input type="text" class="form-control" name="cross_street" placeholder="Cross Street" 
                                                       value="{{get_none_empty_string(old('cross_street'), $post->cross_street)}}"/>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="city">City</label>
                                                <input type="text" class="form-control" name="city" placeholder="City" 
                                                       value="{{get_none_empty_string(old('city'), $post->city)}}"/>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="state">State</label>
                                                <input type="text" class="form-control" name="state" placeholder="State" 
                                                       value="{{get_none_empty_string(old('state'), $post->state)}}"/>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label for="zipcode">Zip Code</label>
                                                        <input type="text" class="form-control" name="zipcode" placeholder="Zip Code" 
                                                               value="{{get_none_empty_string(old('zipcode'), $post->zipcode)}}"/>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-12">
                                            <h5>Land listings must have a flag marking the install location.</h5><hr/>
                                        </div>
                                        <div class="col-md-12">
                                            <h4><b>Post Options</b></h4>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label for="no_of_signboards">Quantity</label>
                                                        <input type="number" class="form-control" name="no_of_signboards" placeholder="Quantity" 
                                                               value="{{get_none_empty_string(old('no_of_signboards'), $post->no_of_signboards)}}"/>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label for="signboard_size">Size</label>
                                                        {{--*/ $signboard_size_selected = get_none_empty_string(old('signboard_size'), $post->signboard_size); /*--}}
                                                        <select class="form-control" name="signboard_size">
                                                            <option value=""> - Please Select - </option>
                                                            <option {{$signboard_size_selected == '4 Foot' ? 'selected' : ''}}>4 Foot</option>
                                                            <option {{$signboard_size_selected == '6 Foot' ? 'selected' : ''}}>6 Foot</option>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-12">
                                            <hr/><h5><b>Optional Elements</b></h5>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label>Brochure Box</label>
                                                        {{--*/ $is_brochure_box_selected = get_none_empty_string(old('is_brochure_box'), $post->is_brochure_box); /*--}}
                                                        <div class="radio">
                                                            <label>
                                                                <input type="radio" name="is_brochure_box" value="1" {{$is_brochure_box_selected == 1 ? 'checked' : ''}}/>
                                                                Yes
                                                            </label> &nbsp;&nbsp;&nbsp; 
                                                            <label>
                                                                <input type="radio" name="is_brochure_box" value="0" {{$is_brochure_box_selected == 0 ? 'checked' : ''}}/>
                                                                No
                                                            </label>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label>Name Rider</label>
                                                        {{--*/ $is_name_rider_selected = get_none_empty_string(old('is_name_rider'), $post->is_name_rider); /*--}}
                                                        <div class="radio">
                                                            <label>
                                                                <input type="radio" name="is_name_rider" value="1" {{$is_name_rider_selected == 1 ? 'checked' : ''}}/>
                                                                Yes
                                                            </label> &nbsp;&nbsp;&nbsp; 
                                                            <label>
                                                                <input type="radio" name="is_name_rider" value="0" {{$is_name_rider_selected == 0 ? 'checked' : ''}}/>
                                                                No
                                                            </label>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>

@endsection
