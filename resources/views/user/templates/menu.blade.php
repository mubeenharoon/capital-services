
        @if (!Auth::Guest())
        <section class="menu-section">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="navbar-collapse collapse ">
                            <ul id="menu-top" class="nav navbar-nav">
                                <li><a class="menu-top-active" href="#">Dashboard</a></li>
                                <li><a href="#">Installation</a></li>
                                <li><a href="#">My Posts</a></li>
                                <li><a href="#">Workorders</a></li>
                                 <li><a href="#">My Account</a></li>

                            </ul>
                        </div>
                    </div>

                </div>
            </div>
        </section>
        <!-- MENU SECTION END-->
        @endif
        