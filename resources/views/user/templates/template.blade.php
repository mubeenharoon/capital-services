<!DOCTYPE html>
<html>

    @include('user.partials.htmlheader')

    <body>

        @include('user.partials.mainheader')
        @include('user.partials.menu')

        <div class="content-wrapper">
            <div class="container">
                @yield('page_content')
            </div>
        </div>
        <!-- CONTENT-WRAPPER SECTION END-->

        @include('user.partials.footer')
        @include('user.partials.scripts')
    </body>
</html>