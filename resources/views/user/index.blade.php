@extends('user.templates.template')

@section('page_title') Dashboard @endsection

@section('page_content')
                <div class="row">
                    <div class="col-md-12">
                        <h4 class="page-head-line">Dashboard</h4>

                    </div>

                </div>
                <div class="row">
                    <div class="col-md-12">
                        <a href="{{url(USER_PREFIX.'/installation/add')}}" class="btn btn-success"><span class="glyphicon glyphicon-plus"></span> New Installation </a>
                    </div>
                </div>
                
@endsection
