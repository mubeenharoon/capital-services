@extends('user.templates.template')

@section('page_title') Dashboard @endsection

@section('page_content')
                <div class="row">
                    <div class="col-md-12">
                        <h4 class="page-head-line">Installation Details</h4>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-8">
                        <p>Job ID: {{$post->id}}<br/>Job Address:</p>
                        <h4>{{$post->address}} {{$post->city}}, {{$post->state}} {{$post->zipcode}}</h4>
                        <table>
                            <tr>
                                <td width='20'>&nbsp;</td>
                                <td width="170"><b>Cross street:</b></td>
                                <td>{{$post->cross_street}}</td>
                            </tr>
                            <tr>
                                <td></td>
                                <td><b>Home Owner:</b></td>
                                <td>{{$post->home_owner}}</td>
                            </tr>
                            <tr>
                                <td></td>
                                <td><b>Property Type:</b></td>
                                <td>{{$post->proper_type_name}}</td>
                            </tr>
                        </table>
                        <hr>
                        <table class="jobdetails">
                            <tr>
                                <td width='20'>&nbsp;</td>
                                <td width="170"><b>Number of Signboards: </b></td>
                                <td>{{$post->no_of_signboards}}</td>
                            </tr>
                            <tr>
                                <td></td>
                                <td><b>Signboard Size:</b></td>
                                <td>{{$post->signboard_size}}</td>
                            </tr>
                            <tr>
                                <td></td>
                                <td><b>Brochure box:</b></td>
                                <td>{{$post->is_brochure_box ? 'Yes' : 'No'}}</td>
                            </tr>
                            <tr>
                                <td></td>
                                <td><b>Name Rider:</b></td>
                                <td>{{$post->is_name_rider ? 'Yes' : 'No'}}</td>
                            </tr>
                        </table>
                    </div>
                    <div class="col-md-4">
                        <div class="panel panel-default">
                            <div class="panel-body">
                                <b>Specific Instructions:</b><br/>
                                {{$post->instructions}}
                            </div>
                        </div>
                        <h4>Job Details</h4>
                        <p>
                            Created at: {{date('Y-m-d', strtotime($post->created_at))}}<br/>
                            Updated at: {{date('Y-m-d', strtotime($post->updated_at))}}<br/>
                            Accepted on: <br/>
                            Completed on: <br/>
                            Job Date: {{date('Y-m-d', strtotime($post->job_date))}}<br/>
                            Expiration: {{date('Y-m-d', strtotime($post->expiry_date))}}<br/>
                        </p>
                    </div>
                </div>
                <hr>
                <div class="row">
                    @forelse ($pos_items as $pos_item)
                    <div class="col-md-3">
                        <div class="installation_products_view_cont">
                            <div class="installation_products_box">
                                <span class="product_image">
                                    <img src="{{url("img/sign_boards/".getFolderStructureByDate($pos_item->created_at)."/orignal/".$pos_item->image)}}" 
                                         alt=""/>
                                </span>
                                <span class="product_title">{{$pos_item->name}}</span>
                            </div>
                        </div>
                    </div>
                    @empty
                    <div class="col-md-12">No Products selected.</div>
                    @endforelse
                </div>

@endsection
