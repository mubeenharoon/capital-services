@extends('user.templates.template')

@section('page_title') Posts @endsection

@section('page_content')
                <div class="row">
                    <div class="col-md-12">
                        <h4 class="page-head-line">My Posts</h4>

                    </div>

                </div>
                <div class="row">
                    <div class="col-md-12">
                        @include('user.partials.form_errors')
                    </div>
                    <div class="col-md-12"><div class="box-body">
                        <table class="table table-bordered">
                          <thead>
                              <tr>
                                  <th width="40">No</th>
                                  <th width="100">Job Date</th>
                                  <th width="100">Expiery Date</th>
                                  <th width="490">Address</th>
                                  <th width="150">State</th>
                                  <th width="165">Status</th>
                                  <th width="148">Action</th>
                              </tr>
                          </thead>
                          <tbody>
                              @if (count($posts) > 0)
                                  {{--*/ $loopCounter = ($posts->currentPage() * $posts->perPage()) - $posts->perPage() /*--}}
                                  @foreach ($posts as $post)
                                      {{--*/ $loopCounter++ /*--}}
                                      <tr>
                                          <td>{{$loopCounter}}</td>
                                          <td>{{date('Y-m-d', strtotime($post->job_date))}}</td>
                                          <td>{{date('Y-m-d', strtotime($post->expiry_date))}}</td>
                                          <td>{{$post->address}}</td>
                                          <td>{{$post->state}}</td>
                                          <td>{{$post->status_name}}</td>
                                          <td class="text-right">
                                              <form action="{{url(USER_PREFIX."/posts")}}" method="POST">
                                                  <input type="hidden" name="_token" value="{{ csrf_token() }}"/>
                                                  <input type="hidden" name="_method" value="DELETE" />
                                                  <input type="hidden" name="id" value="{{$post->id}}"/>
                                                  @if ($post->status == 3)
                                                  <a class="btn btn-default" href="{{ url(USER_PREFIX."/installation/add?id=".$post->id) }}">
                                                      <i class="fa fa-edit"></i></a>
                                                  @endif
                                                  <a class="btn btn-default" href="{{ url(USER_PREFIX."/posts/".$post->id) }}">
                                                      <i class="fa fa-eye"></i></a>
                                                  <button type="submit" class="btn btn-danger" onclick="return confirm('Are you sure you want to delete?')">
                                                      <i class="fa fa-times"></i></button>
                                              </form>
                                          </td>
                                      </tr>
                                  @endforeach
                              @endif
                          </tbody>
                          <tfoot>
                              <tr>
                                  <th>No</th>
                                  <th>Job Date</th>
                                  <th>Expiery Date</th>
                                  <th>Address</th>
                                  <th>State</th>
                                  <th>Status</th>
                                  <th>Action</th>
                              </tr>
                          </tfoot>
                        </table>
                      </div>
                      <!-- /.box-body -->
                      <div class="box-footer clearfix">
                          <div id="example2_paginate" class="dataTables_paginate paging_simple_numbers">{!! $posts->render() !!}</div>
                      </div>
                        
                    </div>
                </div>
                
@endsection
