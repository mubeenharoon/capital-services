
    <head>
        <meta charset="utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
        <!--[if IE]>
            <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
            <![endif]-->
        <title> Captial Signs Services |  @yield('page_title', 'Your title here') </title>
        <!-- BOOTSTRAP CORE STYLE  -->
        <link href="{{asset('/plugins/bootstrap/css/bootstrap.min.css')}}" rel="stylesheet" />
        <!-- FONT AWESOME ICONS  -->
        <link href="{{asset('/plugins/font-awesome/css/font-awesome.css')}}" rel="stylesheet" />
        <!-- Date Picker -->
        <link rel="stylesheet" href="{{asset('plugins/datepicker/datepicker3.css')}}" type="text/css" media="screen" />
        <!-- CUSTOM STYLE  -->
        <link href="{{asset('/css/user/style.css')}}" rel="stylesheet" />
         <!-- HTML5 Shiv and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
            <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
            <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->
    </head>
