
        <!-- JAVASCRIPT AT THE BOTTOM TO REDUCE THE LOADING TIME  -->
        <!-- JQuery -->
        <script src="{{asset('/plugins/jQuery/jQuery-2.1.4.min.js')}}"></script>
        <!-- Bootstrap  -->
        <script src="{{asset('/plugins/bootstrap/js/bootstrap.min.js'  )}}"></script>
        <!-- Date Picker -->
        <script src="{{asset('/plugins/datepicker/bootstrap-datepicker.js')}}"></script>
        <!-- Custom Scripts -->
        <script src="{{asset('/js/user/custom.js')}}"></script>