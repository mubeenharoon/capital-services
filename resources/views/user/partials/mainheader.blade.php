
        <div class="navbar navbar-inverse set-radius-zero">
            <div class="container">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle pull-right" data-toggle="collapse" data-target=".navbar-collapse">
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="{{url('user')}}">

                        <img src="{{asset('/img/logo.png')}}" />
                    </a>

                </div>
                @if (!Auth::Guest())
                <div class="left-div pull-right">
                    <div class="user-settings-wrapper">
                        <ul class="nav">

                            <li class="dropdown">
                                <a class="dropdown-toggle" data-toggle="dropdown" href="#" aria-expanded="false">
                                    <span class="glyphicon glyphicon-user" style="font-size: 25px;"></span>
                                </a>
                                <div class="dropdown-menu dropdown-settings">
                                    <div class="media">
                                        <div class="text-center"><img src="{{asset('/img/avatar5.png')}}" alt="" class="img-rounded" /></div>
                                        <div class="">
                                            <h4 class="media-heading">{{Auth::user()->first_name}} {{Auth::user()->last_name}}</h4>
                                        </div>
                                    </div>
                                    <hr />
                                    <a href="#" class="btn btn-info btn-sm">Full Profile</a>&nbsp; 
                                    <a href="{{url('auth/logout')}}" class="btn btn-danger btn-sm">Logout</a>

                                </div>
                            </li>


                        </ul>
                    </div>
                </div>
                @endif
            </div>
        </div>
        <!-- LOGO HEADER END-->
