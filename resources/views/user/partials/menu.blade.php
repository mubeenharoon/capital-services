
        @if (!Auth::Guest())
        <section class="menu-section">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="navbar-collapse collapse ">
                            <ul id="menu-top" class="nav navbar-nav">
                                <li><a class="{{activeMenu(USER_PREFIX, 'menu-top-active')}}" href="{{url('user')}}">Dashboard</a></li>
                                <li>
                                    <a class="{{activeMenu(USER_PREFIX.'/installation/add', 'menu-top-active')}}" 
                                       href="{{url(USER_PREFIX.'/installation/add')}}">Installation</a>
                                </li>
                                <li>
                                    <a class="{{activeMenu(USER_PREFIX.'/posts', 'menu-top-active')}}" href="{{url(USER_PREFIX.'/posts')}}">My Posts</a>
                                </li>
                                <!--<li><a href="#">Workorders</a></li>-->
                                <li>
                                    <a class="{{activeMenu(USER_PREFIX.'/profile', 'menu-top-active')}}" href="{{url(USER_PREFIX.'/profile')}}">My Account</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- MENU SECTION END-->
        @endif
        