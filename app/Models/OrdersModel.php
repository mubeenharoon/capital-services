<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class OrdersModel extends Model {

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'orders';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['job_date', 'expiry_date', 'phone_number', 'no_of_signboards', 'signboard_size', 'address', 'cross_street', 'city', 'state', 
                            'zipcode', 'home_owner', 'property_type', 'is_permit', 'permit_number', 'is_sprinklers', 'instructions', 'is_brochure_box', 
                            'is_name_rider', 'total_amount', 'status'];
    
    public function order_items() {
        return $this->hasMany('App\Models\OrdersItemsModel', 'order_id');
    }

}
