<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TransactionModel extends Model {

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'transaction';

    public $timestamps = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['order_id', 'user_id', 'payment_id', 'amount'];

}
