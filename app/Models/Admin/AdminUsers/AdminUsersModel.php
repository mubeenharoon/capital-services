<?php namespace App\Models\Admin\AdminUsers;

use Illuminate\Database\Eloquent\Model;

class AdminUsersModel extends Model {
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'admin_users';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['first_name', 'last_name', 'email', 'password', 'role', 'status'];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */

}
