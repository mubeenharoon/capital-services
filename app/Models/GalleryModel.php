<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class GalleryModel extends Model {

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'gallery';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name'];

}
