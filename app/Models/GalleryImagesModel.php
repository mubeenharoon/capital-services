<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class GalleryImagesModel extends Model {

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'gallery_images';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['gallery_id'];

}
