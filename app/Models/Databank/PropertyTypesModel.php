<?php namespace App\Models\Databank;

use Illuminate\Database\Eloquent\Model;

class PropertyTypesModel extends Model {

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'dbk_property_types';

    public $timestamps = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name'];

}
