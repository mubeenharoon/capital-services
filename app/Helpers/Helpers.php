<?php

function activeMenu($uri = '', $active_class = 'active') {
    $active = '';
    if (Request::is(Request::segment(1) . '/' . $uri . '/*') || Request::is(Request::segment(1) . '/' . $uri) || Request::is($uri)) {
        $active = $active_class;
    }
    return $active;
}

function saveOrignalImage ($file, $destinationPath = 'img/temporary') {
    $extension = $file->getClientOriginalExtension();
    $fileName = uniqid().'.'.$extension;
    $file->move($destinationPath, $fileName);
    return $fileName;
}

function makeSlug($text) { 
    // replace non letter or digits by -
    $text = preg_replace('~[^\\pL\d]+~u', '-', $text);
    // trim
    $text = trim($text, '-');
    // transliterate
    $text = iconv('utf-8', 'us-ascii//TRANSLIT', $text);
    // lowercase
    $text = strtolower($text);
    // remove unwanted characters
    $text = preg_replace('~[^-\w]+~', '', $text);
    if (empty($text)) {
        return 'n-a';
    }
    return $text;
}

function getUniqueSlug ($slug, $slugsArray) {
    if (count($slugsArray) > 0) {
        $loopCounter = 0;
        while (true) {
            if ($loopCounter == 0) {
                if (!in_array("$slug", $slugsArray)) {
                    $slug = "$slug";break;
                }
            } else {
                if (!in_array("$slug-$loopCounter", $slugsArray)) {
                    $slug = "$slug-$loopCounter";break;
                }
            }
            $loopCounter++;
        }
    }
    return $slug;
}

function get_unique_slug_from_table ($string, $table, $id = '') {
    $id = (is_null($id)) ? '' : $id;
    $slug = makeSlug($string);
    $existingSlugs = DB::table($table)->where('slug', 'LIKE', "%$slug%")->where("id", "!=", $id)->lists('slug');
    
    $existingSlugs = array_column((count($existingSlugs) > 0) ? $existingSlugs : [], 'slug');
    $slug = getUniqueSlug($slug, $existingSlugs);
    return $slug;
}

function get_user_ip () {
    $ipaddress = '';
    if (filter_input(INPUT_SERVER, 'HTTP_CLIENT_IP') !== null) {
        $ipaddress = filter_input(INPUT_SERVER, 'HTTP_CLIENT_IP');
    } else if(filter_input(INPUT_SERVER, 'HTTP_X_FORWARDED_FOR') !== null) {
        $ipaddress = filter_input(INPUT_SERVER, 'HTTP_X_FORWARDED_FOR');
    } else if(filter_input(INPUT_SERVER, 'HTTP_X_FORWARDED') !== null) {
        $ipaddress = filter_input(INPUT_SERVER, 'HTTP_X_FORWARDED');
    } else if(filter_input(INPUT_SERVER, 'HTTP_FORWARDED_FOR') !== null) {
        $ipaddress = filter_input(INPUT_SERVER, 'HTTP_FORWARDED_FOR');
    } else if(filter_input(INPUT_SERVER, 'HTTP_FORWARDED') !== null) {
        $ipaddress = filter_input(INPUT_SERVER, 'HTTP_FORWARDED');
    } else if(filter_input(INPUT_SERVER, 'REMOTE_ADDR') !== null) {
        $ipaddress = filter_input(INPUT_SERVER, 'REMOTE_ADDR');
    } else {
        $ipaddress = 'UNKNOWN';
    }
    return $ipaddress;
}

function pr ($array) {
    echo "<pre>";print_r($array);echo "</pre>";
}

function get_status ($int) {
    switch ($int) {
        case 0:
            $status = "Pending";
            break;
        case 1:
            $status = "Approved";
            break;
        case 2:
            $status = "Rejected";
            break;
        default:
            $status = "Unknown";
            break;
    }
    return $status;
}

function whenDoYouNeedIt ($type, $specificDate = "Specific Date") {
    return ($type == 1) ? "I’m Flexible" : (($type == 2) ? "Within 48 Hours" : "$specificDate");
}

function whatIsThisPurchaseFor ($type) {
    return ($type) ? "One Time" : (($type == 2) ? "Recurring" : "Starting A Business");
}

function profileGetFieldsValues ($old, $actual) {
    return ($old) ? $old : $actual;
}

function get_none_empty_string ($primary_str, $secondary_str) {
//    var_dump($primary_str);exit;
    return ($primary_str) ? $primary_str : $secondary_str;
}

function getQuoteStatus ($type) {
    return ($type == 0) ? "Pending" : (($type == 1) ? "Accepted" : "Rejected");
}

function get_settings ($key = null) {
    if (!is_null($key)) {
        $settingObj = DB::table('settings')->select('value')->where('key', $key)->first();
        return $settingObj->value;
    }
}

function dateDiff($time1, $time2, $precision = 6) {
    // If not numeric then convert texts to unix timestamps
    if (!is_int($time1)) {
      $time1 = strtotime($time1);
    }
    if (!is_int($time2)) {
      $time2 = strtotime($time2);
    }
 
    // If time1 is bigger than time2
    // Then swap time1 and time2
    if ($time1 > $time2) {
      $ttime = $time1;
      $time1 = $time2;
      $time2 = $ttime;
    }
 
    // Set up intervals and diffs arrays
    $intervals = array('year','month','day','hour','minute','second');
    $diffs = array();
 
    // Loop thru all intervals
    foreach ($intervals as $interval) {
      // Create temp time from time1 and interval
      $ttime = strtotime('+1 ' . $interval, $time1);
      // Set initial values
      $add = 1;
      $looped = 0;
      // Loop until temp time is smaller than time2
      while ($time2 >= $ttime) {
        // Create new temp time from time1 and interval
        $add++;
        $ttime = strtotime("+" . $add . " " . $interval, $time1);
        $looped++;
      }
 
      $time1 = strtotime("+" . $looped . " " . $interval, $time1);
      $diffs[$interval] = $looped;
    }
    
    $count = 0;
    $times = array();
    // Loop thru all diffs
    foreach ($diffs as $interval => $value) {
      // Break if we have needed precission
      if ($count >= $precision) {
	break;
      }
      // Add value and interval 
      // if value is bigger than 0
      if ($value > 0) {
	// Add s if value is not 1
	if ($value != 1) {
	  $interval .= "s";
	}
	// Add value and interval to times array
	$times[] = $value . " " . $interval;
	$count++;
      }
    }
 
    // Return string with times
    return implode(" ", $times);
}

function getFolderStructureByDate ($date) {
    return date('Y', strtotime($date))."/".date('n', strtotime($date));
}

function makeMultipleSizesOfLogo ($fileName, $targetPath, $sizes = ['orignal'], $folderStructureDate = "", $tempLocation = 'img/temporary') {
    $dimensions = ['small' => [400, 400], 'thumb' => [150, 150]];
    if (empty($folderStructureDate)) {
        $folderStructureByDate = date('Y')."/".date('n');
    } else {
        $folderStructureByDate = getFolderStructureByDate($folderStructureDate);
    }
    $directoryPath = "$targetPath/$folderStructureByDate";
    foreach ($sizes as $size) {
//        echo $fileExtension = \File::extension(url("$tempLocation/$fileName"));exit;
        $directoryPathFull = "$directoryPath/$size/";

        if (!file_exists($directoryPathFull)) {
            mkdir($directoryPathFull, 0777, true);
        }

        if ($size == 'orignal') {
            // with orignal php
//            copy("$tempLocation/$fileName", $directoryPathFull.$fileName);
            Image::make("$tempLocation/$fileName")
            		->save($directoryPathFull.$fileName);
        } else {
            Image::make("$tempLocation/$fileName")
                  ->resize($dimensions[$size][0], null, function ($constraint) {$constraint->aspectRatio();})
		          ->save($directoryPathFull.$fileName, 60);
        }
    }
    unlink("$tempLocation/$fileName");
}
function makeMultipleSizesOfImage ($fileName, $targetPath, $sizes = ['orignal'], $folderStructureDate = "") {
    $dimensions = ['small' => [400, 400], 'thumb' => [150, 150]];
    $tempLocation = 'img/temporary';
    if (empty($folderStructureDate)) {
        $folderStructureByDate = date('Y')."/".date('n');
    } else {
        $folderStructureByDate = getFolderStructureByDate($folderStructureDate);
    }
    $directoryPath = "$targetPath/$folderStructureByDate";
    foreach ($sizes as $size) {
//        echo $fileExtension = \File::extension(url("$tempLocation/$fileName"));exit;
        $directoryPathFull = "$directoryPath/$size/";

        if (!file_exists($directoryPathFull)) {
            mkdir($directoryPathFull, 0777, true);
        }
        
        if ($size == 'orignal') {
            // with orignal php
//            copy("$tempLocation/$fileName", $directoryPathFull.$fileName);
            Image::make("$tempLocation/$fileName")
//                        ->insert(public_path().'/img/watermark.png', 'center', 10, 10)
            		->save($directoryPathFull.$fileName);
        } else {
            Image::make("$tempLocation/$fileName")
                  ->resize($dimensions[$size][0], null, function ($constraint) {$constraint->aspectRatio();})
		          ->save($directoryPathFull.$fileName, 60);
        }
    }
    unlink("$tempLocation/$fileName");
}
function deleteOldFiles ($directoryPath = 'img/temporary', $hours=24) {
    if ($handle = opendir( $directoryPath)) {
        while (false !== ($file = readdir($handle))) {
            $filelastmodified = filemtime($directoryPath."/".$file);
            if((time()-$filelastmodified) > $hours*3600 && is_file($directoryPath."/".$file)) {
                unlink($directoryPath."/".$file);
            }
        }
        closedir($handle);
    }
}

function removeSpecificFile ($name, $path, $sizes = ['orignal', 'small', 'thumb']) {
    if (empty($sizes)) {
        if (File::exists(public_path("$path/$name"))) {
            File::delete("$path/$name");
        }
    } else {
        foreach ($sizes as $size) {
            if (File::exists(public_path("$path/$size/$name"))) {
                File::delete("$path/$size/$name");
            }
        }
    }
}

function getSettingsVariable ($key = '') {
    $settings = ['siteTitleMain' => 'Games Cottage', 'siteTitle' => ' | Games Cottage'];
    return $settings[$key];
}

function getVideoEmbedURL ($url) {
    $embedURL = "";
    switch (true) {
        case strpos($url, 'youtube.com/'):
            $videoURLArray = explode("com/watch/", $url);
            if (strpos($url, 'watch?v=')) {
                $videoURLArray = explode("watch?v=", $url);
            }
            $videoEmbedID = $videoURLArray[1];
            $embedURL = "https://www.youtube.com/embed/$videoEmbedID";
            break;
        case strpos($url, 'dailymotion.com/'):
            $videoURLArray = explode(".com/video/", $url);
            $videoEmbedID = $videoURLArray[1];
            $embedURL = "//www.dailymotion.com/embed/video/$videoEmbedID";
            break;
        case strpos($url, 'vimeo.com/'):
            $videoURLArray = explode("vimeo.com/", $url);
            $videoEmbedID = $videoURLArray[1];
            $embedURL = "https://player.vimeo.com/video/$videoEmbedID";
            break;
        case strpos($url, 'tune.pk/'):
            $videoURLArray = explode("pk/video/", $url);
            $videoEmbedID = explode("/",$videoURLArray[1])[0];
            $embedURL = "http://tune.pk/player/embed_player.php?vid=$videoEmbedID&folder=&width=600&height=350&autoplay=no";
            break;
    }
    return "$embedURL";
}

function getIframeFromURL ($url, $width = 200, $height = 120) {
    return "<iframe width='$width' height='$height' frameborder='0' allowfullscreen src='".getVideoEmbedURL($url)."'></iframe>";;
}

function getUpcomingGameReleaseDate($date) {
    $dateArray = explode('-', $date);
    $releaseDate = "TBA";
    switch (true) {
        case (empty($dateArray[2]) && empty($dateArray[1]) && empty($dateArray[0])):
            $releaseDate = "TBA";
            break;
        case (empty($dateArray[2]) && empty($dateArray[1])):
            $releaseDate = "TBA, ".date('Y', strtotime($dateArray[2].'-01-01'));
            break;
        case (empty($dateArray[2])):
            $releaseDate = "TBA, ".date('F Y', strtotime($dateArray[2].'-'.$dateArray[1].'-01'));
            break;
        default:
            $releaseDate = date('F, j Y', strtotime($date));
            break;
    }
    return $releaseDate;
}

function getSidebarLatestNews () {
    return DB::table('posts as p')->leftJoin('games as g', DB::raw('FIND_IN_SET(g.id, p.games_ids)'), DB::raw(''), DB::raw(''))
                        ->select('p.id', 'p.name', 'p.slug', 'p.image', 'g.name as gameName', 'p.date_created')
                        ->whereRaw('FIND_IN_SET(1, p.category_ids)')->orderBy('p.id', 'DESC')->limit(5)->get();
}

function getSidebarLatestCheatCodes () {
    return DB::table('posts as p')->leftJoin('games as g', DB::raw('FIND_IN_SET(g.id, p.games_ids)'), DB::raw(''), DB::raw(''))
                        ->select('p.id', 'p.name', 'p.slug', 'p.image', 'g.name as gameName', 'p.date_created')
                        ->whereRaw('FIND_IN_SET(2, p.category_ids)')->orderBy('p.id', 'DESC')->limit(5)->get();
}

function getSidebarLatestUpcoming () {
    $upcomingReleaseDatesQuery = "SELECT rd.game_id, rd.platforms, p.id, rd.date, SUBSTRING_INDEX(rd.date, '-', 1) AS releaseYear, "
                            ."SUBSTRING_INDEX(SUBSTRING_INDEX(rd.date, '-', 2), '-', -1) AS releaseMonth, SUBSTRING_INDEX(rd.date, '-', -1) AS "
                            ."releaseDay, p.name from `release_date` `rd` RIGHT JOIN platforms p ON FIND_IN_SET(p.id, rd.platforms) WHERE "
                            ."rd.game_id having IF (releaseYear != '' AND releaseMonth != '' AND releaseDay != '', "
                            ."(DATE_FORMAT(CONCAT(releaseYear, '-', releaseMonth, '-', releaseDay), '%Y-%m-%d') > DATE_FORMAT(CURDATE(), "
                            ."'%Y-%m-%d')), (IF (releaseYear != '' AND releaseMonth != '', (DATE_FORMAT(CONCAT(releaseYear, '-', releaseMonth, "
                            ."'-28'), '%Y-%m-%d') > DATE_FORMAT(CURDATE(), '%Y-%m-%d')), (IF (releaseYear != '', releaseYear >= YEAR(CURDATE()), "
                            ."1)))))";
//            echo $upcomingReleaseDatesQuery;exit;
    return DB::table(DB::raw("($upcomingReleaseDatesQuery) as ud"))->leftJoin('games as g', 'g.id', '=', 'ud.game_id')
                                ->leftJoin(DB::raw('(SELECT id, name FROM games where parent_id = 0) as pg'), 'pg.id', '=', 'g.parent_id')
                                ->select('ud.*', 'g.name', 'g.slug', 'g.image', 'g.date_created', 'pg.name as parentName', 'g.release_date', 
                                'g.description', 'ud.name as platform')->orderBy('ud.id', 'DESC')
                                ->limit(5)->get();
}

function specialOffer($id = null , $for = null){
    if($id != null && $for == null){
        return DB::table('product as pro')
            ->leftJoin('brands as c', 'c.id', '=', 'pro.company')
            ->leftJoin('company as cmpny', 'cmpny.id', '=', 'c.company')
            ->leftJoin('product_size as s', 's.id', '=', 'pro.size')
            ->leftJoin('product_image as im', 'im.product_id', '=', 'pro.id')
            ->leftJoin('product_type as t', 't.id', '=', 'pro.type')
            ->select('pro.id', 'pro.title', 'pro.created_at', 's.name as size', 'im.name as image', 'pro.price as product_price', 'pro.bundle_price as bundle_prce', 'pro.bundle_quantity as bundle_qnty', 'pro.discount', 'pro.slug', 'pro.discount_type')
            ->groupBy('pro.id')
            ->where('pro.discount', '>', 0)
            ->where('pro.status', 1)
            ->where('cmpny.status', 1)
            ->where('c.status', 1)
            ->where('pro.company',$id)
            ->orderBy('c.name', 'ASC')
            ->take(4)
            ->get();  
    }elseif($id != null && $for == "product"){
        return DB::table('product as pro')
            ->leftJoin('brands as c', 'c.id', '=', 'pro.company')
            ->leftJoin('company as cmpny', 'cmpny.id', '=', 'c.company')
            ->leftJoin('product_size as s', 's.id', '=', 'pro.size')
            ->leftJoin('product_image as im', 'im.product_id', '=', 'pro.id')
            ->leftJoin('product_type as t', 't.id', '=', 'pro.type')
            ->select('pro.id', 'pro.title', 'pro.created_at', 's.name as size', 'im.name as image', 'pro.price as product_price', 'pro.bundle_price as bundle_prce', 'pro.bundle_quantity as bundle_qnty', 'pro.discount', 'pro.slug', 'pro.discount_type')
            ->groupBy('pro.id')
            ->where('pro.discount', '>', 0)
            ->where('pro.status', 1)
            ->where('cmpny.status', 1)
            ->where('c.status', 1)
            ->where('pro.id', '!=', $id)
            ->orderBy('c.name', 'ASC')
            ->take(4)
            ->get();  
    }else{
      return DB::table('product as pro')
            ->leftJoin('brands as c', 'c.id', '=', 'pro.company')
            ->leftJoin('company as cmpny', 'cmpny.id', '=', 'c.company')
            ->leftJoin('product_size as s', 's.id', '=', 'pro.size')
            ->leftJoin('product_image as im', 'im.product_id', '=', 'pro.id')
            ->leftJoin('product_type as t', 't.id', '=', 'pro.type')
            ->select('pro.id', 'pro.title', 'pro.created_at', 's.name as size', 'im.name as image', 'pro.price as product_price', 'pro.bundle_price as bundle_prce', 'pro.bundle_quantity as bundle_qnty', 'pro.discount', 'pro.slug', 'pro.discount_type')
            ->groupBy('pro.id')
            ->where('pro.discount', '>', 0)
            ->where('pro.status', 1)
            ->where('cmpny.status', 1)
            ->where('c.status', 1)
            ->orderBy('c.name', 'ASC')
            ->take(4)
            ->get();  
    }
    
}

function getSidebarLatestWallpapers () {
    return DB::table('gallery as g')->select('g.id', 'g.name', 'g.slug', 'g.image', 'g.created_at')->orderBy('g.id', 'DESC')->limit(4)->get();
}

function getSidebarLatestVideos () {
    return DB::table('video as v')->select('v.id', 'v.name', 'v.slug', 'v.video')->orderBy('v.id', 'DESC')->limit(3)->get();
}

function getLatestBreakingNews () {
    return DB::table('posts as p')->select('p.id', 'p.name', 'p.slug')->orderBy('p.id', 'DESC')->limit(10)->get();
}

function get_user_browser_and_windows () { 
    $u_agent = filter_input(INPUT_SERVER, 'HTTP_USER_AGENT');
    $bname = 'Unknown';
    $platform = 'Unknown';
    $version= "";

    //First get the platform?
    if (preg_match('/linux/i', $u_agent)) {
        $platform = 'linux';
    } elseif (preg_match('/macintosh|mac os x/i', $u_agent)) {
        $platform = 'mac';
    } elseif (preg_match('/windows|win32/i', $u_agent)) {
        $platform = 'windows';
    }

    // Next get the name of the useragent yes seperately and for good reason
    if (preg_match('/MSIE/i',$u_agent) && !preg_match('/Opera/i',$u_agent)) {
        $bname = 'Internet Explorer'; 
        $ub = "MSIE"; 
    } elseif (preg_match('/Firefox/i',$u_agent)) {
        $bname = 'Mozilla Firefox'; 
        $ub = "Firefox"; 
    } elseif(preg_match('/Chrome/i',$u_agent)) {
        $bname = 'Google Chrome'; 
        $ub = "Chrome"; 
    } elseif(preg_match('/Safari/i',$u_agent)) {
        $bname = 'Apple Safari'; 
        $ub = "Safari"; 
    } elseif(preg_match('/Opera/i',$u_agent)) {
        $bname = 'Opera'; 
        $ub = "Opera"; 
    } elseif(preg_match('/Netscape/i',$u_agent)) {
        $bname = 'Netscape'; 
        $ub = "Netscape"; 
    } 

    // finally get the correct version number
    $known = array('Version', $ub, 'other');
    $pattern = '#(?<browser>' . join('|', $known) .
    ')[/ ]+(?<version>[0-9.|a-zA-Z.]*)#';
    if (!preg_match_all($pattern, $u_agent, $matches)) {
        // we have no matching number just continue
    }

    // see how many we have
    $i = count($matches['browser']);
    if ($i != 1) {
        //we will have two since we are not using 'other' argument yet
        //see if version is before or after the name
        if (strripos($u_agent,"Version") < strripos($u_agent,$ub)){
            $version= $matches['version'][0];
        } else {
            $version= $matches['version'][1];
        }
    } else {
        $version= $matches['version'][0];
    }

    // check if we have a number
    if ($version==null || $version=="") {$version="?";}

    return array('userAgent' => $u_agent, 'browser' => $bname, 'version' => $version, 'os' => $platform, 'pattern' => $pattern);
}

function get_page_meta_array($title = "", $keywords = "", $description = "") {
    if (empty($title)) {
        $title = 'Video games, News, Reviews and Cheat Codes';
    }
    if (empty($keywords)) {
        $keywords = "PC games, latest pc games, Xbox 360 games, top pc game, new computer games, Xbox games on Xbox one, best online game ever, "
                        ."Video games reviews, best online gaming site, best online game ever, games for pc, pc game, games pc, cool games, Xbox one "
                        ."games list, Xbox games, Cheat Codes for Games, Video games walkthrough";
    }
    if (empty($description)) {
        $description = "Games Cottage is a reliable source for PS4, Xbox One, PS3, Xbox 360, Wii U, PS Vita, Wii, PC video games news, cheat codes, "
                            ."walk-through, reviews and trailers.";
    }
    
    $currentPath = Request::getPathInfo();
    $metas = DB::table('pages')->select(DB::raw('meta_title as title'), DB::raw('meta_keywords as keywords'), DB::raw('meta_description as description'), 
                    'content')->where('slug', $currentPath)->first();
    if ($currentPath == '/' && $metas) {
        $metas->name = '';
    }
    
    if (!$metas) {
        $metas = (object) ['title' => $title, 'keywords' => $keywords, 'description' => $description, 'content' => ''];
    }
//    pr($metas);exit;
    return $metas;
}

function get_company_name_and_logo () {
    $company_details = DB::table('company')->select('created_at', 'company_name', 'logo')->where('id', Session::get('client_user')->company_id)->first();
    if (empty($company_details->logo)) {
        $logo_url = asset('images/placeholders/company_small.png');
    } else {
        $logo_url = url("img/compay_logos/".getFolderStructureByDate($company_details->created_at)."/orignal/".$company_details->logo);
    }
    return ['name' => $company_details->company_name, 'logo_url' => $logo_url];
}