<?php
namespace App\Http\Requests\User;

use Illuminate\Foundation\Http\FormRequest;

class ProfileRequest extends FormRequest {

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules() {
        return [
            'first_name' => 'required|max:100',
            'last_name' => 'required|max:100',
            'password' => 'confirmed|min:6',
            'company_name' => 'required|max:150',
            'phone_number' => 'required|max:15',
            'address' => 'required|max:200',
            'city' => 'required|max:100',
            'state' => 'required|max:100',
            'zipcode' => 'required|max:10',
        ];
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize() {
        return true;
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages () {
        $messages = [
            'permit_number.required_if' => 'The permit number field is required.',
        ];
        
        return $messages;
    }

}