<?php
namespace App\Http\Requests\User;

use Illuminate\Foundation\Http\FormRequest;

class InstallationRequest extends FormRequest {

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules() {
        return [
            'job_date' => "required|date|date_format:Y/m/d",
            'expiry_date' => "required|date|date_format:Y/m/d|after:".date('Y/m/d')."",
//            'is_permit' => "required",
            'permit_number' => "required_if:is_permit,1",
        ];
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize() {
        return true;
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages () {
        $messages = [
            'permit_number.required_if' => 'The permit number field is required.',
        ];
        
        return $messages;
    }

}