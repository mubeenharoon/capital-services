<?php
namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class addbrand extends FormRequest {

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules() {
        return [
            'oficial_email' => "required",
            'first_nam' => "required",
            'last_name' => "required",
            'position_rol' => "required",
            'person_email' => "required",
            'per_ofice_numbr' => "required",
            'per_mbl' => "required",
            'best_time_reach' => "required",
            'Preferred_cont_mthd' => "required",
            'company_name' => "required",
            'brand_name' => "required",
//            'opration_city' => "required",
            
            
//            'email' => "required",
//            'message' => "required",
//            'company_name' => "required",
        ];
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize() {
        return [
            'brand_logo_file.mimes' => 'Not a valid file type. Valid types include jpeg, jpg, bmp and png.',
            'company_logo_file.mimes' => 'Not a valid file type. Valid types include jpeg, jpg, bmp and png.'
        ];
    }

}