<?php
namespace App\Http\Requests\Admin\AdminUsers;

use Illuminate\Foundation\Http\FormRequest;

class AdminUsersRequest extends FormRequest {

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules() {
        $rulesArray = [
            'first_name' => 'required|max:50',
            'last_name' => 'required|max:50',
            'email' => 'email|required_if:id,""|max:150',
            'password' => 'confirmed|min:6',
            'profile_pic' => "mimes:jpeg,jpg,bmp,png",
        ];
        return $rulesArray;
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize() {
        return [
            'company_logo_file.mimes' => 'Please choose a valid file type. Valid types include jpeg, jpg, bmp and png.'
        ];
    }

}