<?php
namespace App\Http\Requests\Admin\Users;

use Illuminate\Foundation\Http\FormRequest;
use Input;

class UsersRequest extends FormRequest {

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules() {
        
        $rulesArray = [
             'first_name' => 'required|max:50',
            'last_name' => 'required|max:50',
//            'email' => 'email|required_if:id,""|max:150',
            'password' => 'confirmed|min:6',
            'profile_pic' => "mimes:jpeg,jpg,bmp,png",
        ];
        return $rulesArray;
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize() {
        return true;
    }
    public function messages () {
        $messages = [
            'users_companies.required' => 'User company is required'
        ];
        
        return $messages;
    }

}