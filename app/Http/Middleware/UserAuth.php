<?php namespace App\Http\Middleware;

use Closure;
use Session;
use Route;
use Input;
use Illuminate\Contracts\Auth\Guard;

class UserAuth {

	/**
	 * The Guard implementation.
	 *
	 * @var Guard
	 */
	protected $user, $admin_user;

	/**
	 * Create a new filter instance.
	 *
	 * @param  Guard  $auth
	 * @return void
	 */
	public function __construct() {
            $this->user = count(Session::get('client_user'));
//            pr(Session::get('admin_user'));exit;
            $this->admin_user = count(Session::get('admin_user'));
            
	}

	/**
	 * Handle an incoming request.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  \Closure  $next
	 * @return mixed
	 */
	public function handle($request, Closure $next) {
            if (Route::getCurrentRoute()->getPath() == CLIENT_PREFIX."/auth/login/{one?}/{two?}/{three?}/{four?}/{five?}") {
                if ($this->user > 0) {
                    return redirect()->guest(CLIENT_PREFIX);
		}
            } else {
                if ($this->admin_user > 0 && Input::has('company_id')) {
                    $admin_user = Session::get('admin_user');
                    $admin_user->company_id = Input::get('company_id');
                    Session::put('client_user', $admin_user);
                    $this->user = count(Session::get('client_user'));
                }
                
		if ($this->user == 0) {
//                    pr(Session::get('client_user'));exit;
                    if ($request->ajax()) {
                            return response('Unauthorized.', 401);
                    } else {
                            return redirect()->guest(CLIENT_PREFIX.'/auth/login');
                    }
		}
                
            }

		return $next($request);
	}

}
