<?php namespace App\Http\Middleware;

use Closure;
use App\Models\ModelUserVisits;
use Session;

class UserVisits {

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next) {
        $page = $request->getPathInfo();
        if (substr($page, 0, 7) !== "/fonts/") {
            $ip = get_user_ip();
            $referer = filter_input(INPUT_SERVER, 'HTTP_REFERER');
            $userAccessDetails = get_user_browser_and_windows();
            
            $existingVisit = ModelUserVisits::where('page', $page)->where('ip', $ip)
                                ->whereRaw('DATE(created_at) = DATE("'.date('Y-m-d').'")')->orderBy('id', 'DESC')->first();
//            pr($existingVisit);exit;
            if (is_null($existingVisit)) {
                $country = '';
                $state = '';
                $city = '';

                $curl = curl_init();
                curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
                curl_setopt($curl, CURLOPT_URL, "http://ip-api.com/php/$ip");
                $userIPDetails = unserialize(curl_exec($curl));
                curl_close($curl);

                if($userIPDetails && $userIPDetails['status'] == 'success') {
                    $country = $userIPDetails ['country'];
                    $state = $userIPDetails ['regionName'];
                    $city = $userIPDetails ['city'];
                }

                ModelUserVisits::create (
                    ['ip' => $ip, 'count' => 1, 'page' => $page, 'city' => $city, 'state' => $state, 'country' => $country, 'referer' => $referer, 
                        'browser' => $userAccessDetails['browser'].'/'.$userAccessDetails['version'], 'os' => $userAccessDetails['os']]
                );
            } else {
                $existingVisit->count += 1;
                $existingVisit->save();
            }
        }
        return $next($request);
    }

}
