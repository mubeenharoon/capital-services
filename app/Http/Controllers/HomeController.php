<?php

namespace App\Http\Controllers;

use DB;
use App\Models\GalleryModel;
use App\Models\GalleryImagesModel;

class HomeController extends Controller {

    protected $viewData = array();

    /**
     * Show the application dashboard to the user.
     *
     * @return Response
     */
    public function Index () {
        $data['GalleryCategories'] = GalleryModel::orderBy('id', 'DESC')->get();
        
        $GalleryImages = GalleryImagesModel::orderBy('id', 'DESC')->get();
        $data['GalleryImages'] = array_reduce($GalleryImages->toArray(), function($result, $data) {
            $result[$data['gallery_id']][] = $data['name'];
            return $result;
        }, []);
        
        return view('index', $data);
    }

}
