<?php namespace App\Http\Controllers;

use DB;
use Auth;
use Session;
use Input;

use \App\Traits\PaypalTrait;

class PaypalController extends Controller {
    
    use PaypalTrait;
    
    function InstallationPaymentResponse () {
        // Get the payment ID before session clear
        $payer_id = Input::get('PayerID');
        $payment_details = Session::get('paypal_payment_details');

        if (empty(Input::get('PayerID')) || empty(Input::get('token'))) {
            return redirect(USER_PREFIX.'/posts')->withErrors('Something went wrong with payment.');
        }
        
        $status = $this->InstallationPayemntResponse($payer_id, $payment_details);
        
        if ($status == 'success') {
            return redirect(USER_PREFIX.'/posts')->with('message', 'You have successfully paid for installation.');
        }else {
            return redirect(USER_PREFIX.'/posts')->withErrors('Something went wrong with payment.');
        }
        // clear the session payment ID
//        Session::forget('paypal_payment_id');
//        Session::forget('paypalBoughtPackages');

    }

}