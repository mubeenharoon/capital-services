<?php

namespace App\Http\Controllers;

use Mail;
use DB;
use App\Http\Requests\Contact;
use App\Http\Requests\addbrand;
use App\Models\Company\CompanyModel;
use App\Models\Products\ProductsModel;
use App\Models\Products\ProductImagesModel;
use App\Models\addBrandModel;
use App\Models\Products\TypesModel;
use App\Models\Products\SizesModel;
use App\Models\LocationManagement\StatesModel;
use App\Models\LocationManagement\CitiesModel;
use App\Models\LocationManagement\LocationsModel;
use App\Models\LocationManagement\SubLocationsModel;
use Illuminate\Support\Facades\Redirect;
use App\Http\Requests\SuggestionsRequest;
use Session;
use Image;
use File;

class PagesController extends Controller {

    protected $viewData = array();

    public function __construct() {
        $this->viewData = ['selected_name' => '', 'selected_item_chk' => '', 'selected_advance_filter' => '', 'selected_country' => '', 'selected_state' => '',
            'selected_city' => '', 'selected_location' => '', 'selected_sub_location' => '', 'selected_product_type' => '',
            'selected_product_size' => '', 'selected_search_for' => '', 'cities' => [], 'locations' => [], 'sub_locations' => []];
        $this->viewData['compnay_product_link_perms'] = [];

        $this->viewData['states'] = StatesModel::where('country_id', 1)->get();

        $this->viewData['product_types'] = TypesModel::all();
        $this->viewData['product_sizes'] = SizesModel::all();
    }

    public function Contact() {
//        --------------------sEARCH-------------------------
        $data['special_offers'] = specialOffer();
//               pr($data['special_offers']); exit;
        return view("pages.contact", $data);
    }

    public function ContactForm(Contact $request) {
        $folderStructureByDate = date('Y') . "/" . date('n');
        $email_data = $request->except('_token', 'captcha');
        //mail to admin
        $email = 'abdullahforloop@gmail.com';
        $flag = Mail::send('emails.contact', $email_data, function($message) use ($email) {
                    $message->to($email)
                            ->subject('Follow-Up with a Contact Us Request');
                    $message->from('website@wateronline.ae', 'Water Online');
                });
                
        //mail to client
        $email = $request->email;
        $flag = Mail::send('emails.client_email_contact', $email_data, function($message) use ($email) {
                    $message->to($email)
                            ->subject('WO! Thanks for Contacting Us');
                    $message->from('website@wateronline.ae', 'Water Online');
                });

        return redirect("contact")->with('message', 'Your request has been submitted successfully.');
    }

    public function About() {
        $data = $this->viewData;
//        --------------------sEARCH-------------------------
        $data['special_offers'] = specialOffer();

        return view("pages.about", $data);
    }

    public function term() {
        $data = $this->viewData;
//        --------------------sEARCH-------------------------
        $data['special_offers'] = specialOffer();

        return view("pages.term_services", $data);
    }

    public function world_water() {
        $data = $this->viewData;
//        --------------------sEARCH-------------------------
        $data['special_offers'] = specialOffer();

        return view("pages.world_water", $data);
    }

    public function Privacy() {
        $data = $this->viewData;
//        --------------------sEARCH-------------------------
        $data['special_offers'] = specialOffer();
        return view("pages.privacy", $data);
    }

    public function Addbrand() {
        $data = $this->viewData;
//        --------------------sEARCH-------------------------
        $data['special_offers'] = specialOffer();
        $data['cities'] = DB::table('city')->get();
        return view("add-brand", $data);
    }

    public function AddbrandForm(addbrand $request) {

//        print_r($request->all());exit;

        $slug = makeSlug($request->get('company_name'));

        $folderStructureByDate = date('Y') . "/" . date('n');
//        pr($folderStructureByDate);exit;

        if ($request->file('company_logo_file') != null) {
            $fileName_company = saveOrignalImage($request->file('company_logo_file'));
            makeMultipleSizesOfLogo($fileName_company, 'img/requested_brand/companylogo', ['orignal']);
        } else {
            $fileName_company = "";
        }
        if ($request->file('brand_logo_file') != null) {
            $fileName_brand = saveOrignalImage($request->file('brand_logo_file'));
            makeMultipleSizesOfLogo($fileName_brand, 'img/requested_brand/brandlogo', ['orignal']);
        } else {
            $fileName_brand = "";
        }
        if ($request->has('opration_city')) {
            $request->merge(array('opration_city' => implode(",", $request->get('opration_city'))));
        }
        $values = array('oficial_email' => $request->get('oficial_email'), 'descrption' => $request->get('descrption'), 'first_nam' => $request->get('first_nam'), 'last_name' => $request->get('last_name'), 'position_rol' => $request->get('position_rol'), 'person_email' => $request->get('person_email'), 'per_ofice_numbr' => $request->get('per_ofice_numbr'), 'per_mbl' => $request->get('per_mbl'), 'best_time_reach' => $request->get('best_time_reach'), 'Preferred_cont_mthd' => $request->get('Preferred_cont_mthd'), 'company_name' => $request->get('company_name'), 'brand_name' => $request->get('brand_name'), 'opration_city' => $request->get('opration_city'), 'cal_center_numbr' => $request->get('cal_center_numbr'), 'office_number' => $request->get('office_number'), 'company_logo_file' => $fileName_company, 'brand_logo_file' => $fileName_brand, 'company_web' => $request->get('company_web'));
//        print_r($values);exit;
        $userDetails = addBrandModel::updateOrCreate($values);
//        pr($userDetails); exit;
        
        //mail to admin
        $email_data = $request->except('_token');
        $operation_cities = array_column(CitiesModel::select('name')->whereIn('id', explode(',', $email_data['opration_city']))->get()->toArray(), 'name');
        $email_data['opration_city'] = implode(',', $operation_cities);
        $email_body = array('test' => 'test');
        $email = 'abdullahforloop@gmail.com';
        Mail::send('emails.brand_request', $email_data, function($message) use ($email) {
            $message->to($email)
                    ->subject('Follow-Up with New Add Brand Request');
            $message->from('website@wateronline.ae', 'Water Online');
        });
        
        //mail to client
        $email = $request->person_email;
        $email_data['msg1'] = 'A warm welcome from WO! We are happy to see your request to add your brand on our website. We are thankful for your trust on our services, therefore we will proceed with the follow up. We have received the following details from your side;';
        $email_data['msg2'] = 'As per your contact preferences, one of our team member will be soon in touch with you to proceed with the further formalities. We will be doing our best to see you onboard as soon as possible.';
        $flag = Mail::send('emails.client_email_brand_request', $email_data, function($message) use ($email) {
                    $message->to($email)
                            ->subject('Welcome to WO! Add Brand Request Acknowledgement');
                    $message->from('website@wateronline.ae', 'Water Online');
                });
//        $client_mail = $request->get('person_email');
//        Mail::send('emails.add_brand_client_mail', ['content' => 'This is the email Content'], function($message) use ($client_mail) {
//            $message->to($client_mail)
//                    ->subject('Requested Brand');
//            $message->from('website@wateronline.ae', 'Waterapp');
//        });
        return redirect("add_brand")->with('message', 'We have received your add brand request, we will contact you soon.');
    }

    public function CompaniesandProducts() {
        $data = $this->viewData;


        $data['oldvalues'] = array('name' => "", 'state' => "", 'city' => "", 'location' => "", 'product_type' => "", 'product_size' => "");
        $data['products'] = ProductsModel::leftJoin('company as c', 'c.id', '=', 'product.company')->leftJoin('product_size as s', 's.id', '=', 'product.size')
                        ->leftJoin('product_type as t', 't.id', '=', 'product.type')->leftJoin('product_image as i', 'i.product_id', '=', 'product.id')->select('product.id', 'product.title', 'product.created_at', 's.name as size', 'c.company_name as company', 't.name as type', 'i.name as image')->groupBy('product.id')->orderBy('c.company_name', 'ASC')->paginate(26);
        $data['users'] = CompanyModel::leftJoin('country as c', 'c.id', '=', 'company.country')->leftJoin('state as s', 's.id', '=', 'company.state')->leftJoin('location as l', 'l.id', '=', 'company.location')
                        ->leftJoin('city as ci', 'ci.id', '=', 'company.city')->select("company.*", 'c.name as country', 's.name as state', 'ci.name as city')
                        ->orderBy('company_name', 'ASC')->paginate(26);
        return view("allproducts", $data);
    }

    public function Advertise_Your_Brand() {
        $data = $this->viewData;
        $data['special_offers'] = specialOffer();
        return view('pages.advertise_your_brand', $data);
    }

    public function Claim_Your_Listing() {
        $data = $this->viewData;
        $data['special_offers'] = specialOffer();
        return view('pages.claim_your_listing', $data);
    }

    public function Advertise_Brand_Form(Contact $request) {
        $folderStructureByDate = date('Y') . "/" . date('n');
        $email_data = $request->except('_token', 'captcha'); 
        //mail to admin
        $email = 'abdullahforloop@gmail.com';
        $flag = Mail::send('emails.advertise_brand', $request->except('_token', 'captcha'), function($message) use ($email) {
                    $message->to($email)
                            ->subject('Follow-Up with the Advertisement Request');
                    $message->from('website@wateronline.ae', 'Water Online');
                });
                
        //mail to client
        $email = $request->email;
        $email_data['msg'] = 'Your Request for Advertisement of Brand has been received. We have received the following details from your side;';
        $flag = Mail::send('emails.client_email_advertise_brand', $email_data, function($message) use ($email) {
                    $message->to($email)
                            ->subject('WO! Welcomes to Your Advertisement Request');
                    $message->from('website@wateronline.ae', 'Water Online');
                });


        return redirect("advertise_your_brand")->with('message', 'Your request has been submitted successfully.');
    }

    public function Claim_Listing_Form(Contact $request) {
        $folderStructureByDate = date('Y') . "/" . date('n');
        $email_data = $request->except('_token', 'captcha');    
        //mail to admin
        $email = 'abdullahforloop@gmail.com';
        $flag = Mail::send('emails.claim_listing', $email_data, function($message) use ($email) {
                    $message->to($email)
                            ->subject('Follow-Up with new Claim Listing Request');
                    $message->from('website@wateronline.ae', 'Water Online');
                });
                
        //mail to client
        $email = $request->email;
        $email_data['msg'] = 'Your Claim Request has been received. We have received the following details from your side;';
        $flag = Mail::send('emails.client_email_claim_listing', $email_data, function($message) use ($email) {
                    $message->to($email)
                            ->subject('WO! Thanks for Claim Listing');
                    $message->from('website@wateronline.ae', 'Water Online');
                });

        return redirect("claim_your_listing")->with('message', 'Your request has been submitted successfully.');
    }

    public function suggestions(SuggestionsRequest $request) {


//        pr($request->except('_token', 'captcha'));exit;
        $email_data = $request->except('_token', 'captcha');
        $email = "abdullahforloop@gmail.com";

        $flag = Mail::send('emails.SuggestionEmail', $email_data, function($message) use ($email) {
                    $message->to($email)
                            ->subject('Follow-Up with a Suggestion Request');
                    $message->from('website@wateronline.ae', 'Water Online');
                });
                
                //email to client
        $email = $request->email;
        $email_data['msg'] = 'Your suggestions have been submitted successfully. We have received the following details from your side;';
        $flag = Mail::send('emails.client_email_suggestion', $email_data, function($message) use ($email) {
                    $message->to($email)
                            ->subject('WO! Thanks for Your Suggestion');
                    $message->from('website@wateronline.ae', 'Water Online');
                });        
                
        return redirect()->back()->with('message', 'Your request has been submitted successfully.');
    }

}
