<?php
namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use Session;
use DB;
use Input;
use Auth;
use App\Models\Users\UsersModel as MainModel;
use App\Models\SignBoardsModel;
use App\Models\OrdersItemsModel;
use App\Models\Databank\PropertyTypesModel;
use App\Http\Requests\User\ProfileRequest;

use \App\Traits\PaypalTrait;

class ProfileController extends Controller {

    use PaypalTrait;

    public function View () {
        $id = Auth::id();
        $data['post'] = MainModel::find($id);
        $data['pos_items'] = OrdersItemsModel::select('s.*')->join('signboards as s', 's.id', '=', 'order_items.signboard_id')
                                ->where('order_items.order_id', $id)->get();
//        pr($data['pos_items']->toArray());exit;
        return view("user.profile", $data);
    }
    
    public function Save (ProfileRequest $request) {
        $id = Auth::id();
        $post_data = (empty($request->get('password'))) ? 
                        $request->except('email', 'password', 'password_confirmation') : $request->except('email', 'password_confirmation');
        MainModel::where('id', $id)->update($post_data);
        
        return redirect(USER_PREFIX."/profile")->with('message', "Profile Updated Successfully!"); 
    }
    
    public function Delete () {
    	$id = Input::get('id');
        try {
            OrdersItemsModel::where('id', $id)->delete();
            MainModel::destroy($id);

            return redirect(USER_PREFIX."/posts")->with('message', 'Deleted Successfully');
        } catch (\Exception $e) {
            return redirect(USER_PREFIX."/posts")->withErrors('Unable to Delete!');
        }
        
    }
        
}