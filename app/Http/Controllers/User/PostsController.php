<?php
namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use Session;
use DB;
use App\Models\OrdersModel;
use App\Models\SignBoardsModel;
use App\Models\Databank\PropertyTypesModel;
use App\Http\Requests\User\InstallationRequest;

class PostsController extends Controller {

    public function Index () {
//        $data['PropertyTypes'] = PropertyTypesModel::all();
//        $data['signbards'] = SignBoardsModel::orderBy('id', 'DESC')->get();
        return view("user.posts");
    }
    
    public function Save (InstallationRequest $request) {
        $request->merge(['job_date' => date('Y-m-d', strtotime($request->get('job_date'))), 
                            'expiry_date' => date('Y-m-d', strtotime($request->get('expiry_date')))]);
        OrdersModel::create($request->all());
        return redirect(USER_PREFIX."/posts")->with('message', "Request Sent Successfully!"); 
    }
        
}