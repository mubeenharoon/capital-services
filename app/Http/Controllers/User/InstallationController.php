<?php
namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use Session;
use DB;
use Input;
use App\Models\OrdersModel as MainModel;
use App\Models\SignBoardsModel;
use App\Models\OrdersItemsModel;
use App\Models\Databank\PropertyTypesModel;
use App\Http\Requests\User\InstallationRequest;

use \App\Traits\PaypalTrait;

class InstallationController extends Controller {

    use PaypalTrait;
    
    public function Index () {
        $data['posts'] = MainModel::leftJoin('cnf_order_status as s', 's.id', '=', 'orders.status')->select('orders.*', 's.name as status_name')
                            ->orderBy('id', 'DESC')->paginate(50);
        return view("user.installation.list", $data);
    }

    public function View ($id) {
        $data['post'] = MainModel::leftJoin('dbk_property_types as dpt', 'dpt.id', '=', 'orders.property_type')
                            ->select('orders.*', 'dpt.name as proper_type_name')->where('orders.id', $id)->first();
        $data['pos_items'] = OrdersItemsModel::select('s.*')->join('signboards as s', 's.id', '=', 'order_items.signboard_id')
                                ->where('order_items.order_id', $id)->get();
//        pr($data['pos_items']->toArray());exit;
        return view("user.installation.view", $data);
    }
    
    public function Add () {
        $id = Input::has('id') ? Input::get('id') : '';
        $data['PropertyTypes'] = PropertyTypesModel::all();
        $data['signbards'] = SignBoardsModel::orderBy('id', 'DESC')->get();
        
        $data['post'] = MainModel::findOrNew($id);
        
        if ($data['post']->id) {
            $data['post']->job_date = date('Y/m/d', strtotime($data['post']->job_date));
            $data['post']->expiry_date = date('Y/m/d', strtotime($data['post']->expiry_date));
        }

        $data['order_items'] = $data['post']->order_items->toArray();
//        $data['order_items_ids'] = count($data['order_items']) ? array;
        $data['order_items_ids'] = array_column($data['order_items'], 'signboard_id');
        
        return view("user.installtion", $data);
    }
    
    public function Save (InstallationRequest $request) {
        $id = $request->get('id');
        $request->merge(['job_date' => date('Y-m-d', strtotime($request->get('job_date'))), 
                            'expiry_date' => date('Y-m-d', strtotime($request->get('expiry_date')))]);
        $OrderData = $request->except('order_items');
        if (empty($id)) {
            $OrderData['status'] = 3;
        }
        $order = MainModel::updateOrCreate(['id' => $id], $OrderData);
        
        $deleted_order_items = [];
        $selected_items_array = [];
        $total_order_amount = 0;
        foreach($request->get('order_items') as $order_item) {
            if (isset($order_item['id'])) {
                $total_order_amount += $order_item['price'];
                $deleted_order_items[] = $order_item['id'];
                $OrderItemsData = ['order_id' => $order->id, 'signboard_id' => $order_item['id'], 'price' => $order_item['price']];
                OrdersItemsModel::updateOrCreate(['order_id' => $order->id, 'signboard_id' => $order_item['id']], $OrderItemsData);
                $OrderItemsData['name'] = $order_item['name'];
                $selected_items_array[] = $OrderItemsData;
            }
        }
        
        $order->total_amount = $total_order_amount;
        $order->save();
        
        if (count($deleted_order_items)) {
            OrdersItemsModel::where('order_id', $order->id)->whereNotIn('signboard_id', $deleted_order_items)->delete();
        }
        
        $paypal_data = $this->RedirectToPaypalForInsatalltionPayment($selected_items_array, $order->id);
        if ($paypal_data == 'error') {
            return redirect(USER_PREFIX.'/posts')->withErrors('Something went wrong while going paypal for payment.');
        } else {
            return redirect($paypal_data);
        }
//        echo 'hi';exit;
        $updated_msg = empty($id) ? 'Sent' : 'Updated';
        
        return redirect(USER_PREFIX."/posts")->with('message', "Request $updated_msg Successfully!"); 
    }
    
    public function Delete () {
    	$id = Input::get('id');
        try {
            OrdersItemsModel::where('id', $id)->delete();
            MainModel::destroy($id);

            return redirect(USER_PREFIX."/posts")->with('message', 'Deleted Successfully');
        } catch (\Exception $e) {
            return redirect(USER_PREFIX."/posts")->withErrors('Unable to Delete!');
        }
        
    }
        
}