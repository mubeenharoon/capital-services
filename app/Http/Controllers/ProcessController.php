<?php
namespace App\Http\Controllers;

use App\Models\Products\ProductsModel;
use App\Models\LocationManagement\CitiesModel;
use App\Models\Company\CompanyModel;
use App\Models\Brands\BrandsModel;
//use Session;
use Input;
use DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
//use Carbon;

class ProcessController extends Controller {

    public function SubmitAutoCompleteForm () {
        $request = Input::all();
         $retunURL = "search";
         $query_param="";

        $pakageType = "";
        $waterTpe = "";
        $param = array();
        $option_id = $request['option_id'];
        $option_type = $request['option_type'];
        $option_value = $request['option_value'];
        $search_in = $request['search_in'];
        
        if ($option_value == $search_in && !empty($search_in)) {
           
            switch ($option_type) {
                case "product":
                    $data = ProductsModel::select('id', 'slug')->where('id', $option_id)->first();
                    $retunURL = "product/$data->slug/$data->id";
                    break;
                case "brand":
                    $data = BrandsModel::select('id', 'slug')->where('id', $option_id)->first();
                    $retunURL = "brand/$data->slug/$data->id";
                    break;
                case "company":
                    $data = CompanyModel::select('id', 'slug')->where('id', $option_id)->first();
                    $retunURL = "company/$data->slug/$data->id";
                    break;
                case "city":
                    $data = CitiesModel::select('id', 'slug')->where('id', $option_id)->first();
                    $param['city'] = $data->id;

                    break;
            }
            
            
            if(!empty($request['packaging_type'])){
                
                $pakageType = $request['packaging_type'];
                $param['pakageTpe'] = $pakageType;
                
            }
           
             $query_param = "?".http_build_query($param);
            $retunURL = $retunURL.$query_param;
            
            
            
            return redirect($retunURL);
        } else {
            
            if(!empty($request['packaging_type'])){
                $pakageType = $request['packaging_type'];
                $param['pakageTpe'] = $pakageType;

                $query_param = "?".http_build_query($param);
             
            }
            $retunURL = $retunURL.$query_param;
            return redirect($retunURL);
        }
    }
        
}