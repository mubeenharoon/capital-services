<?php

namespace App\Http\Controllers;

use App\Models\Products\SizesModel;
use App\Models\Products\ProductsModel;
use App\Models\Company\CompanyModel;
use App\Models\LocationManagement\SubLocationsModel;
use App\Models\LocationManagement\LocationsModel;
use App\Models\LocationManagement\CitiesModel;
use App\Models\LocationManagement\StatesModel;
use App\Models\LocationManagement\CountriesModel;
use App\Models\Brands\BrandsModel;
use App\Models\Company\CompanyGroceryStoresModel;
use App\Models\GroceryStoresModel;
use App\Models\NewsletterModel;
use Session;
use Input;
use Mail;
use DB;

//use Carbon;


class AjaxRequests extends Controller {

    public function GetStates() {
        $id = Input::get("id");
        if (!is_array($id)) {
            $id = [$id];
        }
        return StatesModel::whereIn('state.country_id', $id)->leftJoin('country as c', 'state.country_id', '=', 'c.id')
                        ->select('state.id', 'state.name', 'c.name as parentName')->orderBy('c.name', "ASC")->orderBy('state.name', "ASC")->get();
    }

    public function GetCities() {
        $id = Input::get("id");
        if (!is_array($id)) {
            $id = [$id];
        }
        return CitiesModel::whereIn('city.state_id', $id)->leftJoin('state as p', 'city.state_id', '=', 'p.id')
                        ->select('city.id', 'city.name', 'p.name as parentName')->orderBy('p.name', "ASC")->orderBy('city.name', "ASC")->get();
    }

    public function GetLocations() {
        $id = Input::get("id");
        if (!is_array($id)) {
            $id = [$id];
        }
        return LocationsModel::whereIn('location.city_id', $id)->leftJoin('city as p', 'location.city_id', '=', 'p.id')
                        ->select('location.id', 'location.name', 'p.name as parentName')->orderBy('p.name', "ASC")->orderBy('location.name', "ASC")->get();
    }

    public function GetSubLocations() {
        $id = Input::get("id");
        if (!is_array($id)) {
            $id = [$id];
        }
        return SubLocationsModel::whereIn('sub_location.location_id', $id)->leftJoin('location as p', 'sub_location.location_id', '=', 'p.id')
                        ->select('sub_location.id', 'sub_location.name', 'p.name as parentName')->orderBy('p.name', "ASC")->orderBy('sub_location.name', "ASC")
                        ->get();
    }

    public function GetProducts() {
        $id = Input::get("id");
        if (!is_array($id)) {
            $id = [$id];
        }
        return ProductsModel::whereIn('product.company', $id)->select('product.id', 'product.title as name')->orderBy('product.title', "ASC")->get();
    }

    public function GetgerocryStrores() {
        $id = Input::get("id");

        $company_available = CompanyGroceryStoresModel :: where('company_available_at.company_id', $id)->select('company_available_at.available_at_id')->get()->toArray();
        return GroceryStoresModel::whereNotIn('id', $company_available)->select('id', 'name')->orderBy('name', 'ASC')->get();
    }

    public function SaveLocationInSession() {
        $lat = Input::get('lat');
        $long = Input::get('long');
        $request = "http://maps.googleapis.com/maps/api/geocode/json?latlng=$lat,$long&sensor=false";
        $file_contents = file_get_contents($request);
        $json_decode = json_decode($file_contents);
        $user_location = [];
        if (isset($json_decode->results[0])) {
            foreach ($json_decode->results[0]->address_components as $addressComponet) {
                if (in_array('country', $addressComponet->types)) {
                    $user_location['country'] = $addressComponet->long_name;
                } else if (in_array('administrative_area_level_1', $addressComponet->types)) {
                    $user_location['state'] = $addressComponet->long_name;
                } else if (in_array('locality', $addressComponet->types)) {
                    $user_location['city'] = $addressComponet->long_name;
                } else if (in_array('sublocality', $addressComponet->types)) {
                    $user_location['location'] = $addressComponet->long_name;
                } else if (in_array('neighborhood', $addressComponet->types)) {
                    $user_location['sub_location'] = $addressComponet->long_name;
                } else if (in_array('route', $addressComponet->types)) {
                    $user_location['address'] = $addressComponet->long_name;
                }
            }

            $locations_ids = [];
            foreach ($user_location as $key => $location) {
                switch ($key) {
                    case 'country':
                        $location_id_obj = CountriesModel::select('id')->where('name', $location)->first();
                        if (isset($location_id_obj->id))
                            $locations_ids['country'] = $location_id_obj->id;
                        break;
                    case 'state':
                        $query = StatesModel::select('id')->where('name', $location);
                        if (isset($locations_ids['country'])) {
                            $query->where('country_id', $locations_ids['country']);
                        }
                        $location_id_obj = $query->first();
                        if (isset($location_id_obj->id))
                            $locations_ids['state'] = $location_id_obj->id;
                        break;
                    case 'city':
                        $query = CitiesModel::select('id')->where('name', $location);
                        if (isset($locations_ids['state'])) {
                            $query->where('state_id', $locations_ids['state']);
                        }
                        $location_id_obj = $query->first();
                        if (isset($location_id_obj->id))
                            $locations_ids['city'] = $location_id_obj->id;
                        break;
                    case 'location':
                        $query = LocationsModel::select('id')->where('name', $location);
                        if (isset($locations_ids['city'])) {
                            $query->where('city_id', $locations_ids['city']);
                        }
                        $location_id_obj = $query->first();
                        if (isset($location_id_obj->id))
                            $locations_ids['location'] = $location_id_obj->id;
                        break;
                    case 'sub_location':
                        $query = SubLocationsModel::select('id')->where('name', $location);
                        if (isset($locations_ids['location'])) {
                            $query->where('location_id', $locations_ids['location']);
                        }
                        $location_id_obj = $query->first();
                        if (isset($location_id_obj->id))
                            $locations_ids['sub_location'] = $location_id_obj->id;
                        break;
                }
            }
            $user_location_array = [$locations_ids, $user_location];
            Session::put('user_location', $user_location_array);
            $response = $user_location_array;
        } else {
            $response = 'false';
        }

        return $response;
    }

    public function AutoComplete() {
        $products = DB::table('product as p')->leftjoin('brands as br', 'br.id', '=', 'p.company')
                        ->leftjoin('company as cmpny', 'cmpny.id', '=', 'br.company')
                        ->select("p.id", "p.title as value", DB::raw('"product" as data'))
                        ->where('p.title', "LIKE", "" . Input::get('query') . "%")
                        ->where('p.status', 1)
                        ->where('br.status', 1)
                        ->where('cmpny.status', 1)
                        ->orderBy('p.title', 'ASC')->limit(3);
        $cities = DB::table('city')->select("id", "name as value", DB::raw('"city" as data'))
                        ->where('name', "LIKE", "" . Input::get('query') . "%")->orderBy('name', 'ASC')->limit(3);
        $query = DB::table('brands as br')->leftJoin('company as cmpny', 'cmpny.id', '=', 'br.company')
                        ->select("br.id", "br.name as value", DB::raw('"brand" as data'))
                        ->where('br.name', "LIKE", "" . Input::get('query') . "%")
                        ->where('br.status', 1)
                        ->where('cmpny.status', 1)
                        ->orderBy('br.name', 'ASC')->limit(3)->union($products);

//        $query = DB::table('company')->select("id", "company_name as value", DB::raw('"company" as data'))
//                        ->where('company_name', "LIKE", "".Input::get('query')."%")->orderBy('company_name', 'ASC')->limit(3)->union($products);
//        
        if (!Session::has('user_location')) {
            $query->union($cities);
        }

        $suggestions = $query->get();

        $response['suggestions'] = array_map(function ($array) {
            return ['id' => $array->id, 'value' => $array->value, 'data' => ['type' => $array->data]];
        }, $suggestions);
        return $response;
    }

    public function AutoCompleteLocations() {
        $city = Input::get('city');
        $locations = explode(',', Input::get('location'));
        $sub_locations = explode(',', Input::get('sub_location'));

        $locations_query = DB::table('location')->select("id", "name as value", DB::raw('"location" as data'))
                        ->where('name', "LIKE", "" . Input::get('query') . "%")->whereNotIn('id', $locations)->orderBy('name', 'ASC');

        $suggestions_query = DB::table('sub_location as sl')->select("sl.id", "sl.name as value", DB::raw('"sub location" as data'))
                        ->leftJoin('location as l', 'l.id', '=', 'sl.location_id')->where('sl.name', "LIKE", "" . Input::get('query') . "%")
                        ->whereNotIn('sl.id', $sub_locations)->orderBy('sl.name', 'ASC');

        if (!empty($city)) {
            $locations_query->where('city_id', $city);
            $suggestions_query->where('l.city_id', $city);
        }

        $suggestions = $suggestions_query->union($locations_query)->get();

        $response['suggestions'] = array_map(function ($array) {
            return ['id' => $array->id, 'value' => $array->value, 'data' => ['type' => $array->data]];
        }, $suggestions);
        return $response;
    }

    public function GetPackageSizes() {
        if (!empty(Input::get("ids")) && Input::get("ids") != "all") {
            $ids = Input::get("ids");

            if (!is_array($ids)) {
                $ids = [$ids];
            }

            $data['product_sizes'] = SizesModel::leftJoin('product_package as pk', DB::raw('find_in_set(product_size.id ,pk.sizes)'), DB::raw(''), DB::raw(''))
                            ->select('product_size.id', 'product_size.name')->whereRaw('FIND_IN_SET(pk.id, "' . implode(',', $ids) . '")')->distinct()->get();

//            pr($data['product_sizes']); exit;
//            ->where('pk.id',$data['packaging_type'])->get();
            return $data['product_sizes'];
        } else {
            $data['product_sizes'] = DB::table('product_size')->get();
            return $data['product_sizes'];
        }
    }

    public function GetSearchResultProducts() {
        $data['reqiest']['city'] = Input::get('city');
        $data['reqiest']['location_ids'] = Input::get('location_ids');
        $data['reqiest']['sub_location_ids'] = Input::get('sub_location_ids');
        $data['reqiest']['sizes'] = Input::get('sizes');
        $data['reqiest']['companies'] = Input::get('companies');
        $data['reqiest']['water_types'] = Input::get('water_types');
        $data['reqiest']['min_price'] = Input::get('min_price');
        $data['reqiest']['max_price'] = Input::get('max_price');
        $data['reqiest']['post_per_page'] = Input::get('post_per_page');
        $data['reqiest']['order_by'] = Input::get('order_by');
        $data['reqiest']['filter_for'] = Input::get('filter_for');
        $data['reqiest']['packaging_type'] = Input::get('packaging_type');
        $data['reqiest']['special_offer'] = Input::get('special_offer');



        if ($data['reqiest']['filter_for'] == 'Products') {
            $postsModel = ProductsModel::leftJoin('brands as co', 'co.id', '=', 'product.company')
                            ->leftJoin('company as campny', 'campny.id', '=', 'co.company')
                            ->leftJoin('service_location as sl', 'sl.company_id', '=', 'campny.id')
                            ->leftJoin('country as c', 'c.id', '=', 'sl.country')
                            ->leftJoin('state as s', 's.id', '=', 'sl.state')
                            ->leftJoin('city as ci', 'ci.id', '=', 'sl.city')
                            ->leftJoin('sub_location as sbl', 'sbl.id', '=', 'sl.sub_location')
                            ->leftJoin('location as l', 'l.id', '=', 'sl.location')
                            ->leftJoin('product_size as si', 'si.id', '=', 'product.size')
                            ->leftJoin('product_type as t', 't.id', '=', 'product.type')
                            ->leftJoin('product_image as i', 'i.product_id', '=', 'product.id')
                            ->leftJoin('product_package as pt', 'pt.id', '=', 'product.package_type')
                            ->leftJoin('phone_numbers as pn', DB::raw("(pn.phone_for = 'brand' && pn.reference_id = co.id)"), DB::raw(''), DB::raw(''))
                            ->leftJoin('phone_numbers as pn_company', DB::raw("(pn_company.phone_for = 'company' && pn_company.reference_id = campny.id)"), DB::raw(''), 
                                    DB::raw(''))
                            ->select('pn.number as phone_number', 'product.price', 'product.discount_type', 'product.discount', 'product.bundle_price', 
                                    'product.bundle_quantity', 'product.slug', 'c.name as country', 's.name as state', 'ci.name as city', 'product.id', 'product.title', 
                                    'product.created_at', 'si.name as size', 'co.name as company', 'co.id as brandId', 'co.slug as brandSlug', 't.name as type', 
                                    't.id as type_id', 'i.name as image', 'co.created_at as company_created', 'co.logo', 'pn_company.number as phone_number_company')
                            ->where('product.status', 1)->where('co.status', 1)->where('campny.status', 1);
            if ($data['reqiest']['min_price'] != null) {
                $max_price = $data['reqiest']['max_price'];
                $min_price = $data['reqiest']['min_price'];
                $postsModel->whereRaw("product.price >= $min_price AND product.price <= $max_price");
//                return $result;
//                $postsModel->whereRaw('if(product.bundle_price=0 , product.price < '.$data['reqiest']['max_price'].' and product.price >'.$data['reqiest']['min_price'].'),product.bundle_price<'.$data['reqiest']['max_price'].'and product.bundle_price > '.$data['reqiest']['min_price']);
//                
            }
            if (!empty($data['reqiest']['city'])) {
                $postsModel->where('sl.city', $data['reqiest']['city']);
            }
            if (!empty($data['reqiest']['location_ids'])) {
                $postsModel->whereIn('sl.location', $data['reqiest']['location_ids']);
            }
            if (!empty($data['reqiest']['sub_location_ids'])) {
                $postsModel->whereIn('sl.sub_location', $data['reqiest']['sub_location_ids']);
            }

            if (!empty($data['reqiest']['water_types'])) {
                $postsModel->whereIn('t.id', $data['reqiest']['water_types']);
            }

            if (!empty($data['reqiest']['sizes'])) {
                $postsModel->whereIn('si.id', $data['reqiest']['sizes']);
            }

            if (!empty($data['reqiest']['companies'])) {
                $postsModel->whereIn('co.id', $data['reqiest']['companies']);
            }

            if (!empty($data['reqiest']['packaging_type'])) {
                $postsModel->whereIn('pt.id', $data['reqiest']['packaging_type']);
            }
            if (!empty($data['reqiest']['special_offer'])) {
                $postsModel->where('product.discount', '>', 0);
            }


            $data['posts'] = $postsModel->groupBy('product.id')->orderBy('product.price', $data['reqiest']['order_by'])
                    ->paginate($data['reqiest']['post_per_page']);

            return \Response::JSON(['data' => $data['posts'], 'pagination' => (string) $data['posts']->links()]);
        } else {

            $postsModel = BrandsModel::leftJoin(DB::raw('(select service_location.company_id, GROUP_CONCAT(DISTINCT(city.name)) as cities from city left join service_location '
                                    .'on service_location.city = city.id  GROUP BY service_location.company_id) as slc'), 'slc.company_id', '=', 'brands.company') // Join With Service Areas Cities
                            ->leftJoin(DB::raw('(SELECT company, GROUP_CONCAT(DISTINCT(size)) AS sizes, status, GROUP_CONCAT(DISTINCT(type)) AS types FROM product where '
                                    .'status = 1 GROUP BY company) as po'), 'po.company', '=', 'brands.id')
                            ->leftJoin(DB::raw('(SELECT id, company,package_type, GROUP_CONCAT(DISTINCT package_type) as packages FROM `product` GROUP BY company) as pt'), 
                                            'pt.company', '=', 'brands.id')
                            ->leftjoin('company as compny', 'compny.id', '=', 'brands.company')
                            ->leftJoin('service_location as sl', 'sl.company_id', '=', 'compny.id')
                            ->leftJoin('country as c', 'c.id', '=', 'sl.country')
                            ->leftJoin('state as s', 's.id', '=', 'sl.state')->leftJoin('city as ci', 'ci.id', '=', 'sl.city')
                            ->leftJoin('location as l', 'l.id', '=', 'sl.location')
                            ->leftJoin('sub_location as sbl', 'sbl.id', '=', 'sl.sub_location')
                            ->leftJoin(DB::raw('(select delivery_days.id, service_location.company_id, delivery_days.reference_id, GROUP_CONCAT(DISTINCT(delivery_days.day)) as '
                                    .'availablity from delivery_days left join service_location on service_location.id = delivery_days.reference_id  GROUP BY '
                                    .'service_location.company_id) as avail'), 'avail.company_id', '=', 'brands.company') // Join With Company Available At
                            ->select("compny.id as companyID", 'c.name as country', "brands.created_at", "brands.logo", 'brands.id as brandsID', 's.name as state', 
                                    'ci.name as city', 'po.sizes', 'po.types', "compny.slug as companySlug", 'brands.slug as brandSlug', 'pt.packages', 'pt.id as productId', 
                                    'brands.name', 'l.name as locationName', 'sbl.name as sublocation_name', 's.name as stateName', 'slc.cities', 'avail.availablity')
                            ->where('brands.status', 1)->where('compny.status', 1);

            if (!empty($data['reqiest']['city'])) {
                $postsModel->where('sl.city', $data['reqiest']['city']);
            }
            if (!empty($data['reqiest']['location_ids'])) {
                $postsModel->whereIn('sl.location', $data['reqiest']['location_ids']);
            }
            if (!empty($data['reqiest']['sub_location_ids'])) {
                $postsModel->whereIn('sl.sub_location', $data['reqiest']['sub_location_ids']);
            }
            if (!empty($data['reqiest']['packaging_type'])) {
                $postsModel->whereIn('pt.package_type', $data['reqiest']['packaging_type']);
            }

            if (!empty($data['reqiest']['water_types'])) {
                $watertypes = $data['reqiest']['water_types'];
                $postsModel->where(function($query) use ($watertypes) {
                    foreach ($watertypes as $watertype) {
                        $query->orWhereRaw("FIND_IN_SET($watertype, po.types)");
                    }
                });
            }
            if (!empty($data['reqiest']['sizes'])) {
                $sizes = $data['reqiest']['sizes'];
                $postsModel->where(function($query) use ($sizes) {
                    foreach ($sizes as $size) {
                        $query->orWhereRaw("FIND_IN_SET($size, po.sizes)");
                    }
                });
            }

            if (!empty($data['selected_name'])) {
                $postsModel->where('company.company_name', 'like', "%{$data['selected_name']}%");
                $data['compnay_product_link_perms']['search_in'] = $data['selected_name'];
            }

            $data['compnay_product_link_perms']['search_for'] = 'product';
            $data['posts'] = $postsModel->groupBy('brands.id')->orderBy('brands.name', $data['reqiest']['order_by'])
//            $data['posts'] = $postsModel->groupBy('brands.id')->orderBy('brands.id', 'desc')
                    ->paginate($data['reqiest']['post_per_page']);


//            print_r($data['posts']);

            return \Response::JSON(['data' => $data['posts'], 'pagination' => (string) $data['posts']->links()]);
        }
    }

    function SubscribeNewsletter() {
        //email to client
//        $email = $request->email;  
        NewsletterModel::create(Input::only('email'));

        $email_data['email_user'] = Input::get('email');
        $email_data['msg'] = '';
        $email = 'abdullahforloop@gmail.com';
        $flag = Mail::send('emails.subscribe', $email_data, function($message) use ($email) {
                    $message->to($email)
                            ->subject('Follow-Up with a Subscriber Request');
                    $message->from('website@wateronline.ae', 'Water Online');
                });
     
        $email = Input::get('email');
        $flag = Mail::send('emails.client_email_subscribe', [], function($message) use ($email) {
                    $message->to($email)
                            ->subject('WO! Subscribed Successfully');
                    $message->from('website@wateronline.ae', 'Water Online');
                });


        return 'Thank You for Subscribing.';
    }

}
