<?php

namespace App\Http\Controllers\APIs;

use App\Http\Controllers\Controller;
use App\Models\Company\CompanyModel;
use Input;
use DB;

class APIController extends Controller {
    
    protected $_authenticated = true;
    
    public function __construct() {
        // key = wateronline, secret = 286db39388d11482d7a9128fcc9be959
        $key = Input::get('key');
        $secret = Input::get('secret');
        if (!($key == 'wateronline' && $secret == md5('wateronlineapipass'))) {
            echo 'Authentication failed';exit;
        }
    }
    
    public function SearchResult() {
        $data['reqiest']['city'] = Input::get('city');
        $data['reqiest']['location_ids'] = Input::get('location_ids');
        $data['reqiest']['sub_location_ids'] = Input::get('sub_location_ids');
        $data['reqiest']['sizes'] = Input::get('sizes');
        $data['reqiest']['companies'] = Input::get('companies');
        $data['reqiest']['water_types'] = Input::get('water_types');
        $data['reqiest']['min_price'] = Input::get('min_price');
        $data['reqiest']['max_price'] = Input::get('max_price');
        $data['reqiest']['post_per_page'] = Input::get('post_per_page');
        $data['reqiest']['order_by'] = Input::get('order_by');
        $data['reqiest']['filter_for'] = Input::get('filter_for');
        $data['reqiest']['packaging_type'] = Input::get('packaging_type');
        
        if ($data['reqiest']['filter_for'] == 'Products') {
            $postsModel = ProductsModel::leftJoin('company as co', 'co.id', '=', 'product.company')->leftJoin('service_location as sl', 'sl.company_id', 
                                '=', 'co.id')->leftJoin('country as c', 'c.id', '=', 'sl.country')->leftJoin('state as s', 's.id', '=', 'sl.state')
                                ->leftJoin('city as ci', 'ci.id', '=', 'sl.city')->leftJoin('sub_location as sbl', 'sbl.id', '=', 'sl.sub_location')
                                ->leftJoin('location as l', 'l.id', '=', 'sl.location')->leftJoin('product_size as si', 'si.id', '=', 'product.size')
                                ->leftJoin('product_type as t', 't.id', '=', 'product.type')->leftJoin('product_image as i', 'i.product_id', '=', 
                                'product.id')->leftJoin('product_package as pt', 'pt.id', '=', 'product.package_type')->leftJoin('phone_numbers as pn', 
                                DB::raw("(pn.phone_for = 'company' && pn.reference_id = co.id)"), DB::raw(''), DB::raw(''))
                                ->select('pn.number as phone_number', 'product.price', 'product.discount_type', 'product.discount', 
                                'product.bundle_price', 'product.bundle_quantity', 'product.slug', 
                                'c.name as country', 's.name as state', 'ci.name as city','product.id', 'product.title', 'product.created_at', 
                                'si.name as size', 'co.company_name as company', 't.name as type', 'i.name as image', 'co.created_at as company_created', 
                                'co.logo', 'pt.name')->where('product.status', 1)->where('co.status', 1);

            if (!empty($data['reqiest']['city'])) {
                $postsModel->where('sl.city', $data['reqiest']['city']);
            }
            if (!empty($data['reqiest']['location_ids'])) {
                $postsModel->whereIn('sl.location', $data['reqiest']['location_ids']);
            }
            if (!empty($data['reqiest']['sub_location_ids'])) {
                $postsModel->whereIn('sl.sub_location', $data['reqiest']['sub_location_ids']);
            }

            if(!empty($data['reqiest']['water_types'])) {
                $postsModel->whereIn('t.id', $data['reqiest']['water_types']);
            }
            
            if(!empty($data['reqiest']['sizes'])) {
                $postsModel->whereIn('si.id', $data['reqiest']['sizes']);
            }
            
            if(!empty($data['reqiest']['companies'])) {
                $postsModel->whereIn('co.id', $data['reqiest']['companies']);
            }
            
            if(!empty($data['reqiest']['packaging_type'])) {
                $postsModel->whereIn('pt.id', $data['reqiest']['packaging_type']);
            }
            
            $data['posts'] = $postsModel->groupBy('product.id')->orderBy('product.id', $data['reqiest']['order_by'])
                                ->paginate($data['reqiest']['post_per_page']);
        
            return $data['posts'];
        } else {
            $postsModel = CompanyModel::leftJoin('service_location as sl', 'sl.company_id', '=', 'company.id')->leftJoin(DB::raw('(SELECT company, '
                                .'GROUP_CONCAT(DISTINCT(size)) AS sizes, GROUP_CONCAT(DISTINCT(type)) AS types FROM product GROUP BY company) as po'), 
                                'po.company', '=', 'company.id')->leftJoin(DB::raw('(SELECT id, company, GROUP_CONCAT(DISTINCT type) as packages FROM '
                                .'`product` GROUP BY company) as pt'), 'pt.company', '=', 'company.id')->leftJoin('country as c', 'c.id', '=', 
                                'sl.country')->leftJoin('state as s', 's.id', '=', 'sl.state')->leftJoin('city as ci', 'ci.id', '=', 'sl.city')
                                ->leftJoin('location as l', 'l.id', '=', 'sl.location')->leftJoin('sub_location as sbl', 'sbl.id', '=', 'sl.sub_location')
                                ->select("company.id", 'c.name as country', "company.created_at", "company.logo", 's.name as state', 'ci.name as city', 
                                'po.sizes', 'po.types', "company.company_name", 'pt.packages', 'company.slug')->where('company.status', 1);

            if (!empty($data['reqiest']['city'])) {
                $postsModel->where('sl.city', $data['reqiest']['city']);
            }
            if (!empty($data['reqiest']['location_ids'])) {
                $postsModel->whereIn('sl.location', $data['reqiest']['location_ids']);
            }
            if (!empty($data['reqiest']['sub_location_ids'])) {
                $postsModel->whereIn('sl.sub_location', $data['reqiest']['sub_location_ids']);
            }

            if(!empty($data['reqiest']['water_types'])) {
                $watertypes = $data['reqiest']['water_types'];
                $postsModel->where(function($query) use ($watertypes) {
                    foreach ($watertypes as $watertype) {
                        $query->orWhereRaw("FIND_IN_SET($watertype, po.types)");
                    }
                });
            }
            if(!empty($data['reqiest']['sizes'])) {
                $sizes = $data['reqiest']['sizes'];
                $postsModel->where(function($query) use ($sizes) {
                    foreach ($sizes as $size) {
                        $query->orWhereRaw("FIND_IN_SET($size, po.sizes)");
                    }
                });
            }

            if(!empty($data['selected_name'])) {
                $postsModel->where('company.company_name', 'like', "%{$data['selected_name']}%");
                $data['compnay_product_link_perms']['search_in'] = $data['selected_name'];
            }

            $data['compnay_product_link_perms']['search_for'] = 'product';
            $data['posts'] = $postsModel->groupBy('company.id')->orderBy('company.id', $data['reqiest']['order_by'])
                                ->paginate($data['reqiest']['post_per_page']);
            
            return $data['posts'];
        }
    }
    
    public function LoginUser () {
        $email = Input::get('email');
    }

}
