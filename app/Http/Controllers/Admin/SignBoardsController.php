<?php
namespace App\Http\Controllers\Admin;


use Input;
use App\Http\Controllers\Controller;
use App\Models\SignBoardsModel as MainModel;
use App\Http\Requests\Admin\SignBoardsRequest as MainRequest;

class SignBoardsController extends Controller {

    public function Index() {
        $data['posts'] = MainModel::orderBy('id', 'DESC')->paginate(50);
        
        return view("admin.sign_boards_list", $data);
    }
    
    public function Add($id = null) {
        
        if (is_null($id)) {
            $data['post'] = (object) ["id" => null, 'name' => "", 'description' => "", 'price' => null, 'image' => ''];
        } else {
            $data['post'] = MainModel::find($id);
        }
        
        return view("admin.sign_boards_add", $data);
    }
    
    public function Save (MainRequest $request) {
        
        $post = MainModel::updateOrCreate(['id' => $request->get('id')], $request->except("_token", "id", 'image'));
//        $newCity = ModelCities::findOrNew($request->get('id'));
//        $newCity->fill($request->except("_token"));
//        $test = $newCity->save();
        
        if ($request->file('image') != null) {
            $dateCreatedStructure = getFolderStructureByDate($post->created_at);
            $filename2 = saveOrignalImage($request->file('image'));
            
            makeMultipleSizesOfImage($filename2, 'img/sign_boards', ['orignal'], $post->created_at);
            
            removeSpecificFile($post->image, "img/sign_boards/$dateCreatedStructure", ['orignal']);
            $post->image = $filename2;
            $post->save();
        }
        
        $responseMsg = "Updated";
        if ($request->get('id') == "") {
            $responseMsg = "Created";
        }
        return redirect(ADMIN_PREFIX."/sign_boards/$post->id")->with('message', "$responseMsg Successfully"); 
    }
    
    public function Delete () {
        MainModel::destroy(Input::get('id'));
        return redirect(ADMIN_PREFIX."/sign_boards")->with('message', "Deleted Successfully"); 
    }
        
}