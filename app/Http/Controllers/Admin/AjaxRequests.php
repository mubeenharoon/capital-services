<?php
namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;

use Input;
//use Carbon;

class AjaxRequests extends Controller {

    public function UploadTempFiles () {
        $response = [];
        $files = Input::file('files');
        $destinationPath = 'img/temporary';
        
        foreach($files as $file) {
            $filename = uniqid().".".$file->getClientOriginalExtension();
            $file->move($destinationPath, $filename);
            $response[] = "$filename";
        }
        deleteOldFiles();
        return $response;
    }
        
}