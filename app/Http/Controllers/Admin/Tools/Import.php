<?php
namespace App\Http\Controllers\Admin\Tools;

use App\Http\Controllers\Controller;
use Carbon\Carbon;
use App\Models\Company\CompanyModel;
use App\Models\Products\SizesModel;
use App\Models\Products\TypesModel;
use App\Models\Products\ProductsModel;
use App\Models\LocationManagement\CitiesModel;
use App\Models\LocationManagement\StatesModel;
use App\Models\LocationManagement\LocationsModel;
use App\Models\LocationManagement\SubLocationsModel;
use App\Http\Requests\Admin\Tools\ImportProductsRequest;
use App\Http\Requests\Admin\Tools\ImportCompaniesRequest;

class Import extends Controller {

    public function Companies () {
        $data['states'] = StatesModel::leftJoin('country as c', 'c.id', '=', 'state.country_id')
                ->select('state.id', 'state.name', 'c.name as parent')->orderBy('c.name', "ASC")->orderBy('state.name', "ASC")->get();
        
        return view("admin.tools.import_companies", $data);
    }
    
    public function SaveCompanies (ImportCompaniesRequest $request) {
        $state = $request->get('state');
        $file = $request->file('csv_file');
        $now = Carbon::now();
        
        if ($file->getClientOriginalExtension() != "csv") {
            return redirect(ADMIN_PREFIX."/tools/import_companies")->withErrors("Invalid File Type");
        }
        
        $extension = $file->getClientOriginalExtension();
        $fileName = uniqid().'.'.$extension;
        $file->move('img/temporary', $fileName);
        
        $ds = DIRECTORY_SEPARATOR;
        $fileTempPath = "img/temporary/$fileName";
        $fileURL = url($fileTempPath);
        $fileDir = public_path().$ds."img$ds"."temporary$ds".$fileName;
        
        if (file_exists($fileDir)) {
            $file = fopen($fileDir, "r");
            $first_row = fgetcsv($file);
            
            if ($first_row[0] == 'company_name' && $first_row[1] == 'business_number' && $first_row[2] == 'phone_number' && $first_row[4] == 'fax' && 
                $first_row[3] == 'mobile_number' && $first_row[5] == 'description' && $first_row[6] == 'no_of_emp' && $first_row[7] == 'website' && 
                $first_row[8] == 'city' && $first_row[9] == 'location' && $first_row[10] == 'sub_location' && $first_row[11] == 'zipcode' && 
                $first_row[12] == 'street_address' && $first_row[13] == 'first_name' && $first_row[14] == 'last_name' && $first_row[15] == 'email') {
                
                $country = StatesModel::select('country_id')->where('id', $state)->first()->country_id;
                $companiesArray = [];
                while (!feof($file)) {
                    $row = fgetcsv($file);
                    if (!empty($row[0])) {
                        $slug = makeSlug($row[0]);
                        $existingSlugs = CompanyModel::where('slug', 'LIKE', "%$slug%")->get()->toArray();
                        $existingSlugs = array_column($existingSlugs, 'slug');
                        $companySlug = getUniqueSlug($slug, $existingSlugs);

                        $cityName = $row[8];
                        $locationName = $row[9];
                        $sub_locationName = $row[10];

                        if (empty($cityName)) {
                            $city = 0;
                        } else {
                            $cityArray = CitiesModel::select('id')->where('name', $cityName)->where('state_id', $state)->first();
                            if ($cityArray) {
                                $city = $cityArray->id;
                            } else {
                                $slug = makeSlug($cityName);
                                $exestingCities = CitiesModel::where('slug', 'LIKE', "%$slug%")->get()->toArray();
                                $existingSlugs = array_column($exestingCities, 'slug');
                                $slug = getUniqueSlug($slug, $existingSlugs);

                                $cityArray = CitiesModel::create(['name' => $cityName, 'slug' => $slug, 'state_id' => $state]);
                                $city = $cityArray->id;
                            }
                        }

                        if (empty($locationName)) {
                            $location = 0;
                        } else {
                            $locationArray = LocationsModel::select('id')->where('name', $locationName)->where('city_id', $city)->first();
                            if ($locationArray) {
                                $location = $locationArray->id;
                            } else {
                                $slug = makeSlug($locationName);
                                $exestingCities = LocationsModel::where('slug', 'LIKE', "%$slug%")->get()->toArray();
                                $existingSlugs = array_column($exestingCities, 'slug');
                                $slug = getUniqueSlug($slug, $existingSlugs);

                                $locationArray = LocationsModel::create(['name' => $locationName, 'slug' => $slug, 'city_id' => $city]);
                                $location = $locationArray->id;
                            }
                        }

                        if (empty($sub_locationName)) {
                            $sub_location = 0;
                        } else {
                            $subLocationArray = SubLocationsModel::select('id')->where('name', $sub_locationName)->where('location_id', $location)->first();
                            if ($subLocationArray) {
                                $sub_location = $subLocationArray->id;
                            } else {
                                $slug = makeSlug($sub_locationName);
                                $exestingCities = SubLocationsModel::where('slug', 'LIKE', "%$slug%")->get()->toArray();
                                $existingSlugs = array_column($exestingCities, 'slug');
                                $slug = getUniqueSlug($slug, $existingSlugs);

                                $subLocationArray = SubLocationsModel::create(['name' => $sub_locationName, 'slug' => $slug, 'location_id' => $location]);
                                $sub_location = $subLocationArray->id;
                            }
                        }

                        $companyArray = ['company_name' => $row[0], 'business_number' => $row[1], 'phone_number' => $row[2], 'mobile_number' => $row[3], 
                                    'description' => $row[5], 'no_of_emp' => $row[6], 'website' => $row[7], 'country' => $country, 'state' => $state, 
                                    'city' => $city, 'location' => $location, 'sub_location' => $sub_location, 'street_address' => $row[12], 
                                    'slug' => $companySlug, 'first_name' => $row[13], 'last_name' => $row[14], 'email' => $row[15], 'status' => 1, 
                                    'updated_at' => $now, 'created_at' => $now, 'fax' => $row[4], 'zipcode' => $row[11]];
                        $companiesArray[] = $companyArray;
                    }
                }
                
                CompanyModel::insert($companiesArray);
            } else {
                fclose($file);
                unlink($fileTempPath);
                return redirect(ADMIN_PREFIX."/tools/import_companies")->withErrors("Invalid File Data");
            }
        } else {
            return redirect(ADMIN_PREFIX."/tools/import_companies")->withErrors("Unable to upload file.");
        }
        
        fclose($file);
        unlink($fileTempPath);
        
        return redirect(ADMIN_PREFIX."/tools/import_companies")->with('message', "imported Successfully");
    }

    public function Products () {
    	$data['companies'] = CompanyModel::select('id', 'company_name as name')->orderBy('id', 'DESC')->get();
        return view("admin.tools.import_products", $data);
    }
    
    public function SaveProducts (ImportProductsRequest $request) {
        $company = $request->get('company');
        $file = $request->file('csv_file');
        $now = Carbon::now();
        
        if ($file->getClientOriginalExtension() != "csv") {
            return redirect(ADMIN_PREFIX."/tools/import_companies")->withErrors("Invalid File Type");
        }
        
        $extension = $file->getClientOriginalExtension();
        $fileName = uniqid().'.'.$extension;
        $file->move('img/temporary', $fileName);
        
        $ds = DIRECTORY_SEPARATOR;
        $fileTempPath = "img/temporary/$fileName";
        $fileURL = url($fileTempPath);
        $fileDir = public_path().$ds."img$ds"."temporary$ds".$fileName;
        
        if (file_exists($fileDir)) {
            $file = fopen($fileDir, "r");
            $first_row = fgetcsv($file);
            
            if ($first_row[0] == 'title' && $first_row[1] == 'description' && $first_row[2] == 'price' && $first_row[3] == 'size' 
                && $first_row[4] == 'type') {
//                $query = "LOAD DATA LOCAL INFILE '$fileURL' INTO TABLE product FIELDS TERMINATED BY ',' ENCLOSED BY '\"' LINES "
//                            ."TERMINATED BY '\n' IGNORE 1 ROWS (@title, @description, @price, @size, @type) SET title = @title, description = @description, "
//                            ."price = @price, size = @size, type = @type, company = '$company'";
//                DB::connection()->getpdo()->exec($query);
                
                $insertRowsArray = [];
                while (!feof($file)) {
                    $row = fgetcsv($file);
                    if (!empty($row[0]) && !empty($row[1]) && !empty($row[2]) && !empty($row[3]) && !empty($row[4])) {
                        $slug = makeSlug($row[0]);
                        $existingSlugs = CompanyModel::where('slug', 'LIKE', "%$slug%")->get()->toArray();
                        $existingSlugs = array_column($existingSlugs, 'slug');
                        $productSlug = getUniqueSlug($slug, $existingSlugs);

                        $sizeName = $row[3];
                        $typeName = $row[4];

                        $sizeArray = SizesModel::select('id')->where('name', $sizeName)->first();
                        if ($sizeArray) {
                            $size = $sizeArray->id;
                        } else {
                            $slug = makeSlug($sizeName);
                            $exestingCities = SizesModel::where('slug', 'LIKE', "%$slug%")->get()->toArray();
                            $existingSlugs = array_column($exestingCities, 'slug');
                            $slug = getUniqueSlug($slug, $existingSlugs);
                            
                            $sizeArray = SizesModel::create(['name' => $sizeName, 'slug' => $slug]);
                            $size = $sizeArray->id;
                        }

                        $typeArray = TypesModel::select('id')->where('name', $typeName)->first();
                        if ($typeArray) {
                            $type = $typeArray->id;
                        } else {
                            $slug = makeSlug($typeName);
                            $exestingCities = TypesModel::where('slug', 'LIKE', "%$slug%")->get()->toArray();
                            $existingSlugs = array_column($exestingCities, 'slug');
                            $slug = getUniqueSlug($slug, $existingSlugs);
                            
                            $typeArray = TypesModel::create(['name' => $typeName, 'slug' => $slug]);
                            $type = $typeArray->id;
                        }

                        $rowArray = ['title' => $row[0], 'description' => $row[1], 'price' => $row[2], 'size' => $size, 'type' => $type, 
                                        'company' => $company, 'slug' => $productSlug, 'status' => 1, 'updated_at' => $now, 'created_at' => $now];
                        $insertRowsArray[] = $rowArray;
                    }
                }
                
                ProductsModel::insert($insertRowsArray);
                
            } else {
                fclose($file);
                unlink($fileTempPath);
                return redirect(ADMIN_PREFIX."/tools/import_products")->withErrors("Invalid File Data");
            }
        } else {
            return redirect(ADMIN_PREFIX."/tools/import_products")->withErrors("Unable to upload file.");
        }
        
        fclose($file);
        unlink($fileTempPath);
        return redirect(ADMIN_PREFIX."/tools/import_products")->with('message', "imported Successfully");
    }
        
}