<?php
namespace App\Http\Controllers\Admin;


use Input;
use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\GalleryRequest;
use App\Models\GalleryModel;
use App\Models\GalleryImagesModel;

class GalleryController extends Controller {

    public function Index() {
        $data['posts'] = GalleryModel::orderBy('id', 'DESC')->paginate();
        
        return view("admin.gallery.list", $data);
    }
    
    public function Add($id = null) {
        if (is_null($id)) {
            $data['post'] = (object) ['id' => null, 'name' => '', 'created_at' => '', 'updated_at' => ''];
            $data['gallery_images'] = [];
        } else {
            $data['post'] = GalleryModel::find($id);
            $data['gallery_images'] = GalleryImagesModel::where('gallery_id', $id)->get()->toArray();
            $data['gallery_images'] = array_map(function($e) {return $e['name'];}, $data['gallery_images']);
        }
//        pr($data['gallery_images']);exit;
        return view("admin.gallery.add", $data);
    }
    
    public function Save (GalleryRequest $request) {
        $id = $request->get('id');
        $uploadedFiles = $request->get('uploadedFiles');
        
        $post = GalleryModel::updateOrCreate(['id' => $id], $request->except("_token", "id"));
        $responseMsg = "Updated";
        if ($id == "") {
            $responseMsg = "Created";
        }
        
        $folderDateStructure = getFolderStructureByDate($post->created_at);
        $path = "img/gallery";
        
        if (!empty($id)) {
            $oldImages = GalleryImagesModel::where('gallery_id', $post->id)->get();
            foreach ($oldImages as $oldImage) {
                if (strpos($uploadedFiles, $oldImage->name) !== false) {
                    $uploadedFiles = str_replace($oldImage->name."######", "", $uploadedFiles);
                    if (strpos($uploadedFiles, $oldImage->name) !== false) {
                        $uploadedFiles = str_replace($oldImage->name, "", $uploadedFiles);
                    }
                } else {
                    unlink("$path/$folderDateStructure/orignal/".$oldImage->name);
                    unlink("$path/$folderDateStructure/small/".$oldImage->name);
                    
                    GalleryImagesModel::destroy($oldImage->id);
                }
            }
        }
        
        $allFiles = explode("######", $uploadedFiles);
        $images = [];
        foreach ($allFiles as $file) {
            if (!empty($file)) {
                $images[] = ['gallery_id' => $post->id, 'name' => $file];
                makeMultipleSizesOfImage($file, $path, ['orignal', 'small'], $post->created_at);
            }
        }
        
        GalleryImagesModel::insert($images);
        
        return redirect(ADMIN_PREFIX."/gallery/$post->id")->with('message', "$responseMsg Successfully"); 
    }
    
    public function Delete () {
        $id = Input::get('id');
        $gallery = GalleryModel::find($id);
        
        $folderDateStructure = getFolderStructureByDate($gallery->created_at);
        $path = "img/gallery";
        
        
        $post_images = GalleryImagesModel::where('gallery_id', $id)->get();
        foreach ($post_images as $post_image) {
            unlink("$path/$folderDateStructure/orignal/".$post_image->name);
            unlink("$path/$folderDateStructure/small/".$post_image->name);
        }
        
        GalleryImagesModel::where('gallery_id', $id)->delete();
        GalleryModel::destroy($id);
        return redirect(ADMIN_PREFIX."/gallery")->with('message', "Deleted Successfully"); 
    }
        
}