<?php
namespace App\Http\Controllers\Admin\Users;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\Users\UsersRequest;
use App\Models\Users\UsersCompanyModel;
use App\Models\Users\UsersRolesModel;
use App\Models\Users\UsersModel;
use App\Models\Company\CompanyModel;
use Input;

class Users extends Controller {

    public function Index() {
        $usersQuery = UsersModel::select('user.id', 'user.first_name', 'user.last_name', 'user.email');
        
        $data['pagination_params'] = [];
        
        if (Input::has('search_string')) {
            $data['search_string'] = Input::get('search_string');
        } else {
            $data['search_string'] = '';
        }
        
        if (!empty($data['search_string'])) {
            $usersQuery->where('user.first_name', 'LIKE', '%'.$data['search_string'].'%')->orWhere('user.last_name', 'LIKE', '%'.$data['search_string'].'%')
                        ->orWhere('user.email', 'LIKE', '%'.$data['search_string'].'%');
            $data['pagination_params']['search_string'] = $data['search_string'];
        }
        
        $data['users'] = $usersQuery->groupBy('user.id')->orderBy('id', 'DESC')->paginate(25);
        
        return view("admin.users.user_list", $data);
    }

    public function Add ($userid = null) {
        if (is_null($userid)) {
            $data['user'] = (object) ['id' => '', 'logo' => '', 'first_name' => '', 'username'=>'', 'last_name' => '', 'email' => '', 'status' => '', 'role' => '', 
                                'users_companies' => []];
        } else {
            $data['user'] = UsersModel::find($userid);
        }
        
        return view("admin.users.users_add", $data);
    }
    
    public function save (UsersRequest $request) {
        $id = Input::get('id');
        $userDataArray = $request->except('_token', 'id', 'profile_pic', 'password_confirmation', 'password');
        if ($request->get('password') != "") {
            $request->merge(array('password' => md5($request->get('password'))));
            $userDataArray = $request->except('_token', 'id', 'profile_pic', 'password_confirmation');
        }
        
        if (!empty($id)) {
            $userDetails = UsersModel::find($id);
            $userDetails->fill($userDataArray);
            $userDetails->save();
            
            $responseMessage = "Updated";
        } else {
            $userDetails = UsersModel::create($userDataArray);
            $id = $userDetails->id;
            
            $responseMessage = "Created";
        }
        $createdAT = $userDetails->created_at;
        
        if ($request->file('profile_pic') != null) {
            $createdAtDirectory = getFolderStructureByDate($createdAT);
            $fileName = saveOrignalImage($request->file('profile_pic'));
            makeMultipleSizesOfImage($fileName, 'img/users', ['orignal'], $createdAT);
            if (!empty($userDetails['image'])) {
                removeSpecificFile($userDetails['image'], "img/users/$createdAtDirectory");
            }
            
            $userDetails->image = $fileName;
            $userDetails->save();
        }
        
        return redirect(ADMIN_PREFIX."/users/$id")->with('message', $responseMessage.' Successfully');
    }
    
    public function Delete() {
    	$userid = Input::get('id');
    	UsersModel::destroy($userid);
        
        return redirect(ADMIN_PREFIX."/users")->with('message', 'Deleted Successfully');
    }
        
}
