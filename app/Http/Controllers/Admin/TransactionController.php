<?php
namespace App\Http\Controllers\Admin;


use Input;
use App\Http\Controllers\Controller;
use App\Models\TransactionModel as MainModel;

class TransactionController extends Controller {

    public function Index() {
        $data['posts'] = MainModel::leftJoin('user as u', 'u.id', '=', 'transaction.user_id')->select('transaction.*', 'u.first_name', 'u.last_name')
                            ->orderBy('id', 'DESC')->paginate(50);
        return view("admin.transactions_list", $data);
    }
        
}