<?php
namespace App\Http\Controllers\Admin\Auth;

use DB;
use Session;
use App\Http\Controllers\Controller;
use Illuminate\Contracts\Auth\Guard;

use App\Http\Requests\Admin\Auth\LoginRequest;
use App\Http\Requests\Admin\Auth\RegisterRequest;

class AuthController extends Controller {

    /**
     * the model instance
     * @var User
     */
    protected $user; 
    /**
     * The Guard implementation.
     *
     * @var Authenticator
     */
    protected $auth;

    /**
     * Create a new authentication controller instance.
     *
     * @param  Authenticator  $auth
     * @return void
     */
    public function __construct(Guard $auth) {
        $this->auth = $auth;

        if (count(Session::get('admin_user')) > 0) {
            return redirect(ADMIN_PREFIX);
        }
    }

    /**
     * Show the application registration form.
     *
     * @return Response
     */
    public function getRegister() {
        return view('admin.auth.register');
    }

    /**
     * Handle a registration request for the application.
     *
     * @param  RegisterRequest  $request
     * @return Response
     */
    public function postRegister(RegisterRequest $request)
    {
        //code for registering a user goes here.
        $this->user->email = $request->email;
        $this->user->password = bcrypt($request->password);
        $this->user->save();
        $this->auth->login($this->user); 
        return redirect(ADMIN_PREFIX); 
    }

    /**
     * Show the application login form.
     *
     * @return Response
     */
    public function getLogin()
    {
        return view('admin.auth.login');
    }

    /**
     * Handle a login request to the application.
     *
     * @param  LoginRequest  $request
     * @return Response
     */
    public function postLogin(LoginRequest $request) {
//        var_dump($request->all());exit;
        $user = DB::table('admin_users')->Where("email", $request->get('email'))->Where("password", md5($request->get('password')))
                    ->where('status', 1)->first();
        
        if (count($user) > 0) {
            Session::put('admin_user', $user);
            return redirect()->intended($this->redirectPath());
        }
        
        return redirect(ADMIN_PREFIX.'/auth/login')->withErrors([
            'email' => 'The credentials you entered did not match our records. Try again?',
        ]);
    }

    /**
     * Log the user out of the application.
     *
     * @return Response
     */
    public function getLogout() {
        Session::forget("admin_user");
        return redirect(ADMIN_PREFIX.'/auth/login');
    }
    
    public function redirectPath() {
        if (property_exists($this, 'redirectPath')) {
            return $this->redirectPath;
        }

        return property_exists($this, 'redirectTo') ? $this->redirectTo : ADMIN_PREFIX;
    }

}