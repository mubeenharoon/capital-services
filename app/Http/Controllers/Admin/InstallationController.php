<?php
namespace App\Http\Controllers\Admin;


use Input;
use App\Http\Controllers\Controller;
use App\Models\OrdersModel as MainModel;
use App\Models\OrdersItemsModel;
use App\Http\Requests\Admin\SignBoardsRequest as MainRequest;

class InstallationController extends Controller {

    public function Index() {
        $data['posts'] = MainModel::leftJoin('cnf_order_status as s', 's.id', '=', 'orders.status')->select('orders.*', 's.name as status_name')
                            ->orderBy('id', 'DESC')->paginate(50);
        return view("admin.installation.list", $data);
    }

    public function View ($id) {
        $data['post'] = MainModel::leftJoin('dbk_property_types as dpt', 'dpt.id', '=', 'orders.property_type')
                            ->select('orders.*', 'dpt.name as proper_type_name')->where('orders.id', $id)->first();
        $data['pos_items'] = OrdersItemsModel::select('s.*')->join('signboards as s', 's.id', '=', 'order_items.signboard_id')
                                ->where('order_items.order_id', $id)->get();
        
        return view("admin.installation.add", $data);
    }
    
    public function Add($id = null) {
        
        if (is_null($id)) {
            $data['post'] = (object) ["id" => null, 'name' => "", 'description' => "", 'price' => null, 'image' => ''];
        } else {
            $data['post'] = MainModel::find($id);
        }
        
        return view("admin.installation.add", $data);
    }
    
    public function Save (MainRequest $request) {
        
        $post = MainModel::updateOrCreate(['id' => $request->get('id')], $request->except("_token", "id", 'image'));
//        $newCity = ModelCities::findOrNew($request->get('id'));
//        $newCity->fill($request->except("_token"));
//        $test = $newCity->save();
        
        if ($request->file('image') != null) {
            $dateCreatedStructure = getFolderStructureByDate($post->created_at);
            $filename2 = saveOrignalImage($request->file('image'));
            
            makeMultipleSizesOfImage($filename2, 'img/sign_boards', ['orignal'], $post->created_at);
            
            removeSpecificFile($post->image, "img/sign_boards/$dateCreatedStructure", ['orignal']);
            $post->image = $filename2;
            $post->save();
        }
        
        $responseMsg = "Updated";
        if ($request->get('id') == "") {
            $responseMsg = "Created";
        }
        return redirect(ADMIN_PREFIX."/sign_boards/$post->id")->with('message', "$responseMsg Successfully"); 
    }
    
    public function Approve ($id) {
        MainModel::where('id', $id)->update(['status' => 1]);
        return redirect(ADMIN_PREFIX."/installations")->with('message', "Approved Successfully"); 
    }
    
    public function Delete () {
        MainModel::destroy(Input::get('id'));
        return redirect(ADMIN_PREFIX."/sign_boards")->with('message', "Deleted Successfully"); 
    }
        
}