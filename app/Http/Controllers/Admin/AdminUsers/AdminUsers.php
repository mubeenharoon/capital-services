<?php
namespace App\Http\Controllers\Admin\AdminUsers;

use App\Http\Controllers\Controller;
use Session;
use App\Models\Admin\AdminUsers\AdminUsersModel;
use App\Http\Requests\Admin\AdminUsers\AdminUsersRequest;
use Input;

class AdminUsers extends Controller {

    public function Index() {
        $usersQuery = AdminUsersModel::select('id', 'first_name', 'last_name', 'email');
        
        $data['pagination_params'] = [];
        
        if (Input::has('search_string')) {
            $data['search_string'] = Input::get('search_string');
        } else {
            $data['search_string'] = '';
        }
        
        if (!empty($data['search_string'])) {
            $usersQuery->where('first_name', 'LIKE', '%'.$data['search_string'].'%')->orWhere('last_name', 'LIKE', '%'.$data['search_string'].'%')
                        ->orWhere('email', 'LIKE', '%'.$data['search_string'].'%');
            $data['pagination_params']['search_string'] = $data['search_string'];
        }
        
        $data['users'] = $usersQuery->orderBy('id', 'DESC')->paginate(25);
        
        return view("admin.admin_users.admin_user_list", $data);
    }

    public function AddEdit ($userid = null) {
        if (is_null($userid)) {
            $data['user'] = (object) ['id' => '', 'first_name' => '', 'last_name' => '', 'email' => '', 'role' => '', 'status' => ''];
        } else {
            if ($userid == 'profile') {
                $userid = Session::get('admin_user')->id;
            }

            $data['user'] = AdminUsersModel::find($userid);
        }
        
        return view("admin.admin_users.admin_user_add", $data);
    }
    
    public function save (AdminUsersRequest $request) {
        $id = Input::get('id');
        
        $userDataArray = $request->except('_token', 'id', 'profile_pic', 'password_confirmation', 'password');
        if ($request->get('password') != "") {
            $request->merge(array('password' => md5($request->get('password'))));
            $userDataArray = $request->except('_token', 'id', 'profile_pic', 'password_confirmation');
        }
        
        if (!empty($id)) {
            unset($userDataArray['email']);
            $userDetails = AdminUsersModel::find($id);
            $userDetails->fill($userDataArray);
            $userDetails->save();
            
            $responseMessage = "Updated";
        } else {
            $userDetails = AdminUsersModel::create($userDataArray);
            $id = $userDetails->id;
            
            $responseMessage = "Created";
        }
        $createdAT = $userDetails->created_at;
        
        if ($request->file('profile_pic') != null) {
            $createdAtDirectory = getFolderStructureByDate($createdAT);
            $fileName = saveOrignalImage($request->file('profile_pic'));
            makeMultipleSizesOfImage($fileName, 'img/admin_users', ['orignal'], $createdAT);
            if (!empty($userDetails['image'])) {
                removeSpecificFile($userDetails['image'], "img/admin_users/$createdAtDirectory");
            }
            
            $userDetails->image = $fileName;
            $userDetails->save();
        }
        
        if (Session::get('admin_user')->id == $id) {
            $response_id = 'profile';
            Session::put('admin_user', $userDetails);
        } else {
            $response_id = $id;
        }
        
        return redirect(ADMIN_PREFIX."/admin_users/$response_id")->with('message', $responseMessage.' Successfully');
    }
    
    public function Delete() {
    	$id = Input::get('id');
    	AdminUsersModel::destroy($id);
        
        return redirect(ADMIN_PREFIX."/admin_users")->with('message', 'Deleted Successfully');
    }
        
}
