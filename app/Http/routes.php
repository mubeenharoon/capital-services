<?php

/*
  |--------------------------------------------------------------------------
  | Application Routes
  |--------------------------------------------------------------------------
  |
  | Here is where you can register all of the routes for an application.
  | It's a breeze. Simply tell Laravel the URIs it should respond to
  | and give it the controller to call when that URI is requested.
  |
 */

    
// Front end Routes
Route::get('/', 'HomeController@Index');

// Paypal Routes
Route::get('paypal/status', 'PaypalController@InstallationPaymentResponse');

//Ajax Requests
Route::post('upload-temp-images', 'Admin\AjaxRequests@UploadTempFiles');

// User Routes
Route::group(array('namespace' => 'User', 'middleware' => 'auth', 'prefix' => USER_PREFIX), function() {
    Route::get('/', 'DashboardController@Index');
    Route::get('installation/add', 'InstallationController@Add');
    Route::post('installation', 'InstallationController@Save');

    //Post
    Route::group(array('prefix' => 'posts'), function() {
        Route::get('/', 'InstallationController@Index');
        Route::get('{id}', 'InstallationController@View');
        Route::delete('/', 'InstallationController@Delete');
    });

    //Post
    Route::group(array('prefix' => 'profile'), function() {
        Route::get('/', 'ProfileController@View');
        Route::post('/', 'ProfileController@Save');
    });
});

// Adming Routes
Route::group(array('namespace' => 'Admin', 'middleware' => 'adminAuth', 'prefix' => ADMIN_PREFIX), function() {
    Route::get('/', 'AdminController@Index');
    Route::get('transactions', 'TransactionController@Index');

    //Users Management
    Route::group(array('namespace' => 'Users', 'prefix' => 'users'), function() {
        //Users
        Route::get('/', 'Users@Index');
        Route::get('add', 'Users@Add');
        Route::get('{id}', 'Users@Add')->where('id', '[0-9]+');
        Route::post('/', 'Users@Save');
        Route::delete('/', 'Users@Delete');
    });

    //Sign Boards Management
    Route::group(array('prefix' => 'sign_boards'), function() {
        Route::get('/', 'SignBoardsController@Index');
        Route::get('add', 'SignBoardsController@Add');
        Route::get('{id}', 'SignBoardsController@Add')->where('id', '[0-9]+');
        Route::post('/', 'SignBoardsController@Save');
        Route::delete('/', 'SignBoardsController@Delete');
    });

    //Gallery Management
    Route::group(array('prefix' => 'gallery'), function() {
        Route::get('/', 'GalleryController@Index');
        Route::get('add', 'GalleryController@Add');
        Route::get('{id}', 'GalleryController@Add')->where('id', '[0-9]+');
        Route::post('/', 'GalleryController@Save');
        Route::delete('/', 'GalleryController@Delete');
    });

    //Installation Orders
    Route::group(array('prefix' => 'installations'), function() {
        Route::get('/', 'InstallationController@Index');
        Route::get('{id}', 'InstallationController@View')->where('id', '[0-9]+');
        Route::get('approve/{id}', 'InstallationController@Approve')->where('id', '[0-9]+');
        
    });

    //Tools
    Route::group(array('namespace' => 'Tools', 'prefix' => 'tools'), function() {

    });

    Route::controllers([
        'auth' => 'Auth\AuthController'
    ]);

    Route::get('404', function() {
        return view('admin.errors.404');
    });
});

// Ajax Controller
//Route::post('ajax/post-comment', 'ProcessController@PostComment');

Route::controllers([
    'auth' => 'Auth\AuthController',
    'password' => 'Auth\PasswordController',
]);

Route::get('404', function() {
    $data['metas'] = get_page_meta_array('Not Found');
    return view('errors.404', $data);
});