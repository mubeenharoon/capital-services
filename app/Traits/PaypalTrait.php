<?php
namespace App\Traits;

use Auth;
use Session;
use App\Models\OrdersModel;
use App\Models\TransactionModel;

use \PayPal\Api\Payer;
use \PayPal\Api\Item;
use \PayPal\Api\ItemList;
use \PayPal\Api\Amount;
use \PayPal\Api\Details;
use \PayPal\Api\Transaction;
use \PayPal\Api\RedirectUrls;
use \PayPal\Api\Payment;
use \PayPal\Exception\PPConnectionException;
use \PayPal\Api\PaymentExecution;

trait PaypalTrait {

    protected $_apiContext, $userid;
    
    public function __construct() {
        $this->userid = Auth::id();
        $this->_apiContext = new \PayPal\Rest\ApiContext(
            new \PayPal\Auth\OAuthTokenCredential(
//                'Adr2ATSRTuKOo1UhL7KKST7PTzwlTi-VyBPz8NgaIpTp2axu1j4L_jWlJ8Rb64hbnWQlxRPK291X3gqr',     // ClientID
//                'ED8VdGBW7VxCjPWYtjAZ75NNiSGmK7vitIFQaycYgN0j8cyYiNjM0FU55d33-7tXMP2TXUJ5Ci-84WHZ'      // ClientSecret
                'AYSq3RDGsmBLJE-otTkBtM-jBRd1TCQwFf9RGfwddNXWz0uFU9ztymylOhRS',     // ClientID
                'EGnHDxD_qRPdaLdZz8iCr8N7_MzF-YHPTkjs6NKYQvQSBngp4PTTVWkPZRbL'      // ClientSecret
            )
        );
        $this->_apiContext->setConfig(['mode' => 'sandbox']);
    }
    
    function RedirectToPaypalForInsatalltionPayment ($selected_items, $order_id) {
        $items = [];
        $totalAmount = 0;
        
        $loopCounter = 0;
        foreach ($selected_items as $selected_item) {
            $items[$loopCounter] = new Item();
            $items[$loopCounter]->setName($selected_item['name'])
                ->setCurrency('USD')
                ->setQuantity(1)
                ->setPrice($selected_item['price']);
            $totalAmount += $selected_item['price'];
            $loopCounter++;
        }
//        $taxAmount = ($totalAmount/100)*$this->userTax->tax_percent;
        
        $payer = new Payer();
        $payer->setPaymentMethod('paypal');
        
        // add item to list
        $item_list = new ItemList();
        $item_list->setItems($items);
		
        $details  = new Details();
        $details/*->setTax($taxAmount)*/
        	->setSubtotal($totalAmount);

        $amount = new Amount();
        $amount->setCurrency('USD')
            ->setDetails($details)
            ->setTotal($totalAmount);

        $transaction = new Transaction();
        $transaction->setAmount($amount)
            ->setItemList($item_list)
            ->setDescription('Payment for you installation.');

        $redirect_urls = new RedirectUrls();
        $redirect_urls->setReturnUrl(URL('paypal/status')) // Specify return URL
            ->setCancelUrl(URL('paypal/status'));

        $payment = new Payment();
        $payment->setIntent('Sale')
            ->setPayer($payer)
            ->setRedirectUrls($redirect_urls)
            ->setTransactions(array($transaction));

        try {
            $payment->create($this->_apiContext);
        } catch (PPConnectionException $ex) {
            if (\Config::get('app.debug')) {
                echo "Exception: " . $ex->getMessage() . PHP_EOL;
                $err_data = json_decode($ex->getData(), true);
                exit;
            } else {
                die('Some error occur, sorry for inconvenient');
            }
        }

        foreach($payment->getLinks() as $link) {
            if($link->getRel() == 'approval_url') {
                $redirect_url = $link->getHref();
                break;
            }
        }

        // add payment ID to session
        Session::put('paypal_payment_details', ['payment_id' => $payment->getId(), 'order_id' => $order_id, 'amount' => $totalAmount]);
        if(isset($redirect_url)) {
            // redirect to paypal
            return $redirect_url;
        } else {
            return 'error';
        }
    
        
    }
    
    function InstallationPayemntResponse ($payer_id, $payment_details) {
        $payment = Payment::get($payment_details['payment_id'], $this->_apiContext);
        
        $execution = new PaymentExecution();
        $execution->setPayerId($payer_id);

        //Execute the payment
        $result = $payment->execute($execution, $this->_apiContext);

        if ($result->getState() == 'approved') {
            TransactionModel::create(['user_id' => $this->userid, 'order_id' => $payment_details['order_id'], 'amount' => $payment_details['amount'], 
                                        'payment_id' => $payment_details['payment_id']]);
            OrdersModel::where('id', $payment_details['order_id'])->update(['status' => 2]);
            return 'success';
        } else {
            OrdersModel::where('id', $payment_details['order_id'])->update(['status' => 3]);
            return 'fail';
        }
    }
    
}
